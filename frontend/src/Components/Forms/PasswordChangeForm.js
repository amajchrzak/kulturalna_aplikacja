const PasswordChangeForm = props => {

    return (
        <div className='passwordChange' id={props.id}>
            <h1 className='title'>Zmień hasło</h1>
            <form onSubmit={e => props.handleChangePassword(e)}>
                <h1 className="h3 mb-3 font-weight-normal title">Podaj dane</h1>
                <label htmlFor="inputCurrentPassword" className="h2 mb-2">Obecne hasło</label>
                <input type="password" id="inputCurrentPassword" className="form-control" placeholder="Current password" required autoFocus
                    onChange={e => { props.setCurrentPassword(e.target.value) }}
                    value={props.inputCurrentPassword}
                />
                <br />
                <label htmlFor="inputNewPassword" className="h2 mb-2">Nowe hasło</label>
                <input type="password" id="inputNewPassword" className="form-control" placeholder="New password" required
                    onChange={e => { props.setNewPassword(e.target.value) }}
                    value={props.inputNewPassword}
                />
                <br />
                <label htmlFor="inputRetypedNewPassword" className="h2 mb-2">Powtórz hasło</label>
                <input type="password" id="inputRetypedNewPassword" className="form-control" placeholder="New password" required
                    onChange={e => { props.setRetypedNewPassword(e.target.value) }}
                    value={props.retypedNewPassword}
                />
                <br />
                <button type="submit" className="btn btn-lg btn-primary btn-block">Zmień hasło</button>
                <p className="mt-5 mb-3 text-muted">&copy; 2021</p>
            </form>
        </div>
    )

}

export default PasswordChangeForm;