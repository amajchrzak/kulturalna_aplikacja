const RemoveEventForm = props => {
    return (
        <div className="window">
            <h2 className="titleRemoveEvent">Czy usunąć wydarzenie?</h2>
            <div>
                <form onSubmit={e => props.handleRemoveEvent(e)}>
                    <div className="buttonsRemoveEvent">
                        <button onClick={props.closeModal} className="btn btn-secondary btn-block singleButton">Zamknij</button>
                        <button type="submit" className="btn btn-success btn-block singleButton">Usuń wydarzenie</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default RemoveEventForm;