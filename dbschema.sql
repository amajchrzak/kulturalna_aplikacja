PGDMP     #        
            y            ka    10.19    10.19 C    a           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            b           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            c           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            d           1262    16393    ka    DATABASE     �   CREATE DATABASE ka WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Polish_Poland.1250' LC_CTYPE = 'Polish_Poland.1250';
    DROP DATABASE ka;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            e           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            f           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16397    chat    TABLE     �   CREATE TABLE public.chat (
    id integer NOT NULL,
    sender_id integer NOT NULL,
    receiver_id integer NOT NULL,
    message text NOT NULL
);
    DROP TABLE public.chat;
       public         postgres    false    3            �            1259    16403    Chat_id_seq    SEQUENCE     �   ALTER TABLE public.chat ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Chat_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    3    196            �            1259    16405    authors    TABLE     W   CREATE TABLE public.authors (
    id integer NOT NULL,
    user_id integer NOT NULL
);
    DROP TABLE public.authors;
       public         postgres    false    3            �            1259    16408    authors_id_seq    SEQUENCE     �   ALTER TABLE public.authors ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.authors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    198    3            �            1259    16410    billing_address    TABLE     �   CREATE TABLE public.billing_address (
    id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying(255),
    surname character varying(255)
);
 #   DROP TABLE public.billing_address;
       public         postgres    false    3            �            1259    16416    billing_address_id_seq    SEQUENCE     �   ALTER TABLE public.billing_address ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.billing_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    200    3            �            1259    16418    events    TABLE     �  CREATE TABLE public.events (
    id integer NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    price character varying(255),
    place_id integer,
    amount_of_participants integer,
    created_at date NOT NULL,
    updated_at date,
    start_of_event timestamp without time zone NOT NULL,
    end_of_event timestamp without time zone NOT NULL,
    author_id integer NOT NULL,
    instructor_id integer,
    limit_of_participants integer
);
    DROP TABLE public.events;
       public         postgres    false    3            �            1259    16424    events_id_seq    SEQUENCE     �   ALTER TABLE public.events ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    3    202            �            1259    16426    instructors    TABLE     [   CREATE TABLE public.instructors (
    id integer NOT NULL,
    user_id integer NOT NULL
);
    DROP TABLE public.instructors;
       public         postgres    false    3            �            1259    16429    instructors_id_seq    SEQUENCE     �   ALTER TABLE public.instructors ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.instructors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    204    3            �            1259    16431    opinions    TABLE     �   CREATE TABLE public.opinions (
    id integer NOT NULL,
    event_id integer NOT NULL,
    user_id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text,
    created_at date NOT NULL
);
    DROP TABLE public.opinions;
       public         postgres    false    3            �            1259    16437    opinions_id_seq    SEQUENCE     �   ALTER TABLE public.opinions ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.opinions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    206    3            �            1259    16439    participants    TABLE     {   CREATE TABLE public.participants (
    id integer NOT NULL,
    event_id integer NOT NULL,
    user_id integer NOT NULL
);
     DROP TABLE public.participants;
       public         postgres    false    3            �            1259    16442    participants_id_seq    SEQUENCE     �   ALTER TABLE public.participants ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    208    3            �            1259    16444    places    TABLE     �   CREATE TABLE public.places (
    id integer NOT NULL,
    name_of_place character varying(255) NOT NULL,
    limit_of_participants integer NOT NULL
);
    DROP TABLE public.places;
       public         postgres    false    3            �            1259    16447    places_id_seq    SEQUENCE     �   ALTER TABLE public.places ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.places_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    3    210            �            1259    16449    posts    TABLE     �   CREATE TABLE public.posts (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text NOT NULL,
    author_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL
);
    DROP TABLE public.posts;
       public         postgres    false    3            �            1259    16455    posts_id_seq    SEQUENCE     �   ALTER TABLE public.posts ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    212    3            �            1259    16457    roles    TABLE     F   CREATE TABLE public.roles (
    id integer NOT NULL,
    name text
);
    DROP TABLE public.roles;
       public         postgres    false    3            �            1259    16463    roles_id_seq    SEQUENCE     �   ALTER TABLE public.roles ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    3    214            �            1259    16465    users    TABLE     3  CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    role integer DEFAULT 1,
    state boolean DEFAULT true NOT NULL,
    reset_password_token character varying(255)
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1259    16473    users_id_seq    SEQUENCE     �   ALTER TABLE public.users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    216    3            K          0    16405    authors 
   TABLE DATA               .   COPY public.authors (id, user_id) FROM stdin;
    public       postgres    false    198   CH       M          0    16410    billing_address 
   TABLE DATA               E   COPY public.billing_address (id, user_id, name, surname) FROM stdin;
    public       postgres    false    200   |H       I          0    16397    chat 
   TABLE DATA               C   COPY public.chat (id, sender_id, receiver_id, message) FROM stdin;
    public       postgres    false    196   &I       O          0    16418    events 
   TABLE DATA               �   COPY public.events (id, title, description, price, place_id, amount_of_participants, created_at, updated_at, start_of_event, end_of_event, author_id, instructor_id, limit_of_participants) FROM stdin;
    public       postgres    false    202   bI       Q          0    16426    instructors 
   TABLE DATA               2   COPY public.instructors (id, user_id) FROM stdin;
    public       postgres    false    204   �L       S          0    16431    opinions 
   TABLE DATA               Y   COPY public.opinions (id, event_id, user_id, title, description, created_at) FROM stdin;
    public       postgres    false    206   �L       U          0    16439    participants 
   TABLE DATA               =   COPY public.participants (id, event_id, user_id) FROM stdin;
    public       postgres    false    208   -N       W          0    16444    places 
   TABLE DATA               J   COPY public.places (id, name_of_place, limit_of_participants) FROM stdin;
    public       postgres    false    210   JN       Y          0    16449    posts 
   TABLE DATA               N   COPY public.posts (id, title, description, author_id, created_at) FROM stdin;
    public       postgres    false    212   �N       [          0    16457    roles 
   TABLE DATA               )   COPY public.roles (id, name) FROM stdin;
    public       postgres    false    214   	S       ]          0    16465    users 
   TABLE DATA               a   COPY public.users (id, email, password, username, role, state, reset_password_token) FROM stdin;
    public       postgres    false    216   ES       g           0    0    Chat_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."Chat_id_seq"', 3, true);
            public       postgres    false    197            h           0    0    authors_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.authors_id_seq', 15, true);
            public       postgres    false    199            i           0    0    billing_address_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.billing_address_id_seq', 15, true);
            public       postgres    false    201            j           0    0    events_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.events_id_seq', 66, true);
            public       postgres    false    203            k           0    0    instructors_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.instructors_id_seq', 2, true);
            public       postgres    false    205            l           0    0    opinions_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.opinions_id_seq', 90, true);
            public       postgres    false    207            m           0    0    participants_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.participants_id_seq', 41, true);
            public       postgres    false    209            n           0    0    places_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.places_id_seq', 11, true);
            public       postgres    false    211            o           0    0    posts_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.posts_id_seq', 18, true);
            public       postgres    false    213            p           0    0    roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.roles_id_seq', 3, true);
            public       postgres    false    215            q           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 32, true);
            public       postgres    false    217            �
           2606    16476    chat Chat_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.chat
    ADD CONSTRAINT "Chat_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.chat DROP CONSTRAINT "Chat_pkey";
       public         postgres    false    196            �
           2606    16478    authors authors_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.authors
    ADD CONSTRAINT authors_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.authors DROP CONSTRAINT authors_pkey;
       public         postgres    false    198            �
           2606    16480 $   billing_address billing_address_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.billing_address
    ADD CONSTRAINT billing_address_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.billing_address DROP CONSTRAINT billing_address_pkey;
       public         postgres    false    200            �
           2606    16482    events events_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.events DROP CONSTRAINT events_pkey;
       public         postgres    false    202            �
           2606    16484    instructors instructors_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.instructors
    ADD CONSTRAINT instructors_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.instructors DROP CONSTRAINT instructors_pkey;
       public         postgres    false    204            �
           2606    16486 &   opinions opinions_event_id_user_id_key 
   CONSTRAINT     n   ALTER TABLE ONLY public.opinions
    ADD CONSTRAINT opinions_event_id_user_id_key UNIQUE (event_id, user_id);
 P   ALTER TABLE ONLY public.opinions DROP CONSTRAINT opinions_event_id_user_id_key;
       public         postgres    false    206    206            �
           2606    16488    opinions opinions_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.opinions
    ADD CONSTRAINT opinions_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.opinions DROP CONSTRAINT opinions_pkey;
       public         postgres    false    206            �
           2606    16490 .   participants participants_event_id_user_id_key 
   CONSTRAINT     v   ALTER TABLE ONLY public.participants
    ADD CONSTRAINT participants_event_id_user_id_key UNIQUE (event_id, user_id);
 X   ALTER TABLE ONLY public.participants DROP CONSTRAINT participants_event_id_user_id_key;
       public         postgres    false    208    208            �
           2606    16492    participants participants_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.participants
    ADD CONSTRAINT participants_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.participants DROP CONSTRAINT participants_pkey;
       public         postgres    false    208            �
           2606    16494    places places_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.places
    ADD CONSTRAINT places_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.places DROP CONSTRAINT places_pkey;
       public         postgres    false    210            �
           2606    16496    posts posts_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.posts DROP CONSTRAINT posts_pkey;
       public         postgres    false    212            �
           2606    16498    users users_email_key 
   CONSTRAINT     Q   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);
 ?   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_key;
       public         postgres    false    216            �
           2606    16500    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    216            �
           2606    16502    users users_username_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_username_key;
       public         postgres    false    216            �
           2606    16503 ,   billing_address billing_address_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.billing_address
    ADD CONSTRAINT billing_address_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 V   ALTER TABLE ONLY public.billing_address DROP CONSTRAINT billing_address_user_id_fkey;
       public       postgres    false    200    216    2764            K   )   x���  �wnD���]��Ȕ<�.G��bl�x�O*�      M   �   x�]�A�@����jz�H!�uZb1W�]���%d�὏7�҅�6��5~ω�׏~�[R`�f���2�����x����F	8Eݻ��s���t���i���K0�5�AmB$f�Fq��\b�$�+I�؏+iH��Չ(��uGD/bF�      I   ,   x�3�4�4�t�J=:�H;��m�������e�i����qqq �Y      O   ]  x��T�n�6=�_1���F��8���{�6X���^��ВH�A�ǠA���g�[���Gَm�E�8��7S����)7L��PJN�5��r��ք��鑓"O���}RdE~����U�:Eo�X~�2��cEv��I�$��<�Ӫq�R'���F�!����m�n o�`�F��q��A�&W	8�<5�R\�H�nxz-W�ц*�ۈ��=��9�c��x�y�,͋4"6�`����`xJ:��X��l��J�7V4F  �w��7V�>)�)�1(��#HGz�֭l ���Y�{�������uG�qW�O3�A��嶺B]K�?��ePbC_��i����qy�P�B�xM��Q���2E����j_��C����Ja�|���k��Oi��+]žd��Udi�98���o/�m5ʢZ�0丳�w'*�����S)�贒�țqW�dDW����
#a��V����1���Q�ӣ���VR� �"ѶMz��p��ρi� Лؽ^��NC��3
jZ�q�z�}{i� �<�U\>��#Kn�_B�Q���bu.�~��G	�#j�L~��*����
n=�k	��cd��#c��*��p��q����9��o��>T:��g==UȀZ���y���q1H-AfO�����`�͟�ƃ��4�����< �ڨ0�0�eA��V�iB��Z�Sg��k��f܅!��-^u��]�FL�п�0���J/<}ͪ�b~�R[��X=�8����a�H����Ũ���"@�{�Ik�n��/Ժ�L���r�^�{�`�5J)==cy0\6�#:Bm(	C��瘆:�诔>#�^���?aQ_'���/��M\�˅p»��ZY|O�� �"�      Q      x�3�4�2�4����� ��      S   )  x�m��j�0ƯO�"{��Tco��m�y��LK��E�R{��u��Z���0���_��'�`���J���&�s�R�
�ڔhDɆ�M� >�$��m�;j%��
}����`�d.*x�̕�*\m��S?��;�pW���{\7�`G7y�1e�Fr��(r��6��B�Qt�lh;7���GQ
w���B�!8Y#�0T+ȑ�%7�gҶ��t�^Q��A� +
/6��^(吰�8o]-p������
ь���a�c�Ck�lA�8������ާ����	[t��}�����Q      U      x������ � �      W   E   x�3�N�IT0J�42ಀp���S�J��A�\�`QCSNS.C(π���\#�$s��qqq xSn      Y   Z  x�}UMk�F=˿�|��l/�o��MHa/5Rg��R��j!���op�3|ݛG�+�Z#������Q���{UY��zܹfPV���LW��m)W��ɴUh}O��}9hE����}�S��j���!J�v�u�C��$�H�e��e�Y~E��u�;ɖ�Mo����Y�����ڸq��:�v�%T�u���0g�CrC���;Ӓ~�k�����V����6c��'.R��}98�ġ�ڍ��?s���^{WN���$B?X��"�+��Z#	����UVmȢf�l+.�;�/�,O�XZ���՚�L\`s!��p�u ��C��$*~֑=U?SN��B� "ƿ;�e���~��<�@��^JqC�,rQ��/��_�ѓ�t�!��X��KЊx�`� TA[���`A���|I\��]����!{�o+Ws�g:�A��ɕ+���)�.�e&d� 뛽A�M0-k�/[�٬�:�ϲˣ�~H�x~��cd{U�O�k�fA& �Qʀ�w<�*]Iً&g~q<'_�K�^�{�d�7�*@��c���}!���A��Vވ<����^���
j{��r4�ph)��ո�zk]i@��w
ErDR�N J���Ib���45�kvұ�rF���:˓�[S�fR)`)}�E: G!�I�e�̐���u2��Ԋ���;� c�A� �s%O!#�R'��9��i&pԅ�V�@���; ��R���S��eqh_����n�h�iW�	�jA�+]l�H@���k5a�%���Q�7��Z�],��xA��uiUzp��X�@��j���D����*M����d�o���Z��`�,}ŸXɀ�m(6غ����-���u?��qn� ��jh�34�A淵��x��@@�^����*YNH	�P��t��*�d��r2/0߃���/�I���n\���1.���ȯW�ۀ��æA��Eӿ1>�T�^zm!R3��U����H��6�2&��8P���(���|�.�k4 �����1����x�pz+�iX���}j�}����f�w3�1ۼ��K�V����֓%pzz�/��������]z�]��'�ғ�� �1Y      [   ,   x�3�,JM/�I,�2��SJ2��2�K��9Sr3�b���� �J�      ]   %  x�m�ɶ�Z��92�eA����	�� �<�=I.�Zf�`��jWm��#�?����!?(N�R�U]�
=V�7b���Pk���c�jx��"�L���)�����*�w���%A@���M�,��N<XI/!�����sp_��J�k6�
�T�6n��y���]b���M�Է�� IH��@��\�9�{�1�TC2L^d�+N<�2ƭA���f|���ht��g���!������b�y�ΖM���9�M<-�%&� ~^�Fw�_�hd�Fg��%��:x�e�
�����&��k	O���2�#:;����B#���~&���@�TK
�O�;��bR�Նl<�J������9zU��}��xR���M������m�Ăw��\/ʈ}�õ1O�kJڕ�<xש� �y׍��!�?<^q�{�l�ll�Y<h�H��o9���@~:d�C
���N��,��+=ֹ���-�[3���p�X�G}.�d��h(xl+��=-՝U�nZǢ$�U��Q��&.׃@����D3�f�I�H���l&6�V��JE�E5)��˝"	�ue��'x��ѝUY��p{�`�{b���ѽ���xr��I���_`�9��=�sO�9ni�_l�*dM<|�y�6T���,�ȀzK�;�d[<I��Y��F;�/]'S|��y�����¯�2��?� |OP�8|>�͙����i���*���f�H$����u&�m�5�&�-�I�����E\����y5�%�|j]�BprN퐉���͞�*�^�~�[�ϔ�Me�^k��=0qh��p�,�b�4j�I�5�T���$��&�q#ۻX�Q�c���al���^2��8asW8Mr�g��5/ӉH��9c�ص~b;�>�{�u�*U,^�{�%Sib�i���,�n��%��С- `k���aK�?S�J��G��o����H)$�h2	����E]��gf6�
x�f���37%���Z�j_fn	L�_�
��?��M����=Ƭ�qb
|c�o��������@A     