import React from 'react';
import '../Styles/Footer.css'
import SocialFlow from './SocialFlow';
import { animateScroll as scroll } from "react-scroll";

const Footer = ({ id }) => {
    const scrollToTop = () => {
        scroll.scrollToTop();
    };

    return (
        <div className='Footer' id={id}>
            <nav className="navbar fixed-bottom navbar-expand-sm navbar-dark bg-dark">
                <span className="navbar-brand nav-item active" onClick={scrollToTop}>Kulturalna Aplikacja</span>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <span className="nav-link" onClick={scrollToTop}> GO TOP</span>
                    </li>
                </ul>

                <SocialFlow />

            </nav>

        </div >
    );
}

export default Footer;
