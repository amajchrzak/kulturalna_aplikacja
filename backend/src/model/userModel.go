package model

import (
	"database/sql"
	"net/http"

	"github.com/lib/pq"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"golang.org/x/crypto/bcrypt"
)

func InsertUser(user interfaces.User) (string, error) {
	_, err := DB.Exec("INSERT INTO users(email, username, password) VALUES($1, $2, $3)", user.Email, user.Username, user.Password)
	message := interfaces.MESSAGE
	if err, ok := err.(*pq.Error); ok {
		if err.Constraint == interfaces.USERS_EMAIL_KEY {
			message = "Email jest już zajęty"

		} else if err.Constraint == interfaces.USERS_USERNAME_KEY {

			message = "Login jest już zajęty"
		} else if err.Constraint == interfaces.UNIQUE_COLUMNS {

			message = "Email oraz Login są zajęte"
		} else {

			message = "Coś poszło nie tak"
		}
	}

	return message, err
}

func FindOneByUsername(username string, password string) (interfaces.User, int) {
	rows := DB.QueryRow("SELECT id, email, password, username, role, state FROM users WHERE username = $1", username)

	user := interfaces.User{}
	err := rows.Scan(&user.ID, &user.Email, &user.Password, &user.Username, &user.Role, &user.Status)
	switch {
	case err == sql.ErrNoRows:

		return user, http.StatusBadRequest
	case err != nil:

		return user, http.StatusBadRequest
	case err == nil:
		err = bcrypt.CompareHashAndPassword(user.Password, []byte(password))
		if err != nil {

			return user, http.StatusBadRequest
		}
	default:

		return user, http.StatusBadRequest
	}

	return user, http.StatusOK
}

func FindOneByID(id int) (interfaces.User, int) {
	rows := DB.QueryRow("SELECT id, email, password, username, role, state FROM users WHERE id = $1", id)

	user := interfaces.User{}

	err := rows.Scan(&user.ID, &user.Email, &user.Password, &user.Username, &user.Role, &user.Status)
	switch {
	case err == sql.ErrNoRows:

		return user, http.StatusBadRequest
	case err != nil:

		return user, http.StatusBadRequest
	}

	return user, http.StatusOK
}

func ChangePassword(password string, newPassword string, id int) int {
	_, err := DB.Exec("UPDATE users SET password = $1 WHERE password = $2 AND id = $3", newPassword, password, id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func CheckIfUserExists(id int) int {
	rows := DB.QueryRow("SELECT id FROM users WHERE id = $1", id)

	var check int
	err := rows.Scan(&check)
	if err != nil {
		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetRole(id int) (*interfaces.Role, int) {
	query := `
		SELECT roles.id, roles.name FROM users INNER JOIN roles ON roles.id = users.role WHERE users.id = $1
	`
	row := DB.QueryRow(query, id)

	role := &interfaces.Role{}

	err := row.Scan(&role.ID, &role.Name)
	switch {
	case err == sql.ErrNoRows:

		return nil, http.StatusBadRequest
	case err != nil:

		return nil, http.StatusBadRequest
	case err == nil:

		return role, http.StatusOK
	default:

		return nil, http.StatusBadRequest
	}
}

func UpdateResetPasswordToken(email string, genUUID string) int {
	_, err := DB.Exec("UPDATE users SET reset_password_token = $1 WHERE email = $2", genUUID, email)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func FindOneByUUID(token string) (interfaces.User, int) {
	rows := DB.QueryRow(`
		SELECT id, email, password, reset_password_token 
		FROM users 
		WHERE reset_password_token = $1`,
		token)

	user := interfaces.User{}

	err := rows.Scan(&user.ID, &user.Email, &user.Password, &user.ResetPasswordToken)
	switch {
	case err == sql.ErrNoRows:

		return user, http.StatusBadRequest
	case err != nil:

		return user, http.StatusBadRequest
	}

	return user, http.StatusOK
}

func ResetPasswordAndToken(hash string, id int) int {
	_, err := DB.Exec(`
		UPDATE users SET password = $1, reset_password_token = $2 
		WHERE id = $3
	`, hash, nil, id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetBillingAddress(id int) ([]interfaces.BillingData, int) {
	rows := DB.QueryRow("SELECT name, surname FROM billing_address WHERE user_id = $1", id)

	var billing_data []interfaces.BillingData
	var data interfaces.BillingData
	err := rows.Scan(&data.Name, &data.Surname)
	if err == sql.ErrNoRows {

		return billing_data, http.StatusOK
	}
	if err != nil {

		return billing_data, http.StatusBadRequest
	}
	billing_data = append(billing_data, data)

	return billing_data, http.StatusOK

}

func InsertBillingAddress(data interfaces.NewBillingData, id int) int {
	_, err := DB.Exec("INSERT INTO billing_address(user_id, name, surname) VALUES ($1, $2, $3)", id, data.Name, data.Surname)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func ChangeBillingAdress(data interfaces.NewBillingData, id int) int {
	_, err := DB.Exec("UPDATE billing_address SET name = $1, surname = $2 WHERE user_id = $3", data.Name, data.Surname, id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func CheckIfBillingAddressExists(id int) int {
	rows := DB.QueryRow("SELECT user_id FROM billing_address WHERE user_id = $1", id)

	var check int
	err := rows.Scan(&check)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func SetAccountAsInactive(id int) (int, string) {
	query := `
		UPDATE users 
		SET state = $1 
		WHERE id = $2`

	_, err := DB.Exec(query, interfaces.INACTIVE_USER, id)
	if err != nil {

		return http.StatusBadRequest, "Nie udało się usunąć konta"
	}

	return http.StatusOK, interfaces.MESSAGE
}
