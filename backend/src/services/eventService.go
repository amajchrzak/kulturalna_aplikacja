package services

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func ValidateEventData(formattedBody map[string]string) (int, string) {
	response := "SUCCESS"

	if len(fmt.Sprintf("%v", formattedBody["title"])) == 0 {
		response = "Wydarzenie musi mieć tytuł"

		return http.StatusBadRequest, response
	}

	if len(fmt.Sprintf("%v", formattedBody["description"])) == 0 {
		response = "Wydarzenie musi mieć opis"

		return http.StatusBadRequest, response
	}

	if len(formattedBody["price"]) != 0 {
		_, err := strconv.Atoi(formattedBody["price"])
		if err != nil {
			response = "Cena musi być liczbą"

			return http.StatusBadRequest, response
		}
	}

	if len(formattedBody["limit_of_participants"]) != 0 {
		limit, err := strconv.Atoi(formattedBody["limit_of_participants"])
		if err != nil {
			response = "Limit uczestników musi być liczbą"

			return http.StatusBadRequest, response
		}
		if limit < 0 {
			response = "Limit uczestników musi być liczbą większą od 0"

			return http.StatusBadRequest, response
		}
	}

	if len(formattedBody["place_id"]) == 0 {
		response = "Proszę zaznaczyć miejsce gdzie wydarzenie ma się odbyć."

		return http.StatusBadRequest, response
	}

	start, err := time.Parse(time.RFC3339, formattedBody["start_of_event"])
	if err != nil {
		response = "Niepoprawny format daty"

		return http.StatusBadRequest, response
	}

	end, err := time.Parse(time.RFC3339, formattedBody["end_of_event"])
	if err != nil {
		response = "Niepoprawny format daty"

		return http.StatusBadRequest, response
	}

	if end.Before(start) {
		response = "Data zakończenia nie może być wcześniejsza niż data rozpoczęcia"

		return http.StatusBadRequest, response
	}

	return http.StatusOK, response
}

func PrepareNewEvent(formattedBody map[string]string) interfaces.Event {
	var event interfaces.Event

	ParseEventData(&event, formattedBody)
	event.CreatedAt = time.Now()

	return event
}

func PrepareUpdatedEvent(formattedBody map[string]string) interfaces.Event {
	var event interfaces.Event

	ParseEventData(&event, formattedBody)

	return event
}

func ParseEventData(event *interfaces.Event, formattedBody map[string]string) {
	event.Title = formattedBody["title"]
	event.Description = formattedBody["description"]
	event.StartOfEvent, _ = time.Parse(time.RFC3339, formattedBody["start_of_event"])
	event.EndOfEvent, _ = time.Parse(time.RFC3339, formattedBody["end_of_event"])
	event.AuthorID, _ = strconv.Atoi(formattedBody["author_id"])
	event.PlaceID, _ = strconv.Atoi(formattedBody["place_id"])

	event.Price = NewNullString(formattedBody["price"])

	var amount int64
	event.AmountOfParticipants = NewNullInt(amount)

	var id int
	if len(formattedBody["instructor_id"]) != 0 {
		id, _ = strconv.Atoi(formattedBody["instructor_id"])
	}
	event.InstructorID = NewNullInt(int64(id))

	event.UpdatedAt = time.Now()
}

func ValidatePlaceData(formattedBody map[string]string) (int, string) {
	message := interfaces.MESSAGE

	if len(formattedBody["name_of_place"]) == 0 {

		return http.StatusBadRequest, "Nazwa miejsca jest wymagana"
	}

	return http.StatusOK, message
}

func PrepareNewPlace(formattedBody map[string]string) interfaces.Place {
	var place interfaces.Place

	place.NameOfPlace = formattedBody["name_of_place"]

	var limit int
	if len(formattedBody["limit_of_participants"]) != 0 {
		limit, _ = strconv.Atoi(formattedBody["limit_of_participants"])
	}
	place.LimitOfParticipants = NewNullInt(int64(limit))

	return place

}
