const DeleteUser = (userId, openModalDeleteUserOpen) => {
    return (
        <button
            className='btn btn-danger btn-lg btn-block deleteBtn'
            onClick={e => openModalDeleteUserOpen(e, userId)}
        >
            Usuń konto
        </button>
    )
}

export default DeleteUser;