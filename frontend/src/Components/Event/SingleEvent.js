import { handleFreeSlots, handlePrepareStartDate, handlePrepareEndDate } from "./PrepareDateToDisplay"
import { changeStyle } from "../../AuxilliaryFunctions/changeStyle"
import { isAdminOrReceptionist } from "../../AuxilliaryFunctions/isAdminOrReceptionist"

export const SingleEvent = ({ listEvents, event,
    handleRemoveEvent, handleAddOpinion, handleJoinEvent, msgNoData,
    cookieStyle
}) => {
    return (
        <div className={`${listEvents.length > 1 ? 'infoEvent' : 'infoEvent singleEvent'} ${changeStyle(cookieStyle, 'opinions')}`}>
            {Object.keys(event).length ?
                <div>
                    <p><span className='titleEventInfo'>{event?.Title ? event.Title : msgNoData}</span><span className='dateAdded'>{event?.CreatedAt ? event?.CreatedAt?.substring(0, 10) : msgNoData}</span></p>
                    <p className='authorEventInfo'>{event?.Author ? event.Author : msgNoData}</p>
                    <div className='infoDateAndLimit'>
                        <div className='date'>
                            <p>Data rozpoczęcia: {handlePrepareStartDate(event.StartOfEvent, msgNoData)}</p>
                            <p>Data zakończenia: {handlePrepareEndDate(event.EndOfEvent, msgNoData)}</p>
                        </div>
                        <div>
                            <p>Liczba wolnych miejsc: {handleFreeSlots(event.PlaceLimitOfParticipants, event.AmountOfParticipants, msgNoData)}</p>
                            <p>Liczba wszystkich miejsc: {event?.PlaceLimitOfParticipants ? event.PlaceLimitOfParticipants : msgNoData}</p>
                        </div>
                    </div>
                    <p>Miejsce: {event?.PlaceName ? event.PlaceName : msgNoData}</p>
                    <p>Instruktor: {event?.Instructor ? event.Instructor : msgNoData}</p>
                    <p>Wejściówka: {event?.Price ? event.Price + ' zł' : msgNoData}</p>
                    <div className='description'>Opis: {event?.Description ? event.Description : msgNoData}</div>
                    <div className="buttons">
                        {isAdminOrReceptionist() &&
                            <button onClick={() => handleRemoveEvent()} className='btn btn-danger eventButton'>Usuń wydarzenie</button>
                        }
                        <button onClick={() => handleAddOpinion()} className='btn btn-primary eventButton'>Dodaj opinię</button>
                        <button onClick={() => handleJoinEvent()} className='btn btn-success eventButton'>Zapisz się</button>
                    </div>
                </div>
                :
                null
            }
        </div>
    )
}