import '../../Styles/AdminPanel.css';
import Table from './UserManagement/Table';
import { useMemo } from 'react';
import PrepareData from './UserManagement/PrepareData';
import axios from "axios";
import { useState, useEffect } from 'react';
import ApproveModal from './UserManagement/Modals/ApproveModal';
import LoginPanel from '../LoginPanel';
import handleIsToken from '../../AuxilliaryFunctions/handleIsToken';
import { nameHost } from '../../GlobalVariables/GlobalVariables';
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../Notification/Notification';
import { changeStyle } from '../../AuxilliaryFunctions/changeStyle';
import { useHistory } from "react-router-dom";


const AdminPanel = ({ cookieStyle }) => {

    let history = useHistory();

    const [userManagementShow, setUserManagementShow] = useState(false);
    const [users, setUsers] = useState([]);
    const [foundUsers, setFoundUsers] = useState([]);
    const [roles, setRoles] = useState([]);
    const [loginUser, setLoginUser] = useState("");

    const [modalAssignRoleOpen, setModalAssignRoleOpen] = useState(false);
    const [assignRole, setAssignRole] = useState({
        userId: "",
        roleId: ""
    })
    const [modalDeleteUserOpen, setModalDeleteUserOpen] = useState(false);
    const [idDeletedUser, setIdDeletedUser] = useState("");
    const [refresh, setRefresh] = useState(false);

    const handleSearchUser = login => {
        if (!login.length) return setFoundUsers(users);

        // eslint-disable-next-line array-callback-return
        const foundUsersArray = users.filter(user => {
            if (user.Username.toLowerCase().indexOf(login.toLowerCase()) !== -1) {
                return user;
            }
        });
        setFoundUsers(foundUsersArray);
    }

    const handleDisplayUserManagement = () => {
        return (
            <div>
                <h1>Zarządzanie użytkownikami</h1>
                <div>
                    <h2>Wyszukaj użytkownika po loginie</h2>
                    <label>Podaj login
                        <input id="userName" type="text" placeholder="User login" value={loginUser} onChange={e => {
                            setLoginUser(e.target.value)
                            handleSearchUser(e.target.value);
                        }}></input>
                    </label>
                </div>
                {foundUsers.length ? null :
                    <div className='userNotFound'>
                        Nie znaleziono użytkownika o podanym loginie
                    </div>
                }
                <div>
                    <Table columns={columns} data={data} />
                </div>
            </div>
        )
    }

    const handleGetUsersAndRoles = () => {
        const token = localStorage.getItem("token");
        // const url = "https://mocki.io/v1/2174eebd-4573-4392-9113-699ef5145bff";
        axios.get(`${nameHost}/get-users`, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    setRoles(res.data.roles);
                    setUsers(res.data.users);
                    setFoundUsers(res.data.users);
                } else {
                    setRoles([]);
                    setUsers([]);
                    setFoundUsers([]);
                }
            })
            .catch(err => {
                console.error('Error', err);
                setRoles([]);
                setUsers([]);
                setFoundUsers([]);
            });
    }

    useEffect(() => {
        handleGetUsersAndRoles();

        return () => { //cleanup function
            setRoles([]);
            setUsers([]);
            setFoundUsers([]);
        };
    }, [refresh])

   const PanelPostComponent = () => {
    history.push({ 
        pathname: '/panel_post'
        
       })

   }

   const PanelEventComponent = () => {
    history.push({ 
        pathname: '/management-panel'
        
       })
    }


    const handleAssignRole = e => {
        e.preventDefault();

        const token = localStorage.getItem("token");
        axios.patch(`${nameHost}/update-role/${assignRole.userId}`, {
            role: parseInt(assignRole.roleId)
        }, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    setRefresh(prevState => !prevState);
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'success',
                        title: 'Rola została przypisana',
                        message: 'Przypisanie roli zakończone powodzeniem.'
                    });
                } else {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'danger',
                        title: 'Rola nie została przypisana',
                        message: 'Przypisanie roli zakończone niepowodzeniem.'
                    });
                }
                setModalAssignRoleOpen(false);
            })
            .catch(err => {
                console.error("Error", err);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Rola nie została przypisana',
                    message: 'Przypisanie roli zakończone niepowodzeniem.'
                });
                setModalAssignRoleOpen(false);
            })
    }

    const openModalAssignRoleOpen = (e, role, userId) => {
        e.preventDefault();
        setModalAssignRoleOpen(true);
        setAssignRole({ userId: userId, roleId: role })
    }

    const handleDeleteUser = e => {
        e.preventDefault();

        const token = localStorage.getItem("token");
        axios.patch(`${nameHost}/delete-user/${idDeletedUser}`, {}, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'success',
                        title: 'Użytkownik usunięty',
                        message: 'Usuwanie użytkownika zakończone powodzeniem.'
                    });
                    setRefresh(prevState => !prevState);
                } else {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'danger',
                        title: 'Nie udało się usunąć użytkownika',
                        message: 'Usuwanie użytkownika zakończone niepowodzeniem.'
                    });
                }
                setModalDeleteUserOpen(false);
            })
            .catch(err => {
                console.error("Error", err);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Nie udało się usunąć użytkownika',
                    message: 'Usuwanie użytkownika zakończone niepowodzeniem.'
                });
                setModalDeleteUserOpen(false);
            });
    }

    const openModalDeleteUserOpen = (e, userId) => {
        e.preventDefault();
        setModalDeleteUserOpen(true);
        setIdDeletedUser(userId);
    }

    const columns = useMemo(
        () => [
            {
                Header: 'Dane użytkownika',
                columns: [
                    {
                        Header: 'ID',
                        accessor: 'userId',
                    },
                    {
                        Header: 'Login użytkownika',
                        accessor: 'userLogin',
                    },
                    {
                        Header: 'Rola',
                        accessor: 'userRole',
                    }
                ],
            },
            {
                Header: 'Akcje',
                columns: [
                    {
                        Header: 'Przypisz rolę',
                        accessor: 'assignRole',
                    },
                    {
                        Header: 'Usuń konto',
                        accessor: 'deleteUser',
                    }
                ],
            },
        ],
        []
    )

    const data = useMemo(() => PrepareData(foundUsers, roles, openModalAssignRoleOpen, openModalDeleteUserOpen, foundUsers.length), [foundUsers, roles])

    return (
        <>
            {handleIsToken() ?
                <div className='adminPanel'>
                    <div className={`option ${changeStyle(cookieStyle, 'option')}`}>
                        {userManagementShow ?
                            <div>
                                {
                                    users.length ?
                                        handleDisplayUserManagement()
                                        :
                                        <span>
                                            Brak pobranych danych o użytkownikach
                                        </span>
                                }
                            </div>
                            :
                            <h1 className='optionTitle'>
                                Wybierz opcję dostępną na panelu zarządzania
                            </h1>
                        }
                        {
                            modalAssignRoleOpen && <ApproveModal
                                isOpen={modalAssignRoleOpen}
                                closeModal={setModalAssignRoleOpen}
                                contentLabel="Assign role"
                                approve={handleAssignRole}
                                title='Czy przypisać rolę?'
                                nameBtn='Przypisz rolę'
                            />
                        }
                        {
                            modalDeleteUserOpen && <ApproveModal
                                isOpen={modalDeleteUserOpen}
                                closeModal={setModalDeleteUserOpen}
                                contentLabel="Delete user"
                                approve={handleDeleteUser}
                                title='Czy usunąć użytkownika?'
                                nameBtn='Usuń użytkownika'
                            />
                        }
                    </div>
                    <div className={`managementPanel ${changeStyle(cookieStyle, 'managementPanel')}`}>
                        <div>
                            <h1>PANEL ZARZĄDZANIA</h1>
                            <button name="userManagement" className='btn btn-primary btn-lg btn-block' onClick={() => setUserManagementShow(true)}>
                                Zarządzanie użytkownikami
                            </button>
                        </div>
                        <br/> <div>
                            <button className='btn btn-primary btn-lg btn-block' onClick={() => PanelPostComponent()}>
                               Panel zarządzania wpisów
                            </button>
                        </div>
                        <br/> <div>
                            <button className='btn btn-primary btn-lg btn-block' onClick={() => PanelEventComponent()}>
                               Panel wydarzeń
                            </button>
                        </div>
                    </div>
                </div>
                :
                <LoginPanel />
            }
        </>
    )
}

export default AdminPanel;