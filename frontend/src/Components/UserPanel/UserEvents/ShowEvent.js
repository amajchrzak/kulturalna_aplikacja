const ShowEvent = (goEvent, dateStartEvent, eventId) => {
    return (
        <button onClick={e => goEvent(e, dateStartEvent, eventId)} className='btn btn-primary btn-lg btn-block'>Przejdź do wydarzenia</button>
    )
}

export default ShowEvent;