import React, {useState} from 'react';
import FormSignup from './FormSignup';


const RegistrationPanel = () => {
    
    const [formIsSubmitted, setFormIsSubmitted] = useState(false);
    const submitForm =  () => {
        setFormIsSubmitted(true);
    }
        return (
        <div>
           {/*!formIsSubmitted ? <FormSignup submitForm={submitForm}/> : null */}

            {<FormSignup submitForm={submitForm}/>}
        </div>
    )

};

export default RegistrationPanel
