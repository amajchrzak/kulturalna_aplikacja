export const isAdmin = () => {
    const role = localStorage.getItem('role_id');

    if (String(role) === '3') {
        return true;
    } else return false;
}