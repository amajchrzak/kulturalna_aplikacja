package services

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func PrepareJWTToken(user *interfaces.User) (string, int) {
	tokenContent := jwt.MapClaims{
		"user_id": user.ID,
		"expire":  time.Now().Add(time.Minute * 60).Unix(),
	}

	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenContent)
	token, err := jwtToken.SignedString(interfaces.JWT_KEY)
	if err != nil {
		message := "Error Sighning token"

		return message, http.StatusFailedDependency
	}

	return token, http.StatusOK
}

// redirect won't work as intended, so just return status 401 and leave rest to the frontend ?
func Middleware(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			handlerFunc.ServeHTTP(w, r)
		} else {
			authHeader := strings.Split(r.Header.Get("Authorization"), "Bearer ")
			if len(authHeader) != 2 {
				w.WriteHeader(http.StatusUnauthorized)
			} else {
				jwtToken := authHeader[1]

				token, _ := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {

						return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
					}

					return interfaces.JWT_KEY, nil
				})

				if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
					ctx := context.WithValue(r.Context(), "props", claims)
					handlerFunc.ServeHTTP(w, r.WithContext(ctx))
				} else {
					w.WriteHeader(http.StatusUnauthorized)
				}
			}
		}

	}
}

func ExtractClaims(tokenStr string) (interface{}, int) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {

		return interfaces.JWT_KEY, nil
	})
	if err != nil {

		return nil, http.StatusUnauthorized
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {

		return claims["user_id"], http.StatusOK
	} else {

		return nil, http.StatusUnauthorized
	}
}
