package main

import (
	_ "github.com/lib/pq"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/routes"
)

func main() {
	model.ConnectDB()
	routes.StartApp()
	model.CloseDB()
}
