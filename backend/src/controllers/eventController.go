package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/helpers"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/services"
)

func AddEvent(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status, message := services.ValidateEventData(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"message": message,
			})

			return
		}

		event := services.PrepareNewEvent(formattedBody)
		status = model.InsertEvent(event)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func DeleteEvent(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		requestedID := mux.Vars(r)
		id, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status := model.DeleteEvent(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func GetEvents(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		events, status := model.GetEvents()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"events":  events,
			"message": interfaces.MESSAGE,
		})
	}
}

func GetEventsFromDay(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		start_before, err := time.Parse(time.RFC3339, formattedBody["start_of_event"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println(err)
			return
		}
		end_after, err := time.Parse(time.RFC3339, formattedBody["end_of_event"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println(err)

			return
		}

		events, status := model.GetEventsByDay(start_before, end_after)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"events":  events,
			"message": interfaces.MESSAGE,
		})
	}
}

func GetInstructors(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		instructors, status := model.GetInstructors()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"instructors": instructors,
			"message":     interfaces.MESSAGE,
		})
	}
}

func UpdateEvent(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status, message := services.ValidateEventData(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"message": message,
			})

			return
		}

		event := services.PrepareUpdatedEvent(formattedBody)
		requestedID := mux.Vars(r)
		id, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status = model.UpdateEvent(id, event)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func RegisterOnEvent(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		eventID := mux.Vars(r)
		id, err := strconv.Atoi(eventID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		loggedUserID := helpers.GetLoggedUserID(r)

		status := model.CheckParticipants(id, loggedUserID)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		message, status := model.InsertEventParticipants(id, loggedUserID)
		if status != http.StatusOK {
			if status == http.StatusConflict {
				json.NewEncoder(w).Encode(map[string]interface{}{
					"message": message,
				})

				return
			}
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status = model.IncreaseAmountOfParticipants(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func AddPlace(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status, message := services.ValidatePlaceData(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"message": message,
			})

			return
		}

		place := services.PrepareNewPlace(formattedBody)
		status = model.InsertPlace(place)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})

	}
}

func GetPlaces(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		places, status := model.GetPlaces()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"places":  places,
			"message": interfaces.MESSAGE,
		})
	}

}

func UpdatePlace(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status, message := services.ValidatePlaceData(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"message": message,
			})

			return
		}

		place := services.PrepareNewPlace(formattedBody)
		requestedID := mux.Vars(r)
		id, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status = model.UpdatePlace(id, place)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func DeletePlace(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		requestedID := mux.Vars(r)
		id, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status := model.DeletePlace(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}
