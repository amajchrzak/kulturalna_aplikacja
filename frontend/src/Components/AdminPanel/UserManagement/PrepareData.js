import DeleteUser from "./DeleteUser";
import SelectRole from "./SelectRole";

const range = len => {
    const arr = [];
    for (let i = 0; i < len; i++) {
        arr.push(i);
    }
    return arr;
}

const nextUser = (user, roles, openModalAssignRoleOpen, openModalDeleteUserOpen) => {
    return {
        userId: user.ID,
        userLogin: user.Username,
        userRole: user.Role,
        assignRole: SelectRole(user.ID, roles, openModalAssignRoleOpen),
        deleteUser: DeleteUser(user.ID, openModalDeleteUserOpen)
    }
}

const PrepareData = (users, roles, openModalAssignRoleOpen, openModalDeleteUserOpen, ...lens) => {
    const makeDataLevel = (depth = 0) => {
        const len = lens[depth];
        return range(len).map(numberUser => {
            return {
                ...nextUser(users[numberUser], roles, openModalAssignRoleOpen, openModalDeleteUserOpen),
                subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined
            }
        })
    }
    return makeDataLevel();
}

export default PrepareData;