package interfaces

import (
	"database/sql"
	"encoding/json"
	"time"
)

type User struct {
	ID                 uint64
	Email              string
	Username           string
	Password           []byte `json:"-"`
	Role               int
	ResetPasswordToken string
	Status             bool
}

type Login struct {
	Username string
	Password string
}

type ChangePassword struct {
	UserID             uint64
	OldPassword        string
	NewPassword        string
	RetypedNewPassword string
}

type Instructor struct {
	ID      uint64
	Name    string
	Surname string
}

type ShortEvent struct {
	ID           uint64
	Title        string
	StartOfEvent time.Time
	EndOfEvent   time.Time
}

type Event struct {
	ID                   uint64
	Title                string
	Description          string
	Price                NullString
	PlaceID              int
	AmountOfParticipants NullInt64
	CreatedAt            time.Time
	UpdatedAt            time.Time
	StartOfEvent         time.Time
	EndOfEvent           time.Time
	AuthorID             int
	InstructorID         NullInt64
}

type EventDetailed struct {
	Event
	Author                   string
	Instructor               NullString
	PlaceName                string
	PlaceLimitOfParticipants NullInt64
}

type NullInt64 struct {
	sql.NullInt64
}

func (ni *NullInt64) MarshalJSON() ([]byte, error) {
	if !ni.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(ni.Int64)
}

type NullString struct {
	sql.NullString
}

func (ns *NullString) MarshalJSON() ([]byte, error) {
	if !ns.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(ns.String)
}

type NullTime struct {
	sql.NullTime
}

func (nt *NullTime) MarshalJSON() ([]byte, error) {
	if !nt.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(nt.Time)
}

type Opinion struct {
	ID          int
	EventID     int
	UserId      int
	Title       string
	Description string
	CreatedAt   time.Time
}

type OpinionDetailed struct {
	Opinion
	OpinionAuthor NullString
}

type Place struct {
	ID                  int
	NameOfPlace         string
	LimitOfParticipants NullInt64
}

type Author struct {
	ID     int
	UserID int
}

type Role struct {
	ID   int
	Name string
}

type Participant struct {
	EventID int
	UserID  int
}

type Email struct {
	Email string
}

type GroupEmail struct {
	Emails  []string `json:"emails"`
	Message string   `json:"message"`
	Subject string   `json:"title"`
}

type ResetPasword struct {
	NewPassword        string
	RetypedNewPassword string
	SentToken          string
}

type BillingData struct {
	Name    NullString
	Surname NullString
}

type NewBillingData struct {
	Name    string
	Surname string
}

type BillingAddress struct {
	ID      int
	UserId  int
	Name    NullString
	Surname NullString
}

type Post struct {
	ID          int
	Title       string
	Description string
	AuthorID    int
	CreatedAt   time.Time `json:"-"`
}

type PostDetailed struct {
	Post
	Author string
}
