export const notificationTemplate = {
    insert: "top",
    container: "top-right",
    animationIn: ["animate__animated", "animate__fadeIn"],
    animationOut: ["animate__animated", "animate__fadeOut"],
    dismiss: {
        duration: 5000,
        onScreen: true
    }
}

//Info about react-notifications-component
// https://www.npmjs.com/package/react-notifications-component