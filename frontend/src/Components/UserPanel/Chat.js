import { useState, useEffect, useRef } from 'react';
import { db } from '../../firebase';
import SendMessages from './SendMessages';
import UserList from './UserList';
import Message from './Message';
import { nameHost } from '../../GlobalVariables/GlobalVariables';
import axios from 'axios';
import '../../Styles/Chat.css';

const Chat = ({ setReceiverName }) => {

    const [messagesSenderReceiver, setMessagesSenderReceiver] = useState([]);
    const [messagesReceiverSender, setMessagesReceiverSender] = useState([]);
    const [receiver, setReceiver] = useState('');
    const [users, setUsers] = useState([]);
    const conversation = db.collection('messages');

    const positionScrollbar = useRef();

    const getMessages = () => {
        const sender = localStorage.getItem('user_id') + "";
        const receiverMess = receiver + "";

        conversation
            .where('sender', '==', sender)
            .where('receiver', '==', receiverMess)
            .orderBy('createdAt')
            .onSnapshot(snapshot => {
                const senderReceiver = snapshot.docs.map(doc => doc.data()).filter(mess => mess.createdAt !== null);
                setMessagesSenderReceiver(senderReceiver);
            })

        conversation
            .where('sender', '==', receiverMess)
            .where('receiver', '==', sender)
            .orderBy('createdAt')
            .onSnapshot(snapshot => {
                const receiverSender = snapshot.docs.map(doc => doc.data()).filter(mess => mess.createdAt !== null);
                setMessagesReceiverSender(receiverSender);
            })
    }

    const handleReceiverName = () => {
        const nameReceiver = users.find(user => user.ID === receiver);
        if (nameReceiver) setReceiverName(nameReceiver.Username);
    }

    useEffect(() => {
        getMessages();
        handleReceiverName();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [receiver]);

    const compareCreatedAt = (a, b) => {
        return a.createdAt.seconds - b.createdAt.seconds;
    }

    const handleGetMessagesArray = () => {
        const messagesArray = messagesSenderReceiver.concat(messagesReceiverSender);
        if (messagesArray.length) {
            messagesArray.sort(compareCreatedAt);
            return messagesArray;
        } else {
            return [{ text: "Wyślij wiadomość" }];
        }
    }

    const scrollToBottom = () => {
        positionScrollbar.current.scrollIntoView({
            behavior: "smooth",
            block: "nearest",
            inline: "start"
        });
    }

    useEffect(() => {
        scrollToBottom();
    }, [messagesSenderReceiver, messagesReceiverSender])

    const handleGetUsers = () => {
        const token = localStorage.getItem("token");
        axios.get(`${nameHost}/get-users`, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    setUsers(res.data.users)
                } else {
                    setUsers([]);
                }
            })
            .catch(err => {
                console.error('Error', err);
                setUsers([]);
            });
    }

    useEffect(() => {
        handleGetUsers();
        return () => { //cleanup function
            setUsers([]);
        };
    }, [])

    return (
        <div className='chat'>
            <div>
                <div className='chat__message'>
                    <Message handleGetMessagesArray={handleGetMessagesArray} />
                    <div ref={positionScrollbar}></div>
                </div>
                <SendMessages receiver={receiver} />
            </div>
            <div className='chat__user-list'>
                <UserList setReceiver={setReceiver} users={users} />
            </div>
        </div>
    )
}

export default Chat;