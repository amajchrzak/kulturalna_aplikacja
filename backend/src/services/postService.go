package services

import (
	"time"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func PreparePost(formattedBody map[string]string, id int) interfaces.Post {
	var post interfaces.Post

	post.AuthorID = id
	post.Title = formattedBody["title"]
	post.Description = formattedBody["description"]
	post.CreatedAt = time.Now()

	return post
}
