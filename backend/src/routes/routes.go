package routes

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/controllers"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/services"
)

func StartApp() {
	router := mux.NewRouter()
	router.HandleFunc("/register", controllers.Register).Methods("POST", "OPTIONS")
	router.HandleFunc("/login", controllers.Login).Methods("POST", "OPTIONS")
	router.HandleFunc("/change-password/{id}", services.Middleware(controllers.ChangePassword)).Methods("POST", "OPTIONS")
	router.HandleFunc("/send-reset-password-procedure", controllers.SendResetPasswordProcedure).Methods("POST", "OPTIONS")
	router.HandleFunc("/reset-password", controllers.ResetPasword).Methods("POST", "OPTIONS")
	router.HandleFunc("/delete-own-account", services.Middleware(controllers.DeleteOwnAccount)).Methods("DELETE", "OPTIONS")

	router.HandleFunc("/get-logged-user", services.Middleware(controllers.GetLoggedUser)).Methods("GET", "OPTIONS")

	router.HandleFunc("/add-event", services.Middleware(controllers.AddEvent)).Methods("POST", "OPTIONS")
	router.HandleFunc("/remove-event/{id}", services.Middleware(controllers.DeleteEvent)).Methods("DELETE", "OPTIONS")
	router.HandleFunc("/get-events", services.Middleware(controllers.GetEvents)).Methods("GET", "OPTIONS")
	router.HandleFunc("/get-events-day", services.Middleware(controllers.GetEventsFromDay)).Methods("POST", "OPTIONS")
	router.HandleFunc("/update-event/{id}", services.Middleware(controllers.UpdateEvent)).Methods("PATCH", "OPTIONS")

	router.HandleFunc("/get-instructors", services.Middleware(controllers.GetInstructors)).Methods("GET", "OPTIONS")

	router.HandleFunc("/add-place", services.Middleware(controllers.AddPlace)).Methods("POST", "OPTIONS")
	router.HandleFunc("/get-places", services.Middleware(controllers.GetPlaces)).Methods("GET", "OPTIONS")
	router.HandleFunc("/update-place/{id}", services.Middleware(controllers.UpdatePlace)).Methods("PATCH", "OPTIONS")
	router.HandleFunc("/delete-place/{id}", services.Middleware(controllers.DeletePlace)).Methods("DELETE", "OPTIONS")

	router.HandleFunc("/add-opinion", services.Middleware(controllers.AddOpinion)).Methods("POST", "OPTIONS")
	router.HandleFunc("/get-opinions/{event_id}", services.Middleware(controllers.GetOpinionsAboutEvent)).Methods("GET", "OPTIONS")
	router.HandleFunc("/delete-own-opinion/{event_id}", services.Middleware(controllers.DeleteOwnOpinion)).Methods("DELETE", "OPTIONS")
	router.HandleFunc("/delete-opinion/{opinion_id}", services.Middleware(controllers.DeleteOpinion)).Methods("DELETE", "OPTIONS")

	router.HandleFunc("/update-role/{user_id}", services.Middleware(controllers.UpdateRole)).Methods("PATCH", "OPTIONS")
	router.HandleFunc("/register-on-event/{id}", services.Middleware(controllers.RegisterOnEvent)).Methods("PUT", "OPTIONS")
	router.HandleFunc("/resign-from-participation/{id}", services.Middleware(controllers.ResignFromParticipation)).Methods("DELETE", "OPTIONS")
	router.HandleFunc("/get-users", controllers.GetUsers).Methods("GET", "OPTIONS")
	router.HandleFunc("/delete-user/{user_id}", controllers.DeleteUser).Methods("PATCH", "OPTIONS")

	router.HandleFunc("/send-group-email", controllers.SendGroupEmail).Methods("POST", "OPTIONS")

	router.HandleFunc("/get-profile-data", services.Middleware(controllers.GetUserProfileData)).Methods("GET", "OPTIONS")
	router.HandleFunc("/update-profile-data", services.Middleware(controllers.ChangeProfileData)).Methods("PATCH", "OPTIONS")

	router.HandleFunc("/add-post", services.Middleware(controllers.AddPost)).Methods("POST", "OPTIONS")
	router.HandleFunc("/get-posts", controllers.GetPosts).Methods("GET", "OPTIONS")
	router.HandleFunc("/get-post/{id}", controllers.GetPost).Methods("GET", "OPTIONS")
	router.HandleFunc("/update-post/{id}", services.Middleware(controllers.UpdatePost)).Methods("PATCH", "OPTIONS")
	router.HandleFunc("/delete-post/{id}", services.Middleware(controllers.DeletePost)).Methods("DELETE", "OPTIONS")

	log.Fatal(http.ListenAndServe(":8080", router))
}
