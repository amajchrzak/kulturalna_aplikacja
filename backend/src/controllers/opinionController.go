package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/helpers"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/services"
)

func AddOpinion(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusNotAcceptable)

			return
		}

		message, status := services.ValidateOpinion(formattedBody["event_id"], formattedBody["title"])
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{
				"message": message,
			})

			return
		}

		userID := helpers.GetLoggedUserID(r)
		opinion := services.PrepareOpinion(formattedBody["event_id"], formattedBody["title"], formattedBody["description"], userID)

		message, err = model.InsertOpinion(opinion)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{
				"message": message,
			})

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})

	}
}

func GetOpinionsAboutEvent(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		requestedID := mux.Vars(r)
		id, err := strconv.Atoi(requestedID["event_id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		opinions, status := model.GetOpinionsByEventID(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"opinions": opinions,
			"message":  interfaces.MESSAGE,
		})
	}
}

func DeleteOwnOpinion(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		requested_id := mux.Vars(r)
		event_id, err := strconv.Atoi(requested_id["event_id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		user_id := helpers.GetLoggedUserID(r)
		status := model.DeleteOpinionWithUserID(event_id, int(user_id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})

	}
}

func DeleteOpinion(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		requested_id := mux.Vars(r)
		opinion_id, err := strconv.Atoi(requested_id["opinion_id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status := model.DeleteOpinionWithID(opinion_id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})

	}
}
