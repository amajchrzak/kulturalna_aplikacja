import React, { useState } from 'react';
import '../Styles/LoginPanel.css'
import axios from 'axios';
import LoginForm from './Forms/LoginForm';
import UserPanel from './UserPanel/UserPanel';
import { store } from 'react-notifications-component';
import { notificationTemplate } from './Notification/Notification';
import handleIsToken from '../AuxilliaryFunctions/handleIsToken.js';
import { useHistory } from "react-router-dom";
import { nameHost } from '../GlobalVariables/GlobalVariables';


const LoginPanel = ({ userData, setUserData }) => {

    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [blocked, setBlocked] = useState(false); //when user are blocked

    const history = useHistory();


    const handleClearForm = () => {
        console.info("###CLEAR FORM PROCESS");
        setUserName("");
        setPassword("");
    }

    const handleRedirectToResetPass = () => {
        console.log("###RESET PASSWORD")
        history.push("/password-reset");
    }

    const handleBlockedUser = () => {
        setBlocked(true);

        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Konto zablokowane',
            message: `Twoje konto zostało zablokowane. 
            Jeśli zapomniałeś hasła skorzystaj z opcji Resetuj hasło.`,
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        })
    }

    const handleLogin = e => {
        console.info("###LOGIN PROCESS");
        e.preventDefault();
        handleClearForm();

        const url = nameHost + "/login";
        // const addressUrlLogin = "https://mocki.io/v1/2a05da08-55ce-4b97-94ea-06d3206ff779"; //SUCCESS
        // const addressUrlLogin = "https://mocki.io/v1/2a05da08-55ce-4b97-94ea-06d3206ff779"; //SUCCESS
        // const addressUrlLogin = "https://mocki.io/v1/57c62aa3-e5c6-4881-93a6-e0df6419360c"; //FAIL
        // const addressUrlLogin = "https://mocki.io/v1/53261af9-1400-4d66-a802-ed2fe0d75af7"; //user blocked
        axios.post(url, {
            username: userName,
            password: password
        }).then(res => {
            console.info("###Response from login endpoint:", res);
            if (res.status === 200) {
                if (res.data.info === "user_blocked") {
                    console.info("###USER BLOCKED")
                    handleBlockedUser();
                } else if (res.data.message === "SUCCESS") {
                    setBlocked(false);

                    localStorage.setItem("token", res.data.jwt);
                    localStorage.setItem("user", res.data.username);
                    localStorage.setItem("user_id", res.data.id);
                    localStorage.setItem("role_id", res.data.role.ID);

                    setUserData(res.data);

                    store.addNotification({
                        ...notificationTemplate,
                        type: 'success',
                        title: 'Zalogowano pomyślnie',
                        message: 'Witaj w Kulturalnej Aplikacji'
                    })
                } else if (res.data.message === "FAIL") {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'warning',
                        title: 'Konto nieaktywne',
                        message: 'Nie udało się zalogować.'
                    })
                } else {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'warning',
                        title: 'Nie zalogowano',
                        message: 'Nie udało się zalogować.'
                    })
                }
            }
        }).catch(err => {
            console.error("Error from login endpoint", err);

            store.addNotification({
                ...notificationTemplate,
                type: 'danger',
                title: 'Błędne dane',
                message: 'Nie udało się zalogować.'
            })
        });
    };
    return (
        <>
            {(handleIsToken() && localStorage.getItem("user")) ?
                <UserPanel userData={userData} />
                :
                <LoginForm
                    userName={userName}
                    password={password}
                    handleLogin={handleLogin}
                    setUserName={setUserName}
                    setPassword={setPassword}
                    blocked={blocked}
                    handleRedirectToResetPass={handleRedirectToResetPass}
                />
            }
        </>
    );
}

export default LoginPanel;