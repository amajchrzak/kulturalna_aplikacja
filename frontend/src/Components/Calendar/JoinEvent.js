import Modal from 'react-modal';
import JoinEventForm from '../Forms/JoinEventForm';

const JoinEvent = props => {

    const customStyles = {
        content: {
            minWidth: '450px',
            minHeight: '225px',
            top: '50%',
            left: '50%',
            right: '75%',
            height: '20%',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            backgroundColor: "rgb(48, 52, 92)",
            color: "white"
        },
    };

    Modal.setAppElement('#root');

    return (
        <>
            <Modal
                isOpen={props.isOpen}
                onRequestClose={props.onRequestClose}
                style={customStyles}
            >
                <JoinEventForm
                    handleJoinEvent={props.handleJoinEvent}
                    closeModal={props.closeModal}
                />
            </Modal>
        </>
    )
}

export default JoinEvent;