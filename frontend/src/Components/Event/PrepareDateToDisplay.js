export const handlePrepareStartDate = (startOfEvent, msgNoData) => {

    if (startOfEvent) {

        return `${startOfEvent.substring(0, 10)}, ${startOfEvent.substring(11, 16)}`;
    }

    return msgNoData;
}

export const handlePrepareEndDate = (endOfDate, msgNoData) => {
    if (endOfDate) {

        return `${endOfDate?.substring(0, 10)}, ${endOfDate?.substring(11, 16)}`;
    }

    return msgNoData;
}

export const handleFreeSlots = (limitOfParticipants, amountOfParticipants, msgNoData) => {
    if (limitOfParticipants) {
        if (amountOfParticipants) {

            return limitOfParticipants - amountOfParticipants;
        }

        return limitOfParticipants;
    }

    return msgNoData;
}