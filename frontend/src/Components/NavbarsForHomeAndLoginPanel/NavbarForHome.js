import React from "react";
import logo from "../../Photos/logo.png";
import { Link, animateScroll as scroll } from "react-scroll";
import { Link as LinkReactRouterDom } from "react-router-dom";
import { changeStyle } from "../../AuxilliaryFunctions/changeStyle";

const NavbarForHome = ({ cookieStyle }) => {
    const scrollToTop = () => {
        scroll.scrollToTop();
    };

    const setStyle = () => {
        return `nav-item ${changeStyle(cookieStyle, 'linkOtherSection')}`;
    }

    return (
        <nav className={`nav ${changeStyle(cookieStyle, 'nav')}`} id="navbar">
            <div className="nav-content">
                <img
                    src={logo}
                    className="nav-logo"
                    alt="Logo"
                    onClick={scrollToTop}
                />
                <ul className="nav-items">
                    <li className="nav-item home-button"
                        onClick={scrollToTop}
                    >
                        Home
                    </li>
                    <li className={setStyle()}>
                        <Link
                            activeClass="active"
                            to="IncomingEvents"
                            spy={true}
                            smooth={true}
                            offset={-70}
                            duration={400}
                        >
                            Nadchodzące wydarzenia
                        </Link>
                    </li>
                    <li className={setStyle()}>
                        <Link
                            activeClass="active"
                            to="About"
                            spy={true}
                            smooth={true}
                            offset={-70}
                            duration={400}
                        >
                            O nas
                        </Link>
                    </li>
                    <li className={setStyle()}>
                        <Link
                            activeClass="active"
                            to="Contact"
                            spy={true}
                            smooth={true}
                            offset={-70}
                            duration={400}
                        >
                            Kontakt
                        </Link>
                    </li>
                    <li className='nav-item'>
                        <LinkReactRouterDom to="/login-panel" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`}>
                            Panel użytkownika
                        </LinkReactRouterDom>
                    </li>
                </ul>
            </div>
        </nav >
    );
}

export default NavbarForHome;