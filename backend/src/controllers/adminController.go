package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/helpers"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/services"
)

func UpdateRole(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		requestedID := mux.Vars(r)
		userID, err := strconv.Atoi(requestedID["user_id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		_, status := model.FindOneByID(userID)
		if status != 200 {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody interfaces.User
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		role, status := model.UpdateRole(formattedBody.Role, userID)
		if status != 200 {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		if role != interfaces.REGULAR {
			author, status, condition := model.CheckIfUserIsAuthor(userID)
			if status != http.StatusOK {
				w.WriteHeader(http.StatusBadRequest)

				return
			}

			if author == nil && !condition {
				status = model.InsertAuthor(userID)
				if status != http.StatusOK {
					w.WriteHeader(http.StatusBadRequest)

					return
				}
			}
		} else {
			author, status, condition := model.CheckIfUserIsAuthor(userID)
			if status != http.StatusOK {
				w.WriteHeader(http.StatusBadRequest)

				return
			}

			if author != nil && condition {
				status = model.DeleteAuthor(userID)
				if status != http.StatusOK {
					w.WriteHeader(http.StatusBadRequest)

					return
				}
			}
		}

		userRole, status := model.GetRole(userID)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"role":    userRole,
			"message": interfaces.MESSAGE,
		})
	}
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		users, status := model.GetUsers()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		roles, status := model.GetRoles()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"users":   users,
			"roles":   roles,
			"message": interfaces.MESSAGE,
		})
	}
}

func SendGroupEmail(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Lista mailigowa jest nieprawidłowa!"

			return
		}
		var formattedBody interfaces.GroupEmail
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Coś poszło nie tak!"

			return
		}

		status := services.SendGroupEmail(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Nie udało się wysłać emaila"

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		var condition bool
		id := mux.Vars(r)
		userID, err := strconv.Atoi(id["user_id"])
		if err != nil {
			interfaces.MESSAGE = "Taki użytkownik nie istnieje!"
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		interfaces.STATUS = model.SetUserAsInactive(userID)
		if interfaces.STATUS != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		_, interfaces.STATUS, condition = model.CheckIfUserIsAuthor(userID)
		if interfaces.STATUS != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		if condition {
			interfaces.STATUS = model.DeleteAuthor(userID)
			if interfaces.STATUS != http.StatusOK {
				w.WriteHeader(http.StatusBadRequest)

				return
			}

			_, interfaces.STATUS = model.UpdateRole(interfaces.REGULAR, userID)
			if interfaces.STATUS != http.StatusOK {
				w.WriteHeader(http.StatusBadRequest)

				return
			}
		}

		events_id, status := model.GetEventsID(userID)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		for _, event_id := range events_id {
			model.DecreaseAmountOfParticipants(event_id)
		}

		interfaces.STATUS = model.ParticipantDeleted(userID)
		if interfaces.STATUS != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}
