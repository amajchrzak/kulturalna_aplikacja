const AddEventForm = props => {
    return (
        <div className="window">
            <h2 className="title">Formularz dodawania nowego wydarzenia</h2>
            <div>
                <form onSubmit={e => props.data.handleAddEvent(e)}>
                    <div className="titleAndDescription">
                        <div className="setTitleEvent">
                            <label htmlFor="inputTitle" className="h3 mb-3">Tytuł</label>
                            <input type="text" id="inputTitle" className="form-control" placeholder="Title" required
                                name="title"
                                onChange={e => { props.data.handleInputData(e) }}
                                value={props.data.dataForm.title}
                            />
                            <br />
                            <label htmlFor="listInstructors" className="h3 mb-3">Instruktor</label>
                            {props.handleGetListInstructors()}
                        </div>
                        <div className="setDescriptionEvent">
                            <label htmlFor="inputDescription" className="h3 mb-3">Opis</label>
                            <textarea type="text-area" id="inputDescription" className="form-control" placeholder="Description" required
                                name="description"
                                onChange={e => { props.data.handleInputData(e) }}
                                value={props.data.dataForm.description}
                                rows="5"
                            />
                        </div>
                    </div>
                    <br />
                    <div className="panels">
                        <div className="leftPanel">
                            <label htmlFor="listPlaces" className="h3 mb-3">Miejsce</label>
                            {props.handleGetListPlaces()}
                            <br />
                            <label htmlFor="inputParticipants" className="h3 mb-3">Limit uczestników</label>
                            <input type="number" id="inputParticipants" className="form-control" placeholder="Limit of participants"
                                readOnly
                                min={0}
                                max={props.maxParticipants}
                                // onChange={e => { props.data.handleLimitParticipants(e.target.value, props.maxParticipants) }}
                                value={props.data.limitParticipants}
                            />
                            <br />
                            <label htmlFor="inputPrice" className="h3 mb-3">Cena</label>
                            <input type="number" id="inputPrice" className="form-control" placeholder="Price"
                                name="price"
                                min={0}
                                onChange={e => { props.data.handleSetPrice(e.target.value) }}
                                value={props.data.dataForm.price}
                            />
                        </div>
                        <div className="middlePanel">
                            <label htmlFor="inputStartDate" className="h3 mb-3">Data rozpoczęcia</label>
                            <input type="date" id="inputStartDate" className="form-control" required
                                name="startDate"
                                onChange={e => { props.data.handleInputData(e) }}
                                value={props.data.dataForm.startDate}
                            />
                            <br />
                            <label htmlFor="inputEndDate" className="h3 mb-3">Data zakończenia</label>
                            <input type="date" id="inputEndDate" className="form-control" required
                                name="endDate"
                                onChange={e => { props.data.handleInputData(e) }}
                                value={props.data.dataForm.endDate}
                            />
                        </div>
                        <div className="rightPanel">
                            <label htmlFor="inputStartTime" className="h3 mb-3">Godzina rozpoczęcia</label>
                            <input type="time" id="inputStartTime" className="form-control" required
                                name="inputStartTime"
                                onChange={e => { props.data.handleInputData(e) }}
                                value={props.data.dataForm.inputStartTime}
                            />
                            <br />
                            <label htmlFor="inputEndTime" className="h3 mb-3">Godzina zakończenia</label>
                            <input type="time" id="inputEndTime" className="form-control" required
                                name="inputEndTime"
                                onChange={e => { props.data.handleInputData(e) }}
                                value={props.data.dataForm.inputEndTime}
                            />
                        </div>
                    </div>
                    <br />
                    <div>
                        <div className="clearButton">
                            <button onClick={props.data.clearForm} className="btn btn-dark btn-block singleButton">Wyczyść formularz</button>
                        </div>
                        <div className="buttons">
                            <button onClick={props.data.closeModal} className="btn btn-secondary btn-block singleButton">Zamknij</button>
                            <button type="submit" className="btn btn-success btn-block singleButton">Dodaj wydarzenie</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default AddEventForm;