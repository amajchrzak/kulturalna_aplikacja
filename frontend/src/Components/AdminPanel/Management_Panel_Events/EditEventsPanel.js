import React, { useState, useMemo, useEffect } from 'react';
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../../Notification/Notification';
import { Link as LinkReactRouterDom } from "react-router-dom";
import Swal from 'sweetalert2';
import axios from 'axios';
import { useHistory } from "react-router-dom";
import ListInstructorEditEvet from './ListInstructorEditEvet';
import '../../../Styles/EditEvent.css';

import { nameHost } from '../../../GlobalVariables/GlobalVariables';
import { faSafari } from '@fortawesome/free-brands-svg-icons';

const EditEventsPanel = (props) => {

    let history = useHistory();

    const [changes, setChanges] = useState(false);
    const [listInstructors, setListInstructors] = useState([]);
    const [instructor, setInstructor] = useState(props.location.state.InstructorID);


    const [listPlaces, setListPlaces] = useState([]);
    const [maxParticipants, setMaxParticipants] = useState();
    const [placeLimitParticipans, setplaceLimitParticipans] = useState(props.location.state.place_limit_of_participants);
    const [limitParticipants, setLimitParticipants] = useState(props.location.state.place_limit_of_participants);
    const [place, setPlace] = useState(props.location.state.place_id)

    const [values, setValues] = useState({
        id: props.location.state.id,
        title: props.location.state.title,
        description: props.location.state.description,
        start_of_event: props.location.state.start_of_event,
        end_of_event: props.location.state.end_of_event,
        price: props.location.state.price,
        instructor: props.location.state.instructor,
        InstructorID: props.location.state.instructorID,
        limit_of_participants: props.location.state.limit_of_participants,
        quantity_participants: props.location.state.quantity_participants,
        place_name: props.location.state.place,
        place_id: props.location.state.place_id,
        place_limit_of_participants: props.location.state.place_limit_of_participants,
        startHourEvent: props.location.state.startHourEvent,
        endHourEvent: props.location.state.endHourEvent,
        listInstructorsAll: props.location.state.listInstructorsAll,
        places: props.location.state.listPlace,
    });

    const handleGetListInstructors = (event) => {
        const listInstructorsTest = [];
        listInstructors.forEach((element) => {
            listInstructorsTest.push(
                <option key={element.ID} value={element.ID}>{element.Name + " " + element.Surname}</option>
            )
        });
        return (
            <select id="listInstructors" value={instructor}
                onChange={e => {
                    setInstructor(e.target.value)
                    setChanges(true)
                }} className="form-select">
                <option value="" disabled>Select instructor</option>
                <option value=""></option>
                {listInstructorsTest}
            </select>
        )
    }


    const handleMaxParticipants = ID => {
        const place = values.places.filter(place => place.ID === parseInt(ID));
        if (place.length) {
            setplaceLimitParticipans(place[0].LimitOfParticipants);
        } else {
            setplaceLimitParticipans("");
        };
    }

    const handleGetListPlaces = () => {
        const listPlaces = [];

        values.places.forEach(place => {
            listPlaces.push(
                <option key={place.ID} value={place.ID}>{place.NameOfPlace} {place.LimitOfParticipants ? "(" + Number(place.LimitOfParticipants) + ")" : ""}</option>
            )
        })

        const funplaceLimitParticipans = (place_) => {

            listPlaces.forEach((element) => {
                // console.log(place_)
                // console.log(typeof (place))
                if (element.ID == place_) {
                    console.log("sdf")
                    //setplaceLimitParticipans(element.LimitOfParticipants)
                    // console.log(placeLimitParticipans)
                }
            })
        }

        return (
            <select value={place} required
                onChange={e => {
                    setPlace(e.target.value)
                    setChanges(true)
                    handleMaxParticipants(e.target.value)
                    funplaceLimitParticipans(e.target.value)
                    // handleMaxParticipants(e.target.value);
                    // handleLimitParticipants(e.target.value, values.place_limit_of_participants)
                }}
                className="form-select">
                <option value="" disabled>Select place</option>
                {listPlaces}
                <option value=""></option>

            </select>
        )
    }

    const handleLimitParticipants = (value, maxValue) => {
        if (maxValue) {
            if (value > maxValue) {
                setLimitParticipants(maxValue);
            } else if (value < 0) {
                setLimitParticipants(0);
            } else {
                setLimitParticipants(value);
            }
        } else {
            if (value < 0) {
                setLimitParticipants(0);
            } else {
                setLimitParticipants(value);
            }
        }
    }


    /*
    const handleMaxParticipants = ID => {
      const place = places.filter(place => place.ID === parseInt(ID));
      if (place.length) {
          setMaxParticipants(place[0].LimitOfParticipants);
          props.setLimitParticipants(place[0].LimitOfParticipants);
      } else {
          setMaxParticipants("");
          props.setLimitParticipants("");
      };
    }*/



    useEffect(() => {
        //  fetchTableData(UrlShowEvents);
        setInstructor(props.location.state.instructorID);
        setListInstructors(props.location.state.listInstructorsAll);
    }, []);





    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })


    const ReturnToManagmentPanel = (event) => {
        event.preventDefault();

        if (changes) {

            swalWithBootstrapButtons.fire({
                title: 'Wykryto zmiany w wydarzeniu',
                text: "Chcesz wyjść bez zapisu zmian w wydarzeniu?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Zapisz zmiany',
                cancelButtonText: 'Wyjdź mimo zmian',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    handleSavesumbit(event)

                } else if (

                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    //   swalWithBootstrapButtons.fire(
                    //     'Zmiany nie zostały wprowadzo',
                    //     'Your imaginary file is safe :)',
                    //     'error'
                    //   )
                    history.push("/management-panel");
                }
            })


        }
        else {
            history.push("/management-panel");
        }
    }




    const handleSavesumbit = (event) => {

        event.preventDefault();
        SendNewEvents(values);
        setChanges(false);

    }
    const SendNewEvents = () => {
        console.info("place", place)
        if (changes) {
            const token = localStorage.getItem("token");
            const addressUrlSuccessSendNewEvent = `${nameHost}/update-event/${values.id}`

            const startDate = (values.start_of_event + "T" + values.startHourEvent + ":00.000Z");
            const endDate = (values.end_of_event + "T" + values.endHourEvent + ":00.000Z");

            axios.patch(addressUrlSuccessSendNewEvent,
                {
                    title: values.title,
                    description: values.description,
                    start_of_event: startDate,
                    end_of_event: endDate,
                    price: values.price,
                    instructor_id: String(instructor),
                    place_id: String(place),
                    author_id: localStorage.getItem("user_id")
                },
                {
                    headers: {
                        Authorization: 'Bearer ' + token
                    }
                }
            ).then(res => {
                console.info("###Response from SendNewEvent endpoint:", res);
                if (res.status === 200) {
                    if (res.data.message === "SUCCESS") {

                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Zmiany zostały zapisane',
                            showConfirmButton: false,
                            timer: 2000
                        })
                    } else {
                        store.addNotification({
                            ...notificationTemplate,
                            type: 'danger',
                            title: 'Błąd',
                            message: 'Zmiany nie zostały zapisane',
                            dismiss: {
                                duration: 8000,
                                onScreen: true
                            }
                        })
                    }
                }


            }).catch(err => {
                console.error("Error from SaveNewEvent endpoint", err);

                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Problem z zapisaniem wydarzenia',
                    message: 'Wydarzenie nie zostało zapisane',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })
            });
        }
        else {
            store.addNotification({
                ...notificationTemplate,
                type: 'danger',
                title: 'Nie wykryto żadnych zmian',
                message: 'Wydarzenie nie zostanie zapisane',
                dismiss: {
                    duration: 8000,
                    onScreen: true
                }
            })
        }
    }
    const handleChange = (event) => {
        console.log("TESTTTT", event.target.value)
        setValues({
            ...values,
            [event.target.name]: event.target.value,

        })
        //handleLimitParticipants
        setChanges(true);
    }






    return (
        <div className="window editenevtfonr">
            <div >
                <h2 className="titleevent">Formularz edytowanego wydarzenia</h2>
                <div>
                    <form >
                        <div className=" titleevent">
                            <div className="titleeditevent">
                                <label htmlFor="inputTitle" className="h3 mb-3 ">Tytuł</label>
                                <input name="title" type="text" id="inputTitle" className="form-control" placeholder="Title" required
                                    value={values.title}
                                    onChange={handleChange}
                                />
                            </div>
                        </div>
                        <div className="descriptioneditevent">
                            <label htmlFor="inputDescription " className="h3 mb-3">Opis</label>
                            <textarea name="description" type="text-area" className="form-control descriptionPostInput" placeholder="Description" required
                                value={values.description}
                                onChange={handleChange}

                            />
                        </div>

                        <br />
                        <div className="panels">
                            <div className="leftPanel">
                                <label htmlFor="inputPrice" className="h3 mb-3">Cena</label>
                                <input name="price" type="text" id="inputPrice" className="form-control" placeholder="Price" required
                                    value={values.price}
                                    onChange={handleChange}
                                />
                                <br />
                                <label htmlFor="inputParticipants" className="h3 mb-3">Limit uczestników</label>
                                <input name="limit_of_participants" type="number" id="inputParticipants" className="form-control" placeholder="Limit of participants" required
                                    value={placeLimitParticipans}
                                    onChange={handleChange}

                                />
                                <br />
                            </div>
                            <div className="middlePanel">
                                <label htmlFor="inputStartDate" className="h3 mb-3">Data rozpoczęcia</label>
                                <input name="start_of_event" type="date" id="inputStartDate" className="form-control" placeholder="Start date" required
                                    value={values.start_of_event}
                                    onChange={handleChange}
                                />
                                <br />

                                <label htmlFor="inputEndDate" className="h3 mb-3">Data zakończenia</label>
                                <input name="end_of_event" type="date" id="inputEndDate" className="form-control" placeholder="End date" required
                                    value={values.end_of_event}
                                    onChange={handleChange}
                                />
                                <br></br>
                                <label htmlFor="startHourEvent" className="h3 mb-3 ">Godzina rozpoczęcia</label>
                                <input type="time" className="form-control " required
                                    name="startHourEvent"
                                    value={values.startHourEvent}
                                    onChange={handleChange}


                                />
                            </div>

                            <div className='instructoreditevent'>
                                <label htmlFor="listInstructors" className="h3 mb-3">Instruktor</label>
                                {handleGetListInstructors()}
                                <br></br>
                                <label htmlFor="listPlaces" className="h3 mb-3">Miejsce</label>
                                {handleGetListPlaces()}
                                <br></br>
                                <label htmlFor="endHourEvent" className="h3 mb-3">Godzina zakończenia</label>
                                <input type="time" className="form-control " required
                                    name="endHourEvent"
                                    value={values.endHourEvent}
                                    onChange={handleChange}

                                />
                            </div>




                        </div>
                        <br />         <br />
                        <div className="rightPanel">

                            <br />

                        </div>




                        <div>
                            <br></br>
                            <div className="clearButton">
                                <button className="btn btn-dark btn-block singleButton" onClick={(event) => ReturnToManagmentPanel(event)}  >Powrót</button>
                            </div>
                            <div className="buttons">
                                <button type="submit" className="btn btn-success btn-block singleButton " onClick={(event) => handleSavesumbit(event)} >Zapisz</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    )
}

export default EditEventsPanel
