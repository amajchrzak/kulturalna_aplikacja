const PasswordResetForm = props => {

    return (
        <>
            {!props.emailSent ?
                <div className='passwordRessetForm' id={props.id}>
                    <h1>Zresetuj hasło</h1>
                    <form onSubmit={e => props.handleSendToken(e)}>
                        <h3 className="h3 mb-3 font-weight-normal">Podaj adres na który otrzymasz token wymagany w kolejnym kroku procesu resetowania hasła</h3>
                        <label htmlFor="inputEmail" className="h2 mb-2">Adres e-mail:</label>
                        <input type="email" id="inputEmail" className="form-control" placeholder="adres e-mail" required autoFocus
                            onChange={e => { props.setInputEmail(e.target.value) }}
                            value={props.inputCurrentPassword}
                        />
                        <button type="submit" className="buttonPasswordReset mt-3 btn btn-lg btn-primary btn-block">Wyślij token</button>
                        <p className="mt-5 mb-3 text-muted">&copy; 2021</p>
                    </form>
                </div>
                :
                <div className='passwordRessetForm' id={props.id}>
                    <h1>Reset hasła</h1>
                    <h3 className="h3 mb-3 font-weight-normal">Na podany przez Ciebie adres e-mail został wysłany token. Wprowadź go do poniższego formularza.</h3>
                    <form onSubmit={e => props.handleResetPassword(e)}>
                        <label htmlFor="inputToken" className="h2 mb-2">Token</label>
                        <input type="text" id="inputToken" className="form-control" placeholder="Token" required autoFocus
                            onChange={e => { props.setInputToken(e.target.value) }}
                            value={props.inputToken}
                        />
                        <label htmlFor="inputNewPassword" className="h2 mb-2">Nowe hasło</label>
                        <input type="password" id="inputNewPassword" className="form-control" placeholder="New password" required autoFocus
                            onChange={e => { props.setInputNewPassword(e.target.value) }}
                            value={props.inputNewPassword}
                        />
                        <label htmlFor="inputRepeatPassword" className="h2 mb-2">Powtórz hasło</label>
                        <input type="password" id="inputRepeatPassword" className="form-control" placeholder="Repeat password" required autoFocus
                            onChange={e => { props.setInputRepeatPassword(e.target.value) }}
                            value={props.inputRepeatPassword}
                        />
                        <button type="submit" className="buttonPasswordReset mt-3 btn btn-lg btn-primary btn-block">Resetuj hasło</button>
                        <p className="mt-5 mb-3 text-muted">&copy; 2021</p>
                    </form>
                </div>
            }

        </>
    )

}

export default PasswordResetForm;