package model

import (
	"fmt"
	"net/http"

	"github.com/lib/pq"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func InsertOpinion(opinion interfaces.Opinion) (string, error) {
	status := CheckIfUserExists(opinion.UserId)
	if status != http.StatusOK {

		return "Niepoprawne ID użytkownika", fmt.Errorf("niepoprawne ID użytkownika")
	}

	status = CheckIfEventExists(opinion.EventID)
	if status != http.StatusOK {

		return "Niepoprawne ID wydarzenia", fmt.Errorf("niepoprawne ID wydarzenia")
	}

	message := interfaces.MESSAGE
	_, err := DB.Exec("INSERT INTO opinions(event_id, user_id, title, description, created_at) VALUES($1, $2, $3, $4, $5)",
		opinion.EventID, opinion.UserId, opinion.Title, opinion.Description, opinion.CreatedAt)
	if err, ok := err.(*pq.Error); ok {
		if err.Constraint == interfaces.OPINIONS_UNIQUE {
			message = "Użytkownik już dodał opinię do tego wydarzenia"
		} else {
			message = "Coś poszło nie tak"
		}
	}

	return message, err

}

func GetOpinionsByEventID(event_id int) ([]interfaces.OpinionDetailed, int) {
	rows, err := DB.Query("SELECT opinions.id as opinion_id, event_id, opinions.user_id, title, description, created_at, name, surname FROM opinions LEFT JOIN billing_address ON opinions.user_id = billing_address.user_id	WHERE event_id = $1", event_id)
	if err != nil {

		return nil, http.StatusBadRequest
	}
	defer rows.Close()

	var opinions []interfaces.OpinionDetailed
	var opinion interfaces.OpinionDetailed
	var surname interfaces.NullString
	for rows.Next() {
		if err := rows.Scan(&opinion.ID, &opinion.EventID, &opinion.UserId, &opinion.Title, &opinion.Description, &opinion.CreatedAt, &opinion.OpinionAuthor, &surname); err != nil {
			return opinions, http.StatusBadRequest
		}

		if opinion.OpinionAuthor.Valid && surname.Valid {
			opinion.OpinionAuthor.String += " " + surname.String
		} else if surname.Valid {
			opinion.OpinionAuthor = surname
		}

		opinions = append(opinions, opinion)
	}
	if err = rows.Err(); err != nil {

		return opinions, http.StatusBadRequest
	}

	return opinions, http.StatusOK
}

func DeleteOpinionWithUserID(event_id, user_id int) int {
	_, err := DB.Exec("DELETE FROM opinions WHERE event_id = $1 AND user_id = $2", event_id, user_id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK

}

func DeleteOpinionWithID(opinion_id int) int {
	_, err := DB.Exec("DELETE FROM opinions WHERE id = $1", opinion_id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK

}
