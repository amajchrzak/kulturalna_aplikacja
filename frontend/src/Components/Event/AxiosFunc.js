import axios from "axios";
import { store } from "react-notifications-component";
import { nameHost } from "../../GlobalVariables/GlobalVariables";
import { notificationTemplate } from "../Notification/Notification";

export const removeOpinion = (e, opinionEventId, opinionID, setRefresh) => {
    e.preventDefault();
    const token = localStorage.getItem("token");
    axios.delete(`${nameHost}/delete-own-opinion/${opinionEventId}`, {
        headers: {
            Authorization: 'Bearer ' + token
        }
    })
        .then(res => {
            if (res.data.message === "SUCCESS") {
                store.addNotification({
                    ...notificationTemplate,
                    type: 'success',
                    title: 'Opinia została usunięta',
                    message: 'Usuwanie opinii przebiegło pomyślnie.'
                });
                setRefresh(prevState => !prevState);
            } else {
                //TODO
            }
        })
        .catch(err => {
            console.error("Error:", err);
            store.addNotification({
                ...notificationTemplate,
                type: 'danger',
                title: 'Opinia nie została usunięta',
                message: 'Usuwanie opinii przebiegło niepomyślnie.'
            });
        })
}


export const removeEventAfterAprove = (e, event, removeEventFromList, closeModalRemoveEvent, setRemoveEventFromList, handleActionAfterRemove) => {
    e.preventDefault();
    const token = localStorage.getItem("token");
    let removeIdEvent = event.ID;

    if (removeEventFromList !== "") {
        removeIdEvent = removeEventFromList;
    }
    axios.delete(`${nameHost}/remove-event/${removeIdEvent}`, {
        headers: {
            Authorization: 'Bearer ' + token
        }
    })
        .then(res => {
            console.info("Response after remove event", res)

            store.addNotification({
                ...notificationTemplate,
                type: 'success',
                title: 'Wydarzenie zostało usunięte',
                message: 'Wydarzenie zostało poprawnie usunięte z kalendarza.'
            });
            closeModalRemoveEvent();
            setRemoveEventFromList("");
            handleActionAfterRemove();
        })
        .catch(err => {
            console.error("Error", err);
            store.addNotification({
                ...notificationTemplate,
                type: 'danger',
                title: 'Wydarzenie nie zostało usunięte',
                message: 'Wystąpił problem podczas usuwania wydarzenia z kalendarza.'
            });
            closeModalRemoveEvent();
        });
}

export const addOpinionAfterAprove = (e, inputTitle, inputDescription, event, setRefresh, closeModalAddOpinion, setInputTitle, setInputDescription) => {
    e.preventDefault();

    if (inputTitle.length >= 5 && inputDescription.length >= 5) {

        const token = localStorage.getItem("token");
        const config = {
            headers: {
                Authorization: 'Bearer ' + token
            }
        }

        axios.post(`${nameHost}/add-opinion`,
            {
                event_id: event.ID + "",
                title: inputTitle,
                description: inputDescription
            },
            config
        ).then(res => {
            if (res.data.message === "SUCCESS") {
                setRefresh(prevState => !prevState);
                closeModalAddOpinion();
                store.addNotification({
                    ...notificationTemplate,
                    type: 'success',
                    title: 'Opinia została dodana',
                    message: 'Twoja opinia na temat tego wydarzania została dodana.'
                });
            }
            setInputTitle("");
            setInputDescription("");
        }).catch(err => {
            console.error("Error", err);
            if (err.response) {
                if (err.response.data.message) {
                    if (err.response.data.message === "Użytkownik już dodał opinię do tego wydarzenia") {
                        store.addNotification({
                            ...notificationTemplate,
                            type: 'warning',
                            title: 'Dodałeś już opinię do tego wydarzenia.',
                            message: 'Nie można dodać więcej niż jedną opinię do danego wydarzenia.'
                        });
                    }
                }

            } else {
                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Nie dodano opinii',
                    message: 'Wystąpił problem podczas dodawania opinii.'
                });
            }
        })
    } else {
        store.addNotification({
            ...notificationTemplate,
            type: 'warning',
            title: 'Nie dodano opinii',
            message: 'Niepoprawna ilość znaków. Popraw dane w formularzu.'
        });
    }
}

export const joinEventAfterAprove = (e, event, setRefresh, closeModalJoinEvent) => {
    e.preventDefault();
    const token = localStorage.getItem("token");

    const config = {
        headers: {
            Authorization: 'Bearer ' + token
        }
    }
    axios.put(`${nameHost}/register-on-event/${event.ID}`, {},
        config
    ).then(res => {
        if (res.data.message === "SUCCESS") {
            store.addNotification({
                ...notificationTemplate,
                type: 'success',
                title: 'Zapisano na wydarzenie',
                message: 'Zapis na wydarzenie zakończył się powodzeniem.'
            });
            setRefresh(prevState => !prevState);
        } else if (res.data.message === "FAIL") {
            store.addNotification({
                ...notificationTemplate,
                type: 'danger',
                title: 'Zapisałeś się już na to wydarzenie.',
                message: 'Lista wydarzeń, na które się zapisałeś dostępna jest w panelu użytkownika.'
            });
        }
        closeModalJoinEvent();
    }).catch(err => {
        console.error("Err", err);
        store.addNotification({
            ...notificationTemplate,
            type: 'warning',
            title: 'Nie zapisano',
            message: 'Zapis na wydarzenie zakończył się niepowodzeniem.'
        });
        setRefresh(prevState => !prevState);
        closeModalJoinEvent();
    })
}

const findEventWhichUserSelected = (events, selectedEventIdFromCalendar) => {
    const foundEvent = events.find(singleEvent => singleEvent.ID === selectedEventIdFromCalendar);

    return foundEvent ? foundEvent : 0; //if no event was found return 0 and set first event from events
}

export const getEvents = (setEvent, setListEvents, start_of_event, end_of_event, selectedEventIdFromCalendar) => {
    // const url = 'https://mocki.io/v1/32b28cf3-48d2-498e-871c-2a13bbccfc97';
    // const url = "https://mocki.io/v1/433dfa4e-1013-42b3-ae09-876ea78a0ec0";

    const token = localStorage.getItem('token');
    const config = {
        headers: {
            Authorization: 'Bearer ' + token
        }
    }

    axios.post(`${nameHost}/get-events-day`,
        {
            start_of_event,
            end_of_event
        },
        config
    )
        .then(res => {
            const whichEvent = findEventWhichUserSelected(res.data.events, selectedEventIdFromCalendar)
            whichEvent ? setEvent(whichEvent) : setEvent(res.data.events[0]);
            setListEvents(res.data.events);
        }
        )
        .catch(err => {
            console.error("Error:", err);
        })
}

export const getOpinions = (event, setOpinions) => {
    const token = localStorage.getItem("token");
    // const url = "https://mocki.io/v1/ec44d8f2-f8b4-43aa-bedc-291fc548447a";
    if (event.ID) {
        axios.get(`${nameHost}/get-opinions/${event.ID}`, {
            // axios.get(url, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    if (res.data.opinions !== null) {
                        setOpinions(res.data.opinions);
                    } else {
                        setOpinions([]);
                    }
                } else {
                    setOpinions([]);
                }
            })
            .catch(err => {
                console.error("Error:", err);
            })
    }
    else {
        setOpinions([]);
    }
}