import React from 'react';
import '../Styles/IncomingEvents.css';

const IncomingEvents = ({ id }) => {
    return (
        <div className='incomingEvent' id={id}>
            <div className="container">
                <div className="row events-text">
                    <div className="col-md-4">
                        <h2>Wydarzenie 1</h2>
                        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                        <p><button className="btn btn-lg btn-info">Zobacz szczegóły &raquo;</button></p>
                    </div>
                    <div className="col-md-4">
                        <h2>Wydarzenie 2</h2>
                        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                        <p><button className="btn btn-lg btn-info">Zobacz szczegóły &raquo;</button></p>
                    </div>
                    <div className="col-md-4">
                        <h2>Wydarzenie 3</h2>
                        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                        <p><button className="btn btn-lg btn-info">Zobacz szczegóły &raquo;</button></p>
                    </div>
                    <div className="col-md-4">
                        <h2>Wydarzenie 4</h2>
                        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                        <p><button className="btn btn-lg btn-info">Zobacz szczegóły &raquo;</button></p>
                    </div>
                    <div className="col-md-4">
                        <h2>Wydarzenie 5</h2>
                        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                        <p><button className="btn btn-lg btn-info">Zobacz szczegóły &raquo;</button></p>
                    </div>
                    <div className="col-md-4">
                        <h2>Wydarzenie 6</h2>
                        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
                        <p><button className="btn btn-lg btn-info">Zobacz szczegóły &raquo;</button></p>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default IncomingEvents;