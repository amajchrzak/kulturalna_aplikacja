package services

import (
	"net/http"
	"net/smtp"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
)

func StartResetPasswordProcedure(email string) int {
	genUUID := generateNewV4UUID()
	status := model.UpdateResetPasswordToken(email, genUUID)
	if status != http.StatusOK {

		return http.StatusBadRequest
	}

	status = sendResetPasswordEmail(email, genUUID)
	if status != http.StatusOK {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func sendResetPasswordEmail(email string, genUUID string) int {
	auth := smtp.PlainAuth("", "1ce0c31fbeecf7", "b553c53b547af5", "smtp.mailtrap.io")

	// email1 := "dadsad@app.ap"
	to := []string{email}

	message := []byte("From: kontodoolx777@gmail.com\r\n" +
		"To: " + email + "\r\n" +
		"Subject: Reset Password\r\n" +
		"\r\n" +
		"Reset Password Token: \r\n" + genUUID)

	err := smtp.SendMail("smtp.mailtrap.io:2525", auth, "kontodoolx777@gmail.com", to, message)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func SendGroupEmail(groupEmailDetails interfaces.GroupEmail) int {
	receivers := setReceivers(groupEmailDetails.Emails)
	to, status := prepareMailingListInString(receivers)
	if status != http.StatusOK {

		return http.StatusBadRequest
	}

	auth := smtp.PlainAuth(interfaces.EMAIL_IDENTITY, interfaces.EMAIL_USERNAME, interfaces.EMAIL_PASSWORD, interfaces.EMAIL_HOST)
	msg := []byte("From: " + interfaces.EMAIL_FROM + "\r\n" +
		"To: " + to + "\r\n" +
		"Subject: " + groupEmailDetails.Subject + "\r\n" +
		"\r\n" +
		groupEmailDetails.Message + "\r\n")

	err := smtp.SendMail(interfaces.EMAIL_ADDRESS, auth, interfaces.EMAIL_FROM, receivers, msg)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func generateNewV4UUID() string {

	return uuid.NewV4().String()
}

func setReceivers(groupEmailDetails []string) []string {
	receivers := make([]string, len(groupEmailDetails))
	for index, _ := range groupEmailDetails {
		receivers[index] = groupEmailDetails[index]
	}

	return receivers
}

func prepareMailingListInString(receivers []string) (string, int) {
	var listOfReceiversInStringFormat string

	for index, _ := range receivers {
		if index >= len(receivers)-1 {
			listOfReceiversInStringFormat += receivers[index]
		} else {
			listOfReceiversInStringFormat += receivers[index] + ", "
		}
	}

	if len(listOfReceiversInStringFormat) == 0 {

		return listOfReceiversInStringFormat, http.StatusBadRequest
	}

	return listOfReceiversInStringFormat, http.StatusOK
}
