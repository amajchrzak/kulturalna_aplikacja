import { useState } from 'react';
import Modal from 'react-modal';
import '../../Styles/ModalViewAddEvent.css';
import AddEventForm from '../Forms/AddEventForm';

const AddEventModal = props => {

    const [maxParticipants, setMaxParticipants] = useState("");

    const places = props.listPlaces;

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: '50%',
            height: '80%',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            backgroundColor: "rgb(48, 52, 92)",
            color: "white"
        },
    };

    Modal.setAppElement('#root');

    const handleGetListInstructors = () => {
        const listInstructors = [];
        props.listInstructors.forEach((element) => {
            listInstructors.push(
                <option key={element.ID} value={element.ID}>{element.Name + " " + element.Surname}</option>
            )
        });
        return (
            <select id="listInstructors" value={props.instructor} onChange={e => { props.setInstructor(e.target.value) }} className="form-select">
                <option value="" disabled>Select instructor</option>
                <option value=""></option>
                {listInstructors}
            </select>
        )
    }

    const handleMaxParticipants = ID => {
        const place = places.filter(place => place.ID === parseInt(ID));
        if (place.length) {
            setMaxParticipants(place[0].LimitOfParticipants);
            props.setLimitParticipants(place[0].LimitOfParticipants);
        } else {
            setMaxParticipants("");
            props.setLimitParticipants("");
        };
    }

    const handleGetListPlaces = () => {
        const listPlaces = [];
        props.listPlaces.forEach((element) => {
            listPlaces.push(
                <option key={element.ID} value={element.ID}>{element.NameOfPlace} {element.LimitOfParticipants ? "(" + element.LimitOfParticipants + ")" : ""}</option>
            )
        });
        return (
            <select id="listPlaces" value={props.place} required
                onChange={e => {
                    props.setPlace(e.target.value)
                    handleMaxParticipants(e.target.value);
                }}
                className="form-select" placeholder="Place">
                <option value="" disabled>Select place</option>
                <option value=""></option>
                {listPlaces}
            </select>
        )
    }

    return (
        <>
            <Modal
                isOpen={props.isOpen}
                onRequestClose={props.onRequestClose}
                style={customStyles}
                contentLabel={props.contentLabel}
            >
                <AddEventForm
                    data={props}
                    maxParticipants={maxParticipants}
                    handleGetListInstructors={handleGetListInstructors}
                    handleGetListPlaces={handleGetListPlaces}
                />
            </Modal>
        </>
    )
}

export default AddEventModal;