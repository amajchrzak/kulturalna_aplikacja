import '../Styles/PasswordReset.css';
import PasswordResetForm from './Forms/PasswordResetForm';
import axios from 'axios';
import { useState } from 'react';
import { store } from 'react-notifications-component';
import { notificationTemplate } from './Notification/Notification';
import { useHistory } from "react-router-dom";
import { nameHost } from '../GlobalVariables/GlobalVariables';

const PasswordReset = ({ userData, setUserData }) => {

    const [emailSent, setEmailSent] = useState(false);
    const [inputEmail, setInputEmail] = useState("");
    const [inputToken, setInputToken] = useState("");
    const [inputNewPassword, setInputNewPassword] = useState("");
    const [inputRepeatPassword, setInputRepeatPassword] = useState("");
    const [token, setToken] = useState(localStorage.getItem("token"));
    const history = useHistory();

    const config = {
        headers: {
            Authorization: 'Bearer ' + token
        }
    }

    const handleClearForm = () => {
        console.info("###CLEAR FORM PROCESS");
        setInputToken("");
        setInputNewPassword("");
        setInputRepeatPassword("");
    }

    const handleSentMail = () => {
        setEmailSent(true);
        store.addNotification({
            ...notificationTemplate,
            type: 'info',
            title: 'Wysłano token',
            message: 'Na podany adres e-mail został wysłany token. Wprowadź go do formularza.',
            dismiss: {
                duration: 10000,
                onScreen: true
            }
        })
    }

    const handleNoSentMail = () => {
        setEmailSent(false);
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Problem przy wysyłaniu tokenu',
            message: 'Nie udało się wysłać tokenu na podany adres e-mail.',
            dismiss: {
                duration: 10000,
                onScreen: true
            }
        });
    }

    const handlePasswordResetNotifications = result => {
        let type = '';
        let title = '';
        let message = '';

        if (result) {
            type = 'success';
            title = 'Ustawiono nowe hasło';
            message = 'Proces resetowania hasła zakończony pomyślnie. Zaloguj się używając nowego hasła.'
        } else {
            type = 'danger';
            title = 'Problem podczas resetu hasła';
            message = 'Proces resetowania hasła zakończony niepowodzeniem. Podano błędny token.'
        }
        store.addNotification({
            ...notificationTemplate,
            type,
            title,
            message,
            dismiss: {
                duration: 10000,
                onScreen: true
            }
        });
    }

    const handleTestPassword = pass => {
        const reg = {
            'small': /[a-z]/,
            'special': /[!@#$%^&*]/,
        };
        return (reg.small.test(pass) || reg.capital.test(pass)) &&
            reg.special.test(pass) &&
            pass.length >= 5 && pass.length <= 30;
    }

    const handleSendToken = e => {
        console.info("###PASSWORD RESET PROCESS");
        console.info("###SENDING EMAIL WITH TOKEN");
        e.preventDefault();
        handleClearForm();

        // const addressUrlLogin = "http://localhost:4000/reset-password-send-email";
        // const addressUrlSentMail = "https://mocki.io/v1/6c1862df-04b0-4ee8-a321-b349d8fc0b3f";

        setToken(localStorage.getItem("token")); //set current token

        axios.post(`${nameHost}/send-reset-password-procedure`,
            {
                email: inputEmail
            },
            config
        ).then(res => {
            console.info("Response from sent mail endpoint:", res);
            if (res.data.message === "SUCCESS") {
                handleSentMail();
            } else {
                handleNoSentMail();
            }
        }).catch(err => {
            console.error("Error from sent mail endpoint", err);
            handleNoSentMail();
        });
    };

    const handleIncorrectPasswordMessage = () => {
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Hasło nie spełnia wymagań bezpieczeństwa',
            message: 'Hasło musi zawiarać minimum jedną małą lub wielką literę oraz minimum jeden znak specjalny oraz minimalną długość 5 znaków.',
            dismiss: {
                duration: 15000,
                onScreen: true
            }
        })
    }

    const handleComparePasswords = () => {
        return inputNewPassword === inputRepeatPassword;
    }

    const handleComparePasswordMessage = () => {
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Podane hasła różnią się od siebie',
            message: 'Pole Hasło i Powtórz hasło muszą zawierać takie same hasło.',
            dismiss: {
                duration: 10000,
                onScreen: true
            }
        })
    }

    const handleResetPassword = e => {
        console.info("###RESET PASSWORD");
        e.preventDefault();
        handleClearForm();

        const testPassword = handleTestPassword(inputNewPassword);
        let comparePassword = false;

        if (!testPassword) {
            handleIncorrectPasswordMessage();
        } else {
            comparePassword = handleComparePasswords();
            if (!comparePassword) {
                handleComparePasswordMessage();
            }
        }

        if (testPassword && comparePassword) {
            // const url = nameHost + "/reset-password"
            // const addressUrlReset = "https://mocki.io/v1/9105dcec-21e2-41a9-a899-d7258042081a";

            setToken(localStorage.getItem("token")); //set current token

            axios.post(`${nameHost}/reset-password`,
                {
                    SentToken: inputToken,
                    NewPassword: inputNewPassword,
                    RetypedNewPassword: inputRepeatPassword
                },
                config
            ).then(res => {
                console.info("Response from reset password endpoint:", res);
                if (res.data.message === "SUCCESS") {
                    handlePasswordResetNotifications(true);
                    history.push("/login-panel");
                } else {
                    handlePasswordResetNotifications(false);
                }
            }).catch(err => {
                console.error("Error from reset password endpoint", err);
                handlePasswordResetNotifications(false);
            });
        }
    }

    return (
        <div>
            <PasswordResetForm
                handleSendToken={handleSendToken}
                handleResetPassword={handleResetPassword}
                setInputEmail={setInputEmail}
                inputEmail={inputEmail}
                setInputToken={setInputToken}
                inputToken={inputToken}
                setInputNewPassword={setInputNewPassword}
                inputNewPassword={inputNewPassword}
                inputRepeatPassword={inputRepeatPassword}
                setInputRepeatPassword={setInputRepeatPassword}
                emailSent={emailSent}
            />
        </div>
    )
}

export default PasswordReset;