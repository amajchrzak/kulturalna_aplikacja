package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/helpers"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/services"
)

func Register(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusNotAcceptable)

			return
		}

		interfaces.STATUS, interfaces.MESSAGE = services.ValidCorrectnessOfRequest(formattedBody)
		if interfaces.STATUS != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{
				"message": interfaces.MESSAGE,
			})

			return
		}

		interfaces.STATUS = services.ComparePasswords(formattedBody["password"], formattedBody["retypedPassword"])
		if interfaces.STATUS != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{
				"message": interfaces.MESSAGE,
			})

			return
		}

		user := services.PrepareRegisterOperation(formattedBody["email"], formattedBody["username"], formattedBody["password"])
		message, err := model.InsertUser(user)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{
				"message": message,
			})

			return
		}

		//todo return value / message
		json.NewEncoder(w).Encode(map[string]string{
			"email":    formattedBody["email"],
			"username": formattedBody["username"],
			"message":  interfaces.MESSAGE,
		})
	}
}

func Login(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody interfaces.Login
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusNotAcceptable)

			return
		}

		user, status := model.FindOneByUsername(formattedBody.Username, formattedBody.Password)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		if user.Status == interfaces.INACTIVE_USER {
			json.NewEncoder(w).Encode(map[string]interface{}{
				"message": interfaces.FAIL,
			})

			return
		}

		token, status := services.PrepareJWTToken(&user)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusFailedDependency)

			return
		}

		userRole, status := model.GetRole(int(user.ID))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"id":       user.ID,
			"email":    user.Email,
			"username": user.Username,
			"role":     userRole,
			"jwt":      token,
			"status":   user.Status,
			"message":  interfaces.MESSAGE,
		})
	}
}

func ChangePassword(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody interfaces.ChangePassword
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusNotAcceptable)

			return
		}

		formattedBody.UserID = helpers.GetLoggedUserID(r)
		requestedID := mux.Vars(r)
		id, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status := helpers.CheckRightUser(id, formattedBody.UserID)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		user, status := model.FindOneByID(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status = services.CompareHashAndPassword(string(user.Password), formattedBody.OldPassword)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status = services.ComparePasswords(formattedBody.NewPassword, formattedBody.RetypedNewPassword)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status = services.ValidPassword(formattedBody.NewPassword)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		hash, status := services.HashNewPassword(formattedBody.NewPassword)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		} else {
			formattedBody.NewPassword = hash
		}

		status = model.ChangePassword(string(user.Password), formattedBody.NewPassword, int(user.ID))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			//test
			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func GetLoggedUser(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		id := helpers.GetLoggedUserID(r)
		user, status := model.FindOneByID(int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		userRole, status := model.GetRole(int(user.ID))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		billingAddress, status := model.GetUserBillingAddress(int(user.ID))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"id":       user.ID,
			"username": user.Username,
			"role":     userRole,
			"message":  interfaces.MESSAGE,
			"name":     billingAddress.Name,
			"surname":  billingAddress.Surname,
			"state":    user.Status,
		})
	}
}

func SendResetPasswordProcedure(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Proszę wprowadzić email!"

			return
		}

		var email interfaces.Email
		err = json.Unmarshal(body, &email)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Coś poszło nie tak!"

			return
		}

		interfaces.STATUS = services.StartResetPasswordProcedure(email.Email)
		if interfaces.STATUS != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Procedura zmiany hasła nie została rozpoczęta!"

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func ResetPasword(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Wprowadzone dane są niepoprawne!"

			return
		}

		var formattedBody interfaces.ResetPasword
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Coś poszło nie tak!"

			return
		}

		status := services.ResetPasswordForUserWhoLostPassword(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			interfaces.MESSAGE = "Wprowadziłeś niepoprawne dane!"

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func GetUserProfileData(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		id := helpers.GetLoggedUserID(r)
		status := model.CheckIfUserExists(int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		billing_data, status := model.GetBillingAddress(int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		events_ids, status := model.GetEventsID(int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		events, status := model.GetShortEventsByID(events_ids)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"profile_data": billing_data,
			"events":       events,
			"message":      interfaces.MESSAGE,
		})
	}
}

func ChangeProfileData(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody interfaces.NewBillingData
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusNotAcceptable)

			return
		}

		id := helpers.GetLoggedUserID(r)

		status := model.CheckIfBillingAddressExists(int(id))
		if status != http.StatusOK {
			status = model.InsertBillingAddress(formattedBody, int(id))
			if status != http.StatusOK {
				w.WriteHeader(http.StatusBadRequest)

				return
			}

		}

		status = model.ChangeBillingAdress(formattedBody, int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}

func DeleteOwnAccount(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		id := helpers.GetLoggedUserID(r)

		status, message := model.SetAccountAsInactive(int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"message": message,
			})

			return
		}

		_, status, condition := model.CheckIfUserIsAuthor(int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		if condition {
			status = model.DeleteAuthor(int(id))
			if status != http.StatusOK {
				w.WriteHeader(http.StatusBadRequest)

				return
			}
		}

		events_id, status := model.GetEventsID(int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		for _, event_id := range events_id {
			model.DecreaseAmountOfParticipants(event_id)
		}

		status = model.ParticipantDeleted(int(id))
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}
