package model

import (
	"database/sql"
	"net/http"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func UpdateRole(role int, userID int) (int, int) {
	query := `UPDATE users SET role=$1 WHERE id=$2`
	_, err := DB.Exec(query, role, userID)
	if err != nil {

		return interfaces.REGULAR, http.StatusBadRequest
	}

	return role, http.StatusOK
}

func CheckIfUserIsAuthor(id int) (*interfaces.Author, int, bool) {
	query := `
		SELECT id, user_id 
		FROM authors 
		WHERE user_id = $1
	`

	rows := DB.QueryRow(query, id)

	author := &interfaces.Author{}
	err := rows.Scan(&author.ID, &author.UserID)

	switch {
	case err == sql.ErrNoRows:

		return nil, http.StatusOK, false
	case err != nil:

		return nil, http.StatusBadRequest, false
	case err == nil:

		return author, http.StatusOK, true
	default:

		return nil, http.StatusBadRequest, false
	}
}

func InsertAuthor(id int) int {
	query := `
		INSERT INTO authors(user_id)
		VALUES($1)
	`

	_, err := DB.Exec(query, id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func DeleteAuthor(id int) int {
	query := `
		DELETE FROM authors
		WHERE user_id = $1
	`

	_, err := DB.Exec(query, id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetUsers() ([]interfaces.User, int) {
	var users []interfaces.User
	var user interfaces.User

	rows, err := DB.Query("SELECT id, email, password, username, role, state FROM users ORDER BY id ASC")
	if err != nil {

		return users, http.StatusBadRequest
	}

	for rows.Next() {
		if err = rows.Scan(&user.ID, &user.Email, &user.Password, &user.Username, &user.Role, &user.Status); err != nil {

			return users, http.StatusBadRequest
		}

		users = append(users, user)
	}

	if err = rows.Err(); err != nil {

		return users, http.StatusBadRequest
	}

	return users, http.StatusOK
}

func GetRoles() ([]interfaces.Role, int) {
	query := `
		SELECT *
		FROM roles
	`

	roles := []interfaces.Role{}
	role := interfaces.Role{}
	rows, err := DB.Query(query)
	if err != nil {

		return roles, http.StatusBadRequest
	}

	for rows.Next() {
		if err = rows.Scan(&role.ID, &role.Name); err != nil {

			return roles, http.StatusBadRequest
		}

		roles = append(roles, role)
	}

	return roles, http.StatusOK
}

func SetUserAsInactive(id int) int {
	query := `
		UPDATE users 
		SET state = $1 
		WHERE id = $2
	`

	_, err := DB.Exec(query, interfaces.INACTIVE_USER, id)
	if err != nil {
		interfaces.MESSAGE = "Nie udało się usunąć użytkownika"

		return http.StatusBadRequest
	}

	return http.StatusOK
}
