const preapreObjEvent = (element, nrDay, nrMonth, year) => {
    return {
        id: element.ID,
        nrDay,
        nrMonth,
        year,
        title: element.Title,
        description: element.Description,
        price: element.Price,
        limitOfParticipants: element.LimitOfParticipants,
        amountOfParticipants: element.AmountOfParticipants,
        createdAt: element.CreatedAt,
        updatedAt: element.UpdatedAt,
        authorID: element.AuthorID,
        instructorID: element.InstructorID,
        author: element.Author,
        instructor: element.Instructor,
        startOfEvent: element.StartOfEvent,
        endOfEvent: element.EndOfEvent,
        place: element.Place
    }
}

export default preapreObjEvent;