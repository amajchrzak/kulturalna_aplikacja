import React, { useState } from "react";
import '../Styles/Navbar.css'
import NavbarForHome from "./NavbarsForHomeAndLoginPanel/NavbarForHome";
import NavbarForLoginPanel from "./NavbarsForHomeAndLoginPanel/NavbarForLoginPanel";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";


const Navbar = ({ setUserData, setUserLogin, cookieStyle }) => {
  const [path, setPath] = useState(window.location.pathname); //home page
  const history = useHistory(); //read current pathname in URL

  useEffect(() => {
    const unlisten = history.listen(() => {
      setPath(history.location.pathname);
      // This will be evaluated on every route change

    });
    // This function will be invoked on component unmount and will clean up
    // the event listener.
    return () => {
      unlisten();
    };
  }, [history, path]);

  const handleWhichNavbar = () => {
    const namesPath = ["/login-panel", "/password-change", "/password-reset", "/registration-panel", "/event", "/admin-panel", "/calendar", "/emails", "/chat", "/panel_post", "/add_post", "/management-panel", "/edit_events"];
    for (let i in namesPath) {
      if (path === namesPath[i]) return true;
    }
    return false
  }

  return (
    <>
      {handleWhichNavbar() ?
        <NavbarForLoginPanel setUserData={setUserData} setUserLogin={setUserLogin} cookieStyle={cookieStyle} /> :
        <NavbarForHome cookieStyle={cookieStyle} />
      }
    </>
  );
}

export default Navbar;