export const props = (listEvents, event, setEvent, handleRemoveEvent, setRemoveEventFromList, handleAddOpinion,
    handleJoinEvent, opinions, handleRemoveOpinion, modalRemoveIsOpen, closeModalRemoveEvent,
    handleRemoveEventAfterAprove, modalAddOpinionIsOpen, closeModalAddOpinion, handleAddOpinionAfterAprove,
    clearFormOpinion, modalJoinEventIsOpen, closeModalJoinEvent, handleJoinEventAfterAprove, inputTitle,
    handleSetInputTitle, inputDescription, handleSetInputDescription, msgNoData, msgNoOpinions) => {
    //PROPS
    const listEventsProps = {
        listEvents,
        event,
        setEvent,
        handleRemoveEvent,
        setRemoveEventFromList,
    }

    const singleEventProps = {
        listEvents,
        event,
        handleRemoveEvent,
        handleAddOpinion,
        handleJoinEvent,
        msgNoData,
    }

    const opinionsProps = {
        opinions,
        handleRemoveOpinion
    }

    const noOpinions = {
        msgNoOpinions
    }

    const removeEventModalProps = {
        isOpen: modalRemoveIsOpen,
        closeModal: closeModalRemoveEvent,
        contentLabel: "Remove event",
        handleRemoveEvent: handleRemoveEventAfterAprove
    }

    const addOpinionModalProps = {
        isOpen: modalAddOpinionIsOpen,
        closeModal: closeModalAddOpinion,
        contentLabel: "Add event",
        handleAddOpinion: handleAddOpinionAfterAprove,
        inputTitle,
        handleSetInputTitle,
        inputDescription,
        handleSetInputDescription,
        clearForm: clearFormOpinion
    }

    const joinEventProps = {
        isOpen: modalJoinEventIsOpen,
        closeModal: closeModalJoinEvent,
        contentLabel: "Join the event",
        handleJoinEvent: handleJoinEventAfterAprove
    }

    return {
        listEventsProps,
        singleEventProps,
        opinionsProps,
        noOpinions,
        removeEventModalProps,
        addOpinionModalProps,
        joinEventProps
    }
}