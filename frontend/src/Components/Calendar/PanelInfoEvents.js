import '../../Styles/PanelInfoEvents.css';
import { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";
import { changeStyle } from '../../AuxilliaryFunctions/changeStyle';
import { isAdminOrReceptionist } from '../../AuxilliaryFunctions/isAdminOrReceptionist';

const PanelInfoEvent = ({ selectedDay, handleRemoveEvent, eventsMap, cookieStyle }) => {

    const history = useHistory();

    const [selectedEvent] = useState({});
    const [showEvent, setShowEvent] = useState(false);

    const handleSelectEvent = selectedEvent => {
        const StartDay = new Date(Date.UTC(selectedEvent.year, selectedEvent.nrMonth, selectedEvent.nrDay, "23", "59", "59")).toISOString();
        const EndDay = new Date(Date.UTC(selectedEvent.year, selectedEvent.nrMonth, selectedEvent.nrDay, "00", "00", "00")).toISOString();

        const state = { StartDay, EndDay, cookieStyle, selectedEventId: selectedEvent.id }
        history.push('/event', [state])
    }

    useEffect(() => {
        setShowEvent(false);
    }, [selectedDay]);

    const handleShowEvent = () => {
        return (
            <p>
                <span className="showEventTitle">
                    {selectedEvent.title}
                </span>
                <span className="showEventDescription">
                    {selectedEvent.description}
                </span>
                <span className="buttonCloseEvent">
                    <button className="btn-primary" onClick={() => setShowEvent(false)}>Wyjdź</button>
                </span>
            </p>
        )
    }
    const handleDisplayListEvents = () => {
        const events = eventsMap.get(selectedDay);
        const listEvents = [];
        if (events.length && selectedDay) {
            events.forEach((event, index) => {
                listEvents.push(
                    <p key={index} className={`eventOnList ${changeStyle(cookieStyle, 'eventOnList')}`}>
                        {event.title}
                        <span className="buttonOnEventList">
                            <button className="btn btn-primary" onClick={() => handleSelectEvent(event)}>Wyświetl</button>
                        </span>
                        {isAdminOrReceptionist() ?
                            <span className="buttonOnEventList">
                                <button className="btn btn-danger" onClick={() => handleRemoveEvent(event.id)}>Usuń wydarzenie</button>
                            </span>
                            : null
                        }
                    </p>
                )
            });
            return listEvents;
        } else {
            return (
                <div>
                    <h1 className="titleNoEvent">Lista wydarzeń</h1>
                    <h2 className="descriptionNoEvent">Wybierz dzień z kalendarza, w którym organizowane są wydarzenia</h2>
                </div>
            );
        }
    }

    return (
        <div className={`${(Array.isArray(eventsMap.get(selectedDay)) &&
            eventsMap.get(selectedDay).length) ?
            `panelInfoEvent ${changeStyle(cookieStyle, 'panelInfoEvent')}`
            :
            `noEvents ${changeStyle(cookieStyle, 'noEvents')}`}`}>
            <div>
                {showEvent ?
                    <div className="showEvent">
                        {handleShowEvent()}
                    </div>
                    :
                    <div>
                        {eventsMap.size && selectedDay ?
                            <div>
                                {handleDisplayListEvents()}
                            </div>
                            :
                            null
                        }
                    </div>
                }
            </div>
        </div>
    )
}

export default PanelInfoEvent;