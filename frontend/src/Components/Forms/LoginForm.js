import logo from "../../Photos/logo.png";

const LoginForm = props => {

    return (
        <div className='LoginPanelForm' id={props.id}>
            <h1>Zaloguj się</h1>
            <form onSubmit={e => props.handleLogin(e)}>
                <img className="mb-4" src={logo} alt="logo" width="72" height="72" />
                <h1 className="h3 mb-3 font-weight-normal">Podaj dane</h1>
                <label htmlFor="inputUserName" className="h3 mb-2">Login</label>
                <input type="text" id="inputUserName" className="form-control" placeholder="Login" required autoFocus
                    onChange={e => { props.setUserName(e.target.value) }}
                    value={props.userName}
                />
                <br />
                <label htmlFor="inputPassword" className="h3 mb-2">Hasło</label>
                <input type="password" id="inputPassword" className="form-control" placeholder="Password" required
                    onChange={e => { props.setPassword(e.target.value) }}
                    value={props.password}
                />
                <div className="checkbox mb-3">
                    <label>
                        <input type="checkbox" value="remember-me" /> Zapamiętaj mnie
                    </label>
                </div>
                <button name="login" type="submit" className="buttonLogin btn btn-lg btn-primary btn-block">Zaloguj się</button>
            </form>
            <p className="mt-3">
                {props.blocked ?
                    <button type="button" className="btn btn-lg btn-danger" onClick={() => props.handleRedirectToResetPass()}>Resetuj hasło</button> :
                    null
                }
            </p>
            <p className="mt-5 mb-3 text-muted">&copy; 2021</p>
        </div>
    )

}

export default LoginForm;