package model

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"github.com/lib/pq"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func GetInstructors() ([]interfaces.Instructor, int) {
	rows, err := DB.Query("SELECT instructors.id, name, surname FROM instructors INNER JOIN billing_address ON instructors.user_id = billing_address.user_id")
	if err != nil {

		return nil, http.StatusBadRequest
	}
	defer rows.Close()

	var instructors []interfaces.Instructor

	for rows.Next() {
		var instructor interfaces.Instructor
		if err := rows.Scan(&instructor.ID, &instructor.Name, &instructor.Surname); err != nil {

			return instructors, http.StatusBadRequest
		}
		instructors = append(instructors, instructor)
	}
	if err = rows.Err(); err != nil {

		return instructors, http.StatusBadRequest
	}

	return instructors, http.StatusOK
}

func GetAuthors() ([]interfaces.Instructor, int) {
	rows, err := DB.Query("SELECT authors.user_id, name, surname FROM authors INNER JOIN billing_address ON authors.user_id = billing_address.user_id")
	if err != nil {

		return nil, http.StatusBadRequest
	}
	defer rows.Close()

	var authors []interfaces.Instructor

	for rows.Next() {
		var author interfaces.Instructor
		if err := rows.Scan(&author.ID, &author.Name, &author.Surname); err != nil {

			return authors, http.StatusBadRequest
		}
		authors = append(authors, author)
	}
	if err = rows.Err(); err != nil {

		return authors, http.StatusBadRequest
	}

	return authors, http.StatusOK
}

func InsertEvent(event interfaces.Event) int {
	query := `INSERT INTO events(title, description, price, place_id, amount_of_participants, 
				created_at, updated_at, start_of_event, end_of_event, author_id, instructor_id) 
				VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`
	_, err := DB.Exec(query, event.Title, event.Description, event.Price, event.PlaceID,
		event.AmountOfParticipants, event.CreatedAt, event.UpdatedAt, event.StartOfEvent, event.EndOfEvent,
		event.AuthorID, event.InstructorID)

	if err != nil {
		//fmt.Println(err)
		return http.StatusBadRequest
	}

	return http.StatusOK
}

func UpdateEvent(id int, event interfaces.Event) int {
	query := `UPDATE events SET title = $1, description = $2, price = $3, place_id = $4, amount_of_participants = $5, 
		updated_at = $6, start_of_event = $7, end_of_event = $8, instructor_id = $9  WHERE id = $10`
	_, err := DB.Exec(query, event.Title, event.Description, event.Price, event.PlaceID,
		event.AmountOfParticipants, event.UpdatedAt, event.StartOfEvent, event.EndOfEvent, event.InstructorID, id)

	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func DeleteEvent(event_id int) int {
	_, err := DB.Exec("DELETE FROM events WHERE id = $1", event_id)
	if err != nil {

		return http.StatusBadRequest
	}

	EventCanceled(event_id)

	return http.StatusOK
}

func GetEvents() ([]interfaces.EventDetailed, int) {
	authors, status := GetAuthors()
	if status != http.StatusOK {

		return nil, http.StatusBadRequest
	}

	instructors, status := GetInstructors()
	if status != http.StatusOK {

		return nil, http.StatusBadRequest
	}

	formattedAuthors := make(map[int]string)
	for _, value := range authors {
		formattedAuthors[int(value.ID)] = value.Name + " " + value.Surname
	}

	formattedInstructors := make(map[int]string)
	for _, value := range instructors {
		formattedInstructors[int(value.ID)] = value.Name + " " + value.Surname
	}

	query := `
		SELECT events.id as event_id, events.title, events.description, events.price, events.place_id, 
			events.amount_of_participants, events.created_at, events.updated_at, events.start_of_event, 
			events.end_of_event, events.author_id, events.instructor_id, places.name_of_place, places.limit_of_participants
		FROM events 
		INNER JOIN places ON places.id = events.place_id
	`

	rows, err := DB.Query(query)

	if err != nil {
		return nil, http.StatusBadRequest
	}
	defer rows.Close()

	var events []interfaces.EventDetailed
	var event interfaces.EventDetailed
	for rows.Next() {
		if err := rows.Scan(&event.ID, &event.Title, &event.Description, &event.Price, &event.PlaceID, &event.AmountOfParticipants,
			&event.CreatedAt, &event.UpdatedAt, &event.StartOfEvent, &event.EndOfEvent, &event.AuthorID, &event.InstructorID, &event.PlaceName, &event.PlaceLimitOfParticipants); err != nil {

			return events, http.StatusBadRequest
		}
		event.Author = formattedAuthors[event.AuthorID]
		if event.InstructorID.Valid {
			event.Instructor.String = formattedInstructors[int(event.InstructorID.Int64)]
			event.Instructor.Valid = true
		} else {
			event.Instructor = interfaces.NullString{}
		}

		events = append(events, event)
	}
	if err = rows.Err(); err != nil {

		return events, http.StatusBadRequest
	}

	return events, http.StatusOK
}

func GetEventsByDay(start, end time.Time) ([]interfaces.EventDetailed, int) {
	authors, status := GetAuthors()
	if status != http.StatusOK {

		return nil, http.StatusBadRequest
	}

	instructors, status := GetInstructors()
	if status != http.StatusOK {

		return nil, http.StatusBadRequest
	}

	formattedAuthors := make(map[int]string)
	for _, value := range authors {
		formattedAuthors[int(value.ID)] = value.Name + " " + value.Surname
	}

	formattedInstructors := make(map[int]string)
	for _, value := range instructors {
		formattedInstructors[int(value.ID)] = value.Name + " " + value.Surname
	}

	query := `SELECT events.id as event_id, title, description, price, place_id, amount_of_participants,
				created_at, updated_at, start_of_event, end_of_event, author_id, instructor_id, name_of_place, places.limit_of_participants
				FROM events
				INNER JOIN places ON places.id = place_id
				WHERE start_of_event <= $1 AND end_of_event >= $2`
	rows, err := DB.Query(query, start, end)

	if err != nil {

		return nil, http.StatusBadRequest
	}
	defer rows.Close()

	var events []interfaces.EventDetailed
	var event interfaces.EventDetailed
	for rows.Next() {
		if err := rows.Scan(&event.ID, &event.Title, &event.Description, &event.Price, &event.PlaceID, &event.AmountOfParticipants,
			&event.CreatedAt, &event.UpdatedAt, &event.StartOfEvent, &event.EndOfEvent, &event.AuthorID, &event.InstructorID,
			&event.PlaceName, &event.PlaceLimitOfParticipants); err != nil {

			return events, http.StatusBadRequest
		}
		event.Author = formattedAuthors[event.AuthorID]
		if event.InstructorID.Valid {
			event.Instructor.String = formattedInstructors[int(event.InstructorID.Int64)]
			event.Instructor.Valid = true
		} else {
			event.Instructor = interfaces.NullString{}
		}

		events = append(events, event)
	}
	if err = rows.Err(); err != nil {

		return events, http.StatusBadRequest
	}

	return events, http.StatusOK
}

func CheckIfEventExists(id int) int {
	rows := DB.QueryRow("SELECT id FROM events WHERE id = $1", id)

	var check int
	err := rows.Scan(&check)
	if err != nil {
		return http.StatusBadRequest
	}

	return http.StatusOK
}

func InsertPlace(place interfaces.Place) int {
	query := `INSERT INTO places(name_of_place, limit_of_participants) VALUES($1, $2)`
	_, err := DB.Exec(query, place.NameOfPlace, place.LimitOfParticipants)

	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetPlaces() ([]interfaces.Place, int) {
	rows, err := DB.Query("SELECT id, name_of_place, limit_of_participants FROM places")
	if err != nil {

		return nil, http.StatusBadRequest
	}
	defer rows.Close()

	var places []interfaces.Place

	for rows.Next() {
		var place interfaces.Place
		if err := rows.Scan(&place.ID, &place.NameOfPlace, &place.LimitOfParticipants); err != nil {

			return places, http.StatusBadRequest
		}
		places = append(places, place)
	}
	if err = rows.Err(); err != nil {

		return places, http.StatusBadRequest
	}

	return places, http.StatusOK
}

func UpdatePlace(id int, place interfaces.Place) int {
	query := `UPDATE places SET name_of_place = $1, limit_of_participants = $2 WHERE id = $3`
	_, err := DB.Exec(query, place.NameOfPlace, place.LimitOfParticipants, id)

	if err != nil {
		fmt.Println(err)
		return http.StatusBadRequest
	}

	return http.StatusOK
}

func DeletePlace(id int) int {
	_, err := DB.Exec("DELETE FROM places WHERE id = $1", id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func CheckForFreePlaces(id int) int {
	query := `
		SELECT events.amount_of_participants, places.limit_of_participants 
		FROM events 
		INNER JOIN places ON places.id = events.place_id 
		WHERE events.id = $1
	`
	rows := DB.QueryRow(query, id)

	var limitOfParticipants interfaces.NullInt64
	var amountOfParticipants interfaces.NullInt64

	err := rows.Scan(&amountOfParticipants, &limitOfParticipants)
	switch {
	case err == sql.ErrNoRows:

		return http.StatusBadRequest
	case err != nil:

		return http.StatusBadRequest
	}

	if amountOfParticipants.Int64 >= limitOfParticipants.Int64 {
		interfaces.MESSAGE = "Liczba uczestników wydarzenia jest pełna!"

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func IncreaseAmountOfParticipants(eventID int) int {
	_, err := DB.Exec("UPDATE events SET amount_of_participants = Coalesce(amount_of_participants, 0) + 1 WHERE id = $1", eventID)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func DecreaseAmountOfParticipants(eventID int) int {
	_, err := DB.Exec("UPDATE events SET amount_of_participants = GREATEST(amount_of_participants - 1, 0) WHERE id = $1", eventID)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func InsertEventParticipants(id int, loggedUserID uint64) (string, int) {
	_, err := DB.Exec("INSERT INTO participants(event_id, user_id) VALUES($1, $2)", id, loggedUserID)
	//fmt.Println(err)
	if err, ok := err.(*pq.Error); ok {
		if err.Constraint == interfaces.PARTICIPANTS_UNIQUE {

			return interfaces.FAIL, http.StatusConflict
		}
	}
	if err != nil {

		return "Coś poszło nie tak", http.StatusBadRequest
	}

	return interfaces.MESSAGE, http.StatusOK
}

func CheckParticipants(id int, loggedUserID uint64) int {
	rows := DB.QueryRow("SELECT * FROM participants WHERE event_id = $1 AND user_id = $2 ", id, loggedUserID)

	var i int
	var userID int
	var eventID int
	err := rows.Scan(&i, &userID, &eventID)
	switch {
	case err == sql.ErrNoRows:

		return http.StatusOK
	case err != nil:

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetShortEventsByID(ids []int) ([]interfaces.ShortEvent, int) {
	var shortEvents []interfaces.ShortEvent
	var shortEvent interfaces.ShortEvent
	events, status := GetEvents()

	if status != http.StatusOK {

		return shortEvents, http.StatusBadRequest
	}

	for _, id := range ids {
		for _, event := range events {
			if int(event.ID) == id {
				shortEvent.ID = event.ID
				shortEvent.Title = event.Title
				shortEvent.StartOfEvent = event.StartOfEvent
				shortEvent.EndOfEvent = event.EndOfEvent

				shortEvents = append(shortEvents, shortEvent)
				break
			}
		}
	}

	return shortEvents, http.StatusOK
}
