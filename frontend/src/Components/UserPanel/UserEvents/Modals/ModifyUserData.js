import Modal from 'react-modal';

const ModifyUserData = props => {

    const customStyles = {
        content: {
            minWidth: '450px',
            minHeight: '355px',
            top: '50%',
            left: '50%',
            right: '75%',
            height: '20%',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            backgroundColor: "rgb(48, 52, 92)",
            color: "white"
        },
    };

    Modal.setAppElement('#root');

    return (
        <>
            <Modal
                isOpen={props.isOpen}
                style={customStyles}
            >
                <div className='window'>
                    <h2 className='titleModifyData'>{props.title}</h2>
                    <div>
                        <form onSubmit={e => props.approve(e)}>
                            <label className="h3 mb-3">Imię
                                <input type="text" className="form-control" placeholder="Imię"
                                    name="name"
                                    onChange={e => { props.changeDataUser(e) }}
                                    value={props.dataUser.name}
                                />
                            </label>
                            <label className="h3 mb-3">Nazwisko
                                <input type="text" className="form-control" placeholder="Nazwisko"
                                    name="surname"
                                    onChange={e => { props.changeDataUser(e) }}
                                    value={props.dataUser.surname}
                                />
                            </label>
                            <div className="titleModifyData__btns">
                                <button onClick={() => props.closeModal(false)} className="btn btn-secondary btn-block singleButton">Zamknij</button>
                                <button type="submit" className="btn btn-success btn-block singleButton">{props.nameBtn}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default ModifyUserData;