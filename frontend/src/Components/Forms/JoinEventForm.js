const JoinEventForm = props => {
    return (
        <div className="window">
            <h2 className="title">Czy chcesz zapisać się na to wydarzenie?</h2>
            <div>
                <form onSubmit={e => props.handleJoinEvent(e)}>
                    <div className="buttonsRemoveEvent">
                        <button onClick={props.closeModal} className="btn btn-secondary btn-block singleButton">Zamknij</button>
                        <button type="submit" className="btn btn-success btn-block singleButton">Zapisz się</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default JoinEventForm;