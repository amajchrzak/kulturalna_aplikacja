import '../../../Styles/Table.css';
import '../../../Styles/PanelPost.css';
import React, {useState, useEffect } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../../Notification/Notification';
import { useHistory } from "react-router-dom";
import { nameHost } from '../../../GlobalVariables/GlobalVariables';
import '../../../Styles/PanelPost.css';
import Pagination from './Pagination';




const columnsMock = [
  {
    header: " ID",
    accessor: "id", // accessor is the "key" in the data
  },
  {
    header: "Tytuł",
    accessor: "title",
  },
  {
    header: "Opis",
    accessor: "description",
  },
  {
    header: "",
    accessor: "edit",
  },
  {
    header: "",
    accessor: "delte",
  },
];
 
const PanelPost = (props) => {

  let history = useHistory();




    //const UrlShowPosts ="https://mocki.io/v1/d01f2e5a-496a-4c79-bff6-d0623ea0166f";
   const UrlShowPosts = nameHost + "/get-posts";
 

  const [listPosts, setListPosts] = useState([]);
  const [refresh, setRefresh] =  useState(true);

  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(4);


  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = listPosts.slice(indexOfFirstPost, indexOfLastPost);
  const paginate = pageNumber => setCurrentPage(pageNumber );
 
  useEffect(() => {
  handlePosts();
  },[refresh])

  const handleButtonReturn = () => {
    history.push({ 
      pathname: '/admin-panel'
      
     })
  }

  const token = localStorage.getItem("token");

  const handlePosts = () => {
    axios.get(UrlShowPosts, {
     
    })
        .then(res => {
          console.log("Response data about events", res)
          
            if (Array.isArray(res.data.posts) && res.data.posts.length) {
                setListPosts(res.data.posts);
            } else {
                setListPosts([]);
            }
                
        })
        .catch(err => {
            console.error("Error", err);
        }) }

  const AddPostComponent = () => {
    history.push({ 
        pathname: '/add_post'
        
       })
}
   //const UrlSuccessDeletePost = "https://mocki.io/v1/615dca9e-99a6-491b-935b-9d293859b692";
  const UrlSuccessDeletePost = nameHost + "/delete-post/";

  const deletePost = (id) =>{
 
    console.log(id);
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Na pewno usunąć ten wpis?',
      text: "Cofnięcie tej akcji będzie niemożliwe",
      icon: 'warning',
      showCancelButton: true,      
      confirmButtonText: 'Tak, usuń wpis',
      cancelButtonText: 'Anuluj',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {

        axios.delete(UrlSuccessDeletePost + id, {
        headers: {
          Authorization: 'Bearer ' + token
        },
    //    ID: id,
                       
     }).then(res => {              
 
         console.info("###Response from PostDelete endpoint:", res);
        
         if (res.status === 200) {
           
             if (res.data.message === "SUCCESS") {          

              swalWithBootstrapButtons.fire(
                'Sukces!',
                'Wpis został usunięty',
                'success'
              )            
              
             setRefresh(prevState => !prevState)
               store.addNotification({
                 ...notificationTemplate,
                 type: 'success',
                 title: 'Sukces!',
                 message: 'Pomyślnie usunięto wpis',
                 dismiss: {
                    duration: 8000,
                    onScreen: true
                }
                })}
      }
 
     
     }).catch(err => {
         console.error("Error from ShowEvent endpoint", err);
 
         store.addNotification({
           ...notificationTemplate,
           type: 'danger',
           title: 'Error',
           message: 'Błąd z usunięciem wpisu',
           dismiss: {
              duration: 8000,
              onScreen: true
          }
          })
     });
   

        
      } else if (
       
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Anulowano',
          'Wpis nie został usunięty',
          'error'
        )
      }
    })

  }

  const EditPost = (id) => {
 
 
  //  const addressUrlEditPost = "https://mocki.io/v1/a99f95af-4604-4910-af75-69dc3ceefdae";

  // axios.patch(`${nameHost}/delete-user/${idDeletedUser}`, {}, {
   //   headers: {
      //    Authorization: 'Bearer ' + token
     // }
//  })


    //axios.get(addressUrlEditPost, {
      //id: id    


     const postToEdit = listPosts.find((element) =>  element.ID === id  )  

     history.push({ 
      pathname: '/update-post',
      state:  {
        id : postToEdit.ID,  
        title: postToEdit.Title,
        description: postToEdit.Description
       }
     })
           
/*
    axios.patch(`${nameHost}/update-post/${id}`, {}, {
           headers: {
              Authorization: 'Bearer ' + token
         }
     
   }).then(res => {            
      console.info("###Response from EditPost endpoint:", res);
       if (res.status === 200) {     
    
      
        history.push({ 
            pathname: '/update-post',
            state:  {
              id : postToEdit.Id,  
              title: postToEdit.Title,
              description: postToEdit.Description
             }
           })
                 
       }

   
   }).catch(err => {
       console.error("Error from EditEvents endpoint", err);
      
       store.addNotification({
           ...notificationTemplate,
           type: 'danger',
           title: 'Błąd ',
           message: 'Nie udało się pobrać wpisu do edycji',
           dismiss: {
               duration: 8000,
               onScreen: true
           }
       })
   })*/
  
    }
 
  return (
    <div className="panel_post">
      <div className="titlepost">Panel Wpisów</div>
    <div id="buttonaddpost">
      <button className='btn btn-primary btn-lg btn-block' onClick={() => AddPostComponent()}>
          Dodaj nowy wpis
       </button>
   </div>
   <div id="tablepost">
    <table>
      <thead>
        <tr>
          {columnsMock.map(({ header, accessor }) => (
            <th key={accessor}>{header}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {currentPosts.map(
          ({ ID, Title, Description }) => (
            <tr>
              <td>{ID}</td>
              <td className="titlepostpanel">{Title}</td>
              <td className="descriptionpost">{Description.substring(0, 700)}...</td>
              <td><button onClick={() => EditPost(ID)} >Edytuj</button></td>              
              <td><button onClick={() => deletePost(ID)}>Usuń</button></td>
            </tr>
          )
        )}
        
      </tbody>
    </table><div> 
     </div>
     </div><div id="paginactionposts"><Pagination
        postsPerPage={postsPerPage}
        totalPosts={listPosts.length}
        paginate={paginate}
      />Strona: {currentPage}</div>
      <button className="btn btn-success btn-block singleButton buttonreturn" onClick={handleButtonReturn}>Powrót</button></div>
  );
};
 
export default PanelPost;
