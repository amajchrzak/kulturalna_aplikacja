import Modal from 'react-modal';
import AddOpinionForm from '../Forms/AddOpinionForm';

const AddOpinionModal = props => {

    const customStyles = {
        content: {
            minWidth: '450px',
            minHeight: '500px',
            top: '50%',
            left: '50%',
            right: '75%',
            height: '20%',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            backgroundColor: "rgb(48, 52, 92)",
            color: "white"
        },
    };

    Modal.setAppElement('#root');

    return (
        <>
            <Modal
                isOpen={props.isOpen}
                onRequestClose={props.onRequestClose}
                style={customStyles}
            >
                <AddOpinionForm
                    handleAddOpinion={props.handleAddOpinion}
                    closeModal={props.closeModal}
                    inputTitle={props.inputTitle}
                    handleSetInputTitle={props.handleSetInputTitle}
                    inputDescription={props.inputDescription}
                    handleSetInputDescription={props.handleSetInputDescription}
                    clearForm={props.clearForm}
                />
            </Modal>
        </>
    )
}

export default AddOpinionModal;