const UserList = ({ setReceiver, users }) => {

    const handleUserList = () => {
        const usersArray = [];

        users.forEach((user, index) => {
            if (localStorage.getItem('user_id') !== String(user.ID)) {
                usersArray.push(
                    <li value={user.ID} key={index} onClick={e => setReceiver(e.target.value)} className='chat__single-user'>
                        {user.Username ? user.Username : "Anonimowy użytkownik"}
                    </li>
                )
            }
        })

        return usersArray;
    }

    return (
        <ul className='chat__single-user-ul'>
            {handleUserList()}
        </ul>
    )
}

export default UserList;