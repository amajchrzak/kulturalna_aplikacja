import Modal from 'react-modal';
import '../../Styles/ModalViewRemoveEvent.css';
import RemoveEventForm from '../Forms/RemoveEventForm';

const RemoveEventModal = props => {

    const customStyles = {
        content: {
            minWidth: '450px',
            minHeight: '150px',
            top: '50%',
            left: '50%',
            right: '75%',
            height: '20%',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            backgroundColor: "rgb(48, 52, 92)",
            color: "white"
        },
    };

    Modal.setAppElement('#root');

    return (
        <>
            <Modal
                isOpen={props.isOpen}
                onRequestClose={props.onRequestClose}
                style={customStyles}
            >
                <RemoveEventForm
                    handleRemoveEvent={props.handleRemoveEvent}
                    closeModal={props.closeModal}
                />
            </Modal>
        </>
    )
}

export default RemoveEventModal;