package helpers

import (
	"net/http"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

var allowedHeaders = "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"

func SetHeaders(w http.ResponseWriter, r *http.Request) {
	interfaces.MESSAGE = "SUCCESS"

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
}
