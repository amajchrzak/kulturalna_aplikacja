const handleHowManyDays = (daysInMonth) => {
    const days = [];
    for (let i = 1; i <= daysInMonth; i++) {
        days.push(i);
    }
    return days;
}

export default handleHowManyDays;