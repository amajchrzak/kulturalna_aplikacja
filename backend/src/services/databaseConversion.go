package services

import (
	"database/sql"
	"time"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func NewNullString(s string) interfaces.NullString {
	if len(s) == 0 {
		return interfaces.NullString{}
	}
	return interfaces.NullString{
		NullString: sql.NullString{
			String: s,
			Valid:  true,
		},
	}
}

func NewNullInt(i int64) interfaces.NullInt64 {
	if i == 0 {

		return interfaces.NullInt64{}
	}

	return interfaces.NullInt64{
		NullInt64: sql.NullInt64{
			Int64: i,
			Valid: true,
		},
	}
}

func NewNullTime(t time.Time) interfaces.NullTime {
	if t.IsZero() {

		return interfaces.NullTime{}
	}

	return interfaces.NullTime{
		NullTime: sql.NullTime{
			Time:  t,
			Valid: true,
		},
	}
}

// func NewTime(t interfaces.NullTime) time.Time {
// 	if t.Valid == false {
// 		t.Time.
// 	}

// 	return time.Now()
// }
