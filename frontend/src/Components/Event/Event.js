import { useEffect, useState } from "react";
import { useLocation, useHistory } from "react-router-dom";
import '../../Styles/Event.css'
import RemoveEventModal from "../Calendar/RemoveEventModal";
import AddOpinionModal from "../Calendar/AddOpinionModal";
import JoinEvent from "../Calendar/JoinEvent";
import LoginPanel from "../LoginPanel";
import handleIsToken from "../../AuxilliaryFunctions/handleIsToken";
import { SingleEvent } from "./SingleEvent";
import { ListEvents } from "./ListEvents";
import { Opinions } from "./Opinios";
import { NoOpinions } from "./NoOpinions";
import { addOpinionAfterAprove, getEvents, getOpinions, joinEventAfterAprove, removeEventAfterAprove, removeOpinion } from "./AxiosFunc";
import { props } from "./Props";
import { changeStyle } from "../../AuxilliaryFunctions/changeStyle";

const msgNoData = "brak informacji";
const msgNoOpinions = "Do tego wydarzenia nie dodano jeszcze żadnej opinii";
const tileOtherEvents = "Inne wydarzenia tego dnia";
const opinionTitle = "Opinie";

const Event = () => {

    const location = useLocation();
    const history = useHistory();

    const [event, setEvent] = useState("");
    const [listEvents, setListEvents] = useState([]);
    const [opinions, setOpinions] = useState([]);
    const [modalRemoveIsOpen, setModalRemoveIsOpen] = useState(false);
    const [removeEventFromList, setRemoveEventFromList] = useState("");
    const [modalAddOpinionIsOpen, setModalAddOpinionIsOpen] = useState(false);
    const [inputTitle, setInputTitle] = useState("");
    const [inputDescription, setInputDescription] = useState("");
    const [modalJoinEventIsOpen, setModalJoinEventIsOpen] = useState(false);
    const [counterOpinions, setCounterOpinions] = useState(0);
    const [refresh, setRefresh] = useState(false);

    /////////////////////////////////////
    //Remove event (modal)
    const openModalRemoveEvent = () => {
        setModalRemoveIsOpen(true);
    }

    const closeModalRemoveEvent = () => {
        setModalRemoveIsOpen(false);
    }
    /////////////////////////////////////
    //Add event (modal)
    const openModalAddOpinion = () => {
        setModalAddOpinionIsOpen(true);
    }

    const closeModalAddOpinion = () => {
        setModalAddOpinionIsOpen(false);
    }
    /////////////////////////////////////
    //Join the event (modal)
    const openModalJoinEvent = () => {
        setModalJoinEventIsOpen(true);
    }

    const closeModalJoinEvent = () => {
        setModalJoinEventIsOpen(false);
    }
    /////////////////////////////////////
    //Opinions
    const handleGetOpinions = () => {
        getOpinions(event, setOpinions);
    }

    const handleRemoveOpinion = (e, opinionEventId, opinionID) => {
        removeOpinion(e, opinionEventId, opinionID, setRefresh);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //REMOVE EVENT
    const handleActionAfterRemove = () => {
        if (listEvents.length > 1) {
            const otherEvents = listEvents.filter(singleEvent => singleEvent.ID !== event.ID);
            setEvent(otherEvents[0]); //change view, display next event
        } else {
            history.push("/login-panel"); //display calendar
        }
    }

    const handleRemoveEvent = () => {
        openModalRemoveEvent();
    }

    const handleRemoveEventAfterAprove = e => {
        removeEventAfterAprove(e, event, removeEventFromList, closeModalRemoveEvent, setRemoveEventFromList, handleActionAfterRemove);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //ADD OPINION
    const handleAddOpinion = () => {
        openModalAddOpinion();
    }

    const handleSetInputDescription = value => {
        if (value.length <= 300) {
            setInputDescription(value);
        }
    }

    const handleSetInputTitle = value => {
        if (value.length <= 30) {
            setInputTitle(value);
        }
    }

    const handleAddOpinionAfterAprove = e => {
        addOpinionAfterAprove(e, inputTitle, inputDescription, event, setRefresh, closeModalAddOpinion, setInputTitle, setInputDescription);
    }

    const clearFormOpinion = () => {
        setInputTitle("");
        setInputDescription("");
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //JOIN THE EVENT
    const handleJoinEvent = () => {
        openModalJoinEvent();
    }

    const handleJoinEventAfterAprove = e => {
        joinEventAfterAprove(e, event, setRefresh, closeModalJoinEvent);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    const handleGetEvents = () => {
        getEvents(setEvent, setListEvents, location.state[0].StartDay, location.state[0].EndDay, location.state[0].selectedEventId);
    }

    useEffect(() => {
        handleGetEvents();

        return () => { //cleanup function
            setEvent({});
            setListEvents([]);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [refresh])

    useEffect(() => {
        handleGetOpinions();

        return () => { //cleanup function
            setOpinions([]);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [refresh, event.ID]);

    const allProps = props(listEvents, event, setEvent, handleRemoveEvent, setRemoveEventFromList, handleAddOpinion,
        handleJoinEvent, opinions, handleRemoveOpinion, modalRemoveIsOpen, closeModalRemoveEvent,
        handleRemoveEventAfterAprove, modalAddOpinionIsOpen, closeModalAddOpinion, handleAddOpinionAfterAprove,
        clearFormOpinion, modalJoinEventIsOpen, closeModalJoinEvent, handleJoinEventAfterAprove, inputTitle,
        handleSetInputTitle, inputDescription, handleSetInputDescription, msgNoData, msgNoOpinions);

    const handleNextOpinion = () => {
        const someOpinions = allProps.opinionsProps.opinions.slice(0 + counterOpinions, 10 + counterOpinions);
        const prepareProps = { ...allProps.opinionsProps };
        prepareProps.opinions = someOpinions;
        return <Opinions {...prepareProps} />
    }

    const nextOpinions = () => {
        setCounterOpinions(prevState => prevState += 10);
    }

    const prevOpinion = () => {
        setCounterOpinions(prevState => prevState -= 10);
    }

    return (
        <>
            {handleIsToken() ?
                <div className='event'>
                    <div className='eventPanel'>
                        <div className={`${listEvents.length > 1 ? 'listEvents' : null}`}>
                            {listEvents.length > 1 ?
                                <h1 className='h1Event h1ListEvents'>{tileOtherEvents}</h1>
                                :
                                null
                            }
                            <div className={`${listEvents.length > 1 ? 'listEvents__events' : null}`}>
                                <ListEvents {...allProps.listEventsProps} cookieStyle={location.state[0].cookieStyle} />
                            </div>
                        </div>
                        <SingleEvent {...allProps.singleEventProps} cookieStyle={location.state[0].cookieStyle} />
                    </div>
                    <div className={`opinions ${changeStyle(location.state[0].cookieStyle, 'opinions')}`}>
                        <h1>{opinionTitle}</h1>
                        {opinions.length ?
                            handleNextOpinion()
                            :
                            <NoOpinions  {...allProps.noOpinions} />
                        }
                        <div className='opinions__btns'>
                            <button
                                className='opinions__btn btn btn-primary'
                                onClick={() => prevOpinion()}
                                disabled={counterOpinions ? false : true}
                            >POPRZEDIE
                            </button>
                            <button
                                className='opinions__btn btn btn-primary'
                                onClick={() => nextOpinions()}
                                disabled={counterOpinions + 10 < opinions.length ? false : true}
                            >NASTĘPNE</button>
                        </div>
                    </div>
                    <RemoveEventModal {...allProps.removeEventModalProps} />
                    <AddOpinionModal {...allProps.addOpinionModalProps} />
                    <JoinEvent {...allProps.joinEventProps} />
                </div>
                :
                <LoginPanel />
            }
        </>
    )
}

export default Event;