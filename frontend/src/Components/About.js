import React from 'react';
import '../Styles/About.css'
import img_about_1 from '../Photos/about_photo1.jpg'

const About = ({ id }) => {
    return (
        <div className="about jumbotron p-3 p-md-5 text-white rounded bg-dark" id={id}>
            <div className="about-text ol-md-6 px-0">
                <h1 className="about-title display-4 font-italic">W skrócie o naszym Domu Kultury</h1>
                <p className="lead my-3">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nihil repellat ex iure blanditiis fuga! Impedit itaque blanditiis, facere error at mollitia numquam nulla magni officia, praesentium incidunt ipsam fuga dolorum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum magnam, voluptate illo repellendus ratione ut sint explicabo officia voluptatem inventore sed at dolor, a quae nobis minus totam quibusdam laboriosam?</p>
                {/* <p className="lead mb-0"><a href="" className="text-white font-weight-bold">Czytaj dalej..</a></p> */}
            </div>
            <div className="about-photo">
                <img src={img_about_1} alt="img_about_1.jpg" />
            </div>
        </div>
    );
}

export default About;