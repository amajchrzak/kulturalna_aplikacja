import '../Styles/PasswordChange.css';
import PasswordChangeForm from './Forms/PasswordChangeForm';
import LoginPanel from './LoginPanel';
import handleIsToken from '../AuxilliaryFunctions/handleIsToken.js';
import axios from 'axios';
import { useState } from 'react';
import { store } from 'react-notifications-component';
import { notificationTemplate } from './Notification/Notification';
import { useHistory } from "react-router-dom";
import { nameHost } from '../GlobalVariables/GlobalVariables';


const PasswordChange = ({ userData, setUserData }) => {

    const [inputCurrentPassword, setCurrentPassword] = useState("");
    const [inputNewPassword, setNewPassword] = useState("");
    const [retypedNewPassword, setRetypedNewPassword] = useState("");
    const history = useHistory();

    const handleClearForm = () => {
        console.info("###CLEAR FORM PROCESS");
        setCurrentPassword("");
        setNewPassword("");
        setRetypedNewPassword("");
    }

    const handleLogout = () => {
        const clearDataUser = {
            name: "",
            surname: ""
        };

        setUserData(clearDataUser);
        localStorage.removeItem("token");
        history.push("/login-panel");
    }

    const handleIncorrectPasswordMessage = () => {
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Hasło nie spełnia wymagań bezpieczeństwa',
            message: 'Hasło musi zawiarać minimum jedną małą lub wielką literę oraz minimum jeden znak specjalny oraz minimalną długość 5 znaków.',
            dismiss: {
                duration: 15000,
                onScreen: true
            }
        })
    }

    const handleComparePasswords = () => {
        return inputNewPassword === retypedNewPassword;
    }

    const handleComparePasswordMessage = () => {
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Podane hasła różnią się od siebie',
            message: 'Pole Hasło i Powtórz hasło muszą zawierać takie same hasło.',
            dismiss: {
                duration: 10000,
                onScreen: true
            }
        })
    }

    const handleTestPassword = pass => {
        const reg = {
            'small': /[a-z]/,
            'special': /[!@#$%^&*]/,
        };
        return (reg.small.test(pass) || reg.capital.test(pass)) &&
            reg.special.test(pass) &&
            pass.length >= 5 && pass.length <= 30;
    }

    const handleChangePassword = e => {
        console.info("###PASSWORD CHANGE PROCESS");
        e.preventDefault();
        handleClearForm();

        const testPassword = handleTestPassword(inputNewPassword);
        console.log(testPassword)
        let comparePassword = false;

        if (!testPassword) {
            handleIncorrectPasswordMessage();
        } else {
            comparePassword = handleComparePasswords();
            if (!comparePassword) {
                handleComparePasswordMessage();
            }
        }

        if (testPassword && comparePassword) {

            const token = localStorage.getItem("token");

            const config = {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }

            const id = localStorage.getItem("user_id");
            const url = `${nameHost}/change-password/${id}`;
            // const addressUrlLogin = "http://localhost:4000/change-password";
            axios.post(url,
                {
                    oldPassword: inputCurrentPassword,
                    newPassword: inputNewPassword,
                    retypedNewPassword: retypedNewPassword
                },
                config
            ).then(res => {
                console.info("Response from change password endpoint:", res);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'success',
                    title: 'Poprawnie zmieniono hasło',
                    message: 'Zaloguj się używając nowego hasła.'
                })

                handleLogout();

                //TODO
                //next step after success


            }).catch(err => {
                console.error("Error from change password endpoint", err);

                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Problem przy zmianie hasła',
                    message: 'Nie udało się zmienić hasła.'
                })
            });
        }
    };

    return (
        <div>
            {handleIsToken() ?
                <PasswordChangeForm
                    handleChangePassword={handleChangePassword}
                    setCurrentPassword={setCurrentPassword}
                    setNewPassword={setNewPassword}
                    inputCurrentPassword={inputCurrentPassword}
                    inputNewPassword={inputNewPassword}
                    retypedNewPassword={retypedNewPassword}
                    setRetypedNewPassword={setRetypedNewPassword}
                /> :
                <LoginPanel />
            }
        </div>
    )
}

export default PasswordChange;