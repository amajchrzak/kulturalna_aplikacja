import React from 'react';
import '../Styles/Contact.css'

const Contact = ({ id }) => {
    return (
        <div className="container contact" id={id}>
            <div role="main" className="inner cover">
                <h1 className="cover-heading">Napisz do nas</h1>
                <form className='contactForm'>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlInput1" className="form-label">Twój adress email</label>
                        <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="twoj_email@wzor.pl" />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlTextarea1" className="form-label">Treść wiadomości</label>
                        <textarea className="form-control" id="exampleFormControlTextarea1" rows="8" ></textarea>
                    </div>
                    <input className="btn btn-primary contact_btn" type="submit" value="Wyślij" />
                </form>
            </div>
        </div >
    );
}

export default Contact;