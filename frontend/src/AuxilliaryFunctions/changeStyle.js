
const nameStyle1 = 'style-1';
const nameStyle2 = 'style-2';
const separator = '--';
const classNames = [ //css class names
    'App', 'nav', 'linkOtherSection', 'userInfo', 'option', 'managementPanel',
    'calendarNamesDays', 'calendarMonth', 'nameYear', 'nameMonth',
    'numberDay', 'eventOnList', 'noEvents', 'userEvents', 'opinions',
    'panelInfoEvent', 'email',  'panel_post'
]

export const changeStyle = (cookieStyle, nameClass) => {
    const isFound = classNames.findIndex(className => className === nameClass)

    if (isFound > -1) {
        if (cookieStyle === nameStyle1) return `${nameClass}${separator}${nameStyle1}`;
        else if (cookieStyle === nameStyle2) return `${nameClass}${separator}${nameStyle2}`;
        else return null
    } else {
        return null;
    }
}