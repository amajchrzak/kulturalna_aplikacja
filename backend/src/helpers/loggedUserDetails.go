package helpers

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/services"
)

func GetLoggedUserID(r *http.Request) uint64 {
	token := strings.Split(r.Header.Get("Authorization"), "Bearer ")
	loggedUserID, status := services.ExtractClaims(token[1])
	if status != http.StatusOK {

		return http.StatusUnauthorized
	}

	return uint64(loggedUserID.(float64))
}

func CheckRightUser(requestedID int, userID uint64) int {
	fmt.Println(requestedID, userID)
	if requestedID != int(userID) {

		return http.StatusBadRequest
	}

	return http.StatusOK
}
