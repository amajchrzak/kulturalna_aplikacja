import { useState } from 'react';
import { db } from '../../firebase';
import firebase from 'firebase'

const SendMessages = ({ receiver }) => {

    const [message, setMessage] = useState('');

    const sendMessage = async (e) => {
        e.preventDefault();
        if (receiver && message) {
            //convert to string
            const sender = localStorage.getItem('user_id') + "";
            const receiverMess = receiver + "";

            await db.collection('messages').add({
                text: message,
                userName: localStorage.getItem('user'),
                createdAt: firebase.firestore.FieldValue.serverTimestamp(),
                sender,
                receiver: receiverMess
            });
            setMessage('');
        }
    }

    const onEnterPressSubmit = e => {
        if (e.keyCode === 13 && e.shiftKey === false) {
            e.preventDefault();
            sendMessage(e);
        }
    }

    const handleSetMessage = mess => {
        if (mess.length <= 300) {
            setMessage(mess);
        }
    }

    return (
        <div className="send-message">
            <form onSubmit={sendMessage}>
                <textarea className="send-message__textarea" rows='5' cols='60'
                    value={message} onChange={e => handleSetMessage(e.target.value)}
                    onKeyDown={e => onEnterPressSubmit(e)}
                    placeholder="Message..."
                    disabled={!receiver}
                />
                <button className="btn btn-primary send-message__btn" type="submit" disabled={!receiver}>Wyślij</button>
            </form>
        </div >
    )
}

export default SendMessages;