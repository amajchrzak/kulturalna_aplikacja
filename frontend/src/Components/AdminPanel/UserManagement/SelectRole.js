const SelectRole = (userId, roles, openModalAssignRoleOpen) => {

    let role = 1; //default value 

    const roleList = [];
    roles.forEach(role => {
        roleList.push(
            <option key={role.ID} value={role.ID}>{role.Name}</option>
        )
    });
    return (
        <div className='selectRole'>
            <select
                className='form-select select'
                placeholder="Role"
                onChange={e => role = e.target.value}
            >
                <option value="0" disabled>Select a role</option>
                {roleList}
            </select>
            <button
                className='btn btn-primary'
                onClick={e => openModalAssignRoleOpen(e, role, userId)}
            >
                Przypisz rolę
            </button>
        </div>
    )
}

export default SelectRole;