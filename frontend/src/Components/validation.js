import { notificationTemplate } from './Notification/Notification';
import { store } from 'react-notifications-component';

const validation = (values) => {

    const emailValidator = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordValidator = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/;
    let errors = {};

    if(!values.username){

        errors.username="Pole wymagane"; 
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Brak Loginu',
            message: 'Należy wypełnić pole Login.',
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        })
     }    
      
       

    if(!values.email){
      errors.email = "Pole wymagane"
      store.addNotification({
          ...notificationTemplate,
          type: 'danger',
          title: 'Brak Emaila',
          message: 'Należy wypełnić pole Emaila.',
          dismiss: {
            duration: 8000,
            onScreen: true
        }
      })
      
    }
    else if (!emailValidator.test(values.email)){
        errors.email = "Podany Email jest niepoprawny"
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Niepoprawny Email',
            message: 'Email musi zawierać znak specjalny oraz wielką literę ',
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        })
        
    }
    if(!values.passwordRegister){
        errors.passwordRegister = "Pole wymagane"
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Brak hasła',
            message: 'Należy wypełnić pole Hasło.',
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        })
    }else if(values.passwordRegister.length < 5){
            errors.passwordRegister = "Hasło musi mieć co najmniej 5 znaków" 
            store.addNotification({
                ...notificationTemplate,
                type: 'danger',
                title: 'Zbyt krótkie hasło',
                message: 'Hasło musi mieć min. 5 znaków.',
                dismiss: {
                    duration: 8000,
                    onScreen: true
                }
            })
    }else if (!passwordValidator.test(values.passwordRegister)){
        errors.passwordRegister = "Hasło jest niepoprawne" 
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Hasło jest niepoprawne',
            message: 'Hasło musi mieć minimum jeden znak specjalny, wielka i małą literę',
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        })

    }

    if(!values.passwordRegisterConfirm){
        errors.passwordRegisterConfirm = "Pole wymagane"
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Brak Powtórzenia Hasła',
            message: 'Należy wypełnić pole Powtórz Hasło.',
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        })
    }

    if(errors)




    return  errors;
    
};

export default validation;
