import React, { useEffect, useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'

import Home from './Components/Home';
import Navbar from './Components/Navbar';
import LoginPanel from './Components/LoginPanel';
import PasswordChange from './Components/PasswordChange';
import PasswordReset from './Components/PasswordReset';
import Event from './Components/Event/Event'
import AdminPanel from './Components/AdminPanel/AdminPanel';
import Calendar from './Components/Calendar/Calendar';
import Emails from './Components/Emails/Email';
import ChatPanel from './Components/UserPanel/ChatPanels';
import AddPost from './Components/AdminPanel/AddPostPanel/AddPost';
import EditPost from './Components/AdminPanel/AddPostPanel/EditPost';
import PanelPost from './Components/AdminPanel/AddPostPanel/PanelPost';

import RegistrationPanel from "./Components/RegistrationPanel";
import ManagementPanel from './Components/AdminPanel/Management_Panel_Events/ManagementPanel';
import EditEventsPanel from './Components/AdminPanel/Management_Panel_Events/EditEventsPanel';

import 'react-slideshow-image/dist/styles.css';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Footer from './Components/Footer';
import axios from 'axios';

import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css'

import { nameHost } from './GlobalVariables/GlobalVariables';
import { withCookies, useCookies } from 'react-cookie';
import { changeStyle } from './AuxilliaryFunctions/changeStyle';

const App = (props) => {

  const [userData, setUserData] = useState({
    name: "",
  });
  const [cookies] = useCookies(['style']);

  const handleDataUser = async () => {
    const url = `${nameHost}/get-logged-user`;
    // const url = "https://mocki.io/v1/8d255fc8-63da-4b1e-b5f1-e25f83ce36e1"
    const token = localStorage.getItem("token");
    await axios.get(url, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    }).then(res => {
      console.info("###RES USER DATA", res)
      if (res.data.message === "SUCCESS") {
        localStorage.setItem("user_id", res.data.id);
        localStorage.setItem("user", res.data.username);
        localStorage.setItem("role_id", res.data.role.ID);
      } else { //if unauthorized remove items from local storage or bad request
        // localStorage.removeItem("token");
        // localStorage.removeItem("user");
        // localStorage.removeItem("user_id");
      }
    }).catch(err => {
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      localStorage.removeItem("user_id");
      localStorage.removeItem("role_id");
      console.error("Error", err);
    })
    console.info("###USER DATA", userData)
  }

  useEffect(() => {
    const checkUserData = () => {
      const token = localStorage.getItem("token");
      const role_id = localStorage.getItem("role_id");
      const user_id = localStorage.getItem("user_id");
      const user = localStorage.getItem("user");

      if (token || role_id || user_id || user) {
        handleDataUser();
      }
    }

    if (localStorage.getItem('token')) {
      window.addEventListener('storage', checkUserData);
    }

    return () => { //cleanup function
      setUserData({});
      window.removeEventListener('storage', checkUserData);
    };
  }, []);

  return (
    <div className={`App ${changeStyle(cookies?.style, 'App')}`}>
      <ReactNotification />
      <BrowserRouter>
        <Navbar
          setUserData={setUserData}
          cookieStyle={props.cookies?.cookies?.style}
        />
        <Switch>
          <Route
            component={Home}
            path="/home"
          />
          <Route
            render={() => (
              <AdminPanel
                userData={userData}
                setUserData={setUserData}
                cookieStyle={props.cookies?.cookies?.style}
              />
            )}
            path="/admin-panel"
          />
          <Route
            render={() => (
              <Calendar
                cookieStyle={props.cookies?.cookies?.style}
              />
            )}
            path="/calendar"
          />
          <Route
            render={() => (
              <LoginPanel userData={userData} setUserData={setUserData} />
            )}
            path="/login-panel"
          />
          <Route
            render={() => (
              <PasswordChange userData={userData} setUserData={setUserData} />
            )}
            path="/password-change"
          />
          <Route
            component={PasswordReset}
            path="/password-reset"
          />
          <Route
            component={RegistrationPanel}
            path="/registration-panel"
          />
          <Route
            component={ManagementPanel}
            path="/management-panel"
          />
          <Route
            component={Event}
            path="/event"
          />

          <Route
          component={EditEventsPanel}
          path="/edit_events"
          />
          <Route
            render={() => (
              <Emails
                cookieStyle={props.cookies?.cookies?.style}
              />
            )}
            path="/emails"
          />
          <Route
            component={ChatPanel}
            path="/chat"
          />

          <Route
            component={AddPost}
            path="/add_post"
          />

          <Route
            component={EditPost}
            path="/update-post"
          />

          <Route
            component={PanelPost}
            path="/panel_post"
          />
          <Redirect from="*" to="/home" />
        </Switch>
      </BrowserRouter>
      <Footer
        id="Footer"
      />
    </div>
  );
}

export default withCookies(App);
