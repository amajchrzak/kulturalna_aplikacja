const CancelEvent = (cancelEvent, eventId) => {
    return (
        <button onClick={e => cancelEvent(e, eventId)} className='btn btn-danger btn-lg btn-block'>Zrezygnuj z wydarzenia</button>
    )
}

export default CancelEvent;