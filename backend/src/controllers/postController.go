package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/helpers"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/services"
)

func AddPost(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		authorID := int(helpers.GetLoggedUserID(r))

		post := services.PreparePost(formattedBody, authorID)
		status := model.InsertPost(post)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}

}

func GetPosts(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		posts, status := model.GetPosts()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"posts":   posts,
			"message": interfaces.MESSAGE,
		})
	}
}

func GetPost(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		requestedID := mux.Vars(r)
		postID, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		post, status := model.GetPost(postID)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"post":    post,
			"message": interfaces.MESSAGE,
		})
	}
}

func UpdatePost(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody map[string]string
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		requestedID := mux.Vars(r)
		postID, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		authorID := int(helpers.GetLoggedUserID(r))

		post := services.PreparePost(formattedBody, authorID)
		status := model.UpdatePost(post, postID)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}

}

func DeletePost(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		requestedID := mux.Vars(r)
		postID, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status := model.DeletePost(postID)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}
