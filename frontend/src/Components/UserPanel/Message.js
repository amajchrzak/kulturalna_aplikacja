import React from 'react';

const Message = ({ handleGetMessagesArray }) => {
    return (
        handleGetMessagesArray().map(({ text, userName, sender }, id) => (
            <div
                key={id}
                className={`message ${localStorage.getItem('user_id') === sender ? 'message--right' : 'message--left'}`}
            >
                <h5>{userName}</h5>
                <p>{text}</p>
            </div>
        ))
    )
}

export default Message;