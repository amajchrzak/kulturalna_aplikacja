package model

import (
	"fmt"
	"net/http"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func InsertPost(post interfaces.Post) int {
	query := `INSERT INTO posts(title, description, author_id, created_at) 
				VALUES($1, $2, $3, $4)`
	_, err := DB.Exec(query, post.Title, post.Description, post.AuthorID, post.CreatedAt)

	if err != nil {
		fmt.Println(err)
		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetPosts() ([]interfaces.PostDetailed, int) {
	var posts []interfaces.PostDetailed
	authors, status := GetAuthors()
	if status != http.StatusOK {

		return posts, http.StatusBadRequest
	}

	formattedAuthors := make(map[int]string)
	for _, value := range authors {
		formattedAuthors[int(value.ID)] = value.Name + " " + value.Surname
	}

	query := `SELECT id, title, description, author_id
		FROM posts
		ORDER BY created_at DESC`
	rows, err := DB.Query(query)

	if err != nil {

		return nil, http.StatusBadRequest
	}
	defer rows.Close()

	var post interfaces.PostDetailed
	for rows.Next() {
		if err := rows.Scan(&post.ID, &post.Title, &post.Description, &post.AuthorID); err != nil {

			return posts, http.StatusBadRequest
		}
		post.Author = formattedAuthors[post.AuthorID]

		posts = append(posts, post)
	}
	if err = rows.Err(); err != nil {

		return posts, http.StatusBadRequest
	}

	return posts, http.StatusOK
}

func GetPost(id int) ([]interfaces.PostDetailed, int) {
	posts, status := GetPosts()
	if status != http.StatusOK {

		return posts, http.StatusBadRequest
	}

	var post []interfaces.PostDetailed
	for _, value := range posts {
		if value.ID == id {
			post = append(post, value)

			return post, http.StatusOK
		}
	}

	return post, http.StatusBadRequest
}

func UpdatePost(post interfaces.Post, id int) int {
	query := `UPDATE posts SET title = $1, description = $2, author_id = $3
				WHERE id = $4`
	_, err := DB.Exec(query, post.Title, post.Description, post.AuthorID, id)

	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func DeletePost(id int) int {
	query := `DELETE FROM posts WHERE id = $1`
	_, err := DB.Exec(query, id)

	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}
