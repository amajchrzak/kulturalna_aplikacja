import firebase from 'firebase'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyAURgfaGbplap6M7CbMKJbt74TT7LdmeKw",
    authDomain: "kachat-12429.firebaseapp.com",
    projectId: "kachat-12429",
    storageBucket: "kachat-12429.appspot.com",
    messagingSenderId: "842223024435",
    appId: "1:842223024435:web:90a4d9a8b0269d56f16646",
    measurementId: "G-LFV0ZG7W5F"
})

const db = firebaseApp.firestore()

//left for later - possibillity of adding authorization via google, fb etc.
// const auth = firebase.auth()

export { db }