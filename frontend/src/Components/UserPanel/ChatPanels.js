import { useState } from 'react';
import Chat from './Chat';
import '../../Styles/ChatPanel.css';

const ChatPanel = () => {
    const [receiverName, setReceiverName] = useState("");
    return (
        <div>
            {receiverName ?
                <h1 className='chatPanel'>{receiverName}</h1>
                :
                <h1 className='chatPanel'>Wybierz rozmówcę</h1>
            }
            <Chat setReceiverName={setReceiverName} />
        </div>
    )
}

export default ChatPanel;