import { useEffect, useMemo, useState } from 'react';
import { useHistory } from "react-router-dom";
import { useCookies } from 'react-cookie';
import axios from "axios";
import '../../Styles/UserPanel.css';
import { nameHost } from '../../GlobalVariables/GlobalVariables';
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../Notification/Notification';
import Table from '../AdminPanel/UserManagement/Table';
import PrepareData from './UserEvents/PrepareData';
import ApproveModal from '../AdminPanel/UserManagement/Modals/ApproveModal';
import ModifyUserData from './UserEvents/Modals/ModifyUserData';
import { changeStyle } from '../../AuxilliaryFunctions/changeStyle';

const UserPanel = () => {

    const [events, setEvents] = useState([]);
    const [modalAssignCancelEventOpen, setModalAssignCancelEventOpen] = useState(false);
    const [modalModifyUserDataOpen, setModalModifyUserDataOpen] = useState(false);
    const [modalDeleteAccountOpen, setModalDeleteAccountOpen] = useState(false);
    const [eventId, setEventId] = useState("");
    const [refresh, setRefresh] = useState(false);
    const [dataUser, setDataUser] = useState({
        name: "",
        surname: ""
    })
    const [style, setStyle] = useState('default');
    const [cookies, setCookie] = useCookies(['style']);

    const history = useHistory();

    const handleGoEvent = (e, dateStartEvent, selectedEventId) => {
        e.preventDefault();
        console.log("eventId", eventId)
        const StartDay = new Date(Date.UTC(dateStartEvent.substring(0, 4), dateStartEvent.substring(5, 7) - 1, dateStartEvent.substring(8, 10), dateStartEvent.substring(11, 13), dateStartEvent.substring(14, 16))).toISOString();
        const EndDay = new Date(Date.UTC(dateStartEvent.substring(0, 4), dateStartEvent.substring(5, 7) - 1, dateStartEvent.substring(8, 10), dateStartEvent.substring(11, 13), dateStartEvent.substring(14, 16))).toISOString();
        const cookieStyle = cookies.style;
        const state = { StartDay, EndDay, cookieStyle, selectedEventId }
        history.push('/event', [state])
    }

    const handleCancelEvent = (e) => {
        e.preventDefault();
        const token = localStorage.getItem("token");

        axios.delete(`${nameHost}/resign-from-participation/${eventId}`,
            {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    setRefresh(prevState => !prevState);
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'success',
                        title: 'Zrezygnowałeś z udziału w wydarzeniu',
                        message: 'Rezygnacja z udziału w wydarzeniu zakończona powodzeniem.'
                    });
                } else {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'danger',
                        title: 'Nie zrezygnowano z udziału w wydarzeniu',
                        message: 'Rezygnacja z udziału w wydarzeniu zakończona niepowodzeniem.'
                    });
                }
                setModalAssignCancelEventOpen(false);
            })
            .catch(err => {
                console.error("Error", err);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Nie zrezygnowano z udziału w wydarzeniu',
                    message: 'Rezygnacja z udziału w wydarzeniu zakończona niepowodzeniem.'
                });
                setModalAssignCancelEventOpen(false);
            })
    }

    const openModalCancelEventOpen = (e, eventId) => {
        e.preventDefault();
        setModalAssignCancelEventOpen(true);
        setEventId(eventId);
    }

    const handleModifyUserData = e => {
        e.preventDefault();

        const token = localStorage.getItem("token");
        axios.patch(`${nameHost}/update-profile-data`,
            {
                name: dataUser.name,
                surname: dataUser.surname
            },
            {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    setRefresh(prevState => !prevState);
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'success',
                        title: 'Zaktualizowano dane',
                        message: 'Twoje dane zostały poprawnie zaktualizowane.'
                    });
                } else {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'danger',
                        title: 'Nie zaktualizowano danych',
                        message: 'Twoje dane nie zostały zaktualizowane.'
                    });
                }
                setModalAssignCancelEventOpen(false);
            })
            .catch(err => {
                console.error("Error", err);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Nie zaktualizowano danych',
                    message: 'Twoje dane nie zostały zaktualizowane.'
                });
                setModalAssignCancelEventOpen(false);
            })
    }

    const handleChangeDataUser = e => {
        e.preventDefault();
        setDataUser(prevState => ({
            ...prevState,
            [e.target.name]: e.target.value
        }))
    }

    const handleDeleteAccount = e => {
        e.preventDefault();
        console.info("###DELETE ACCOUNT");
        const token = localStorage.getItem("token");
        axios.delete(`${nameHost}/delete-own-account`, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'success',
                        title: 'Konto usunięto poprawnie',
                        message: 'Twoje konto zostało usunięte poprawnie.'
                    });
                    history.push('/home');

                    localStorage.removeItem("token")
                    localStorage.removeItem("user");
                    localStorage.removeItem("user_id");
                } else {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'danger',
                        title: 'Nie usunięto konta',
                        message: 'Usuwanie konta zakończone niepowodzeniem.'
                    });
                }
            })
            .catch(err => {
                console.error("Error", err);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Nie usunięto konta',
                    message: 'Usuwanie konta zakończone niepowodzeniem.'
                });
            })
    }

    const columns = useMemo(
        () => [
            {
                Header: 'Dane o wydarzeniu',
                columns: [
                    {
                        Header: 'Nazwa',
                        accessor: 'name',
                    },
                    {
                        Header: 'Data rozpoczęcia',
                        accessor: 'startDate',
                    },
                    {
                        Header: 'Data zakończenia',
                        accessor: 'endDate',
                    }
                ],
            },
            {
                Header: 'Akcje',
                columns: [
                    {
                        Header: 'Przejdź do wydarzenia',
                        accessor: 'goToEvent'
                    },
                    {
                        Header: 'Zrezygnuj',
                        accessor: 'cancelEvent',
                    }
                ],
            },
        ],
        []
    )

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const data = useMemo(() => PrepareData(events, handleGoEvent, openModalCancelEventOpen, events.length), [events, refresh])

    const handleChangeStyle = () => {
        const currentDate = new Date();
        const nextYear = new Date();
        nextYear.setFullYear(currentDate.getFullYear() + 1);
        setCookie('style', style, { path: '/', expires: nextYear });
    }

    const handleGoChat = e => {
        e.preventDefault();
        history.push('/chat')
    }

    const handleDataUser = () => {
        return (
            <div className={`userInfo ${changeStyle(cookies?.style, 'userInfo')}`}>
                <div>
                    <h1>Twoje dane</h1>
                    <p className='userInfo__p'>Imię: {dataUser.name}</p>
                    <p className='userInfo__p'>Nazwisko: {dataUser.surname}</p>
                </div>
                <div className='userInfo__management'>
                    <div className='userInfoBtn__select' >
                        <label htmlFor='select-style' className='userInfoBtn__label'>
                            Wybierz rodzaj ostylowania
                        </label>
                        <select id='select-style' className='userInfoBtn__form-select form-select'
                            value={style}
                            onChange={e => setStyle(e.target.value)}
                        >
                            <option value='default'>Default (niebieski)</option>
                            <option value='style-1'>Style-1 (zielony)</option>
                            <option value='style-2'>Style-2 (czerwony)</option>
                        </select>
                        <button className='uerInfo__single-btn btn btn-primary' onClick={() => handleChangeStyle(true)}>Zmień</button>

                    </div>
                    <div className="userInfo__btn">
                        <button className='uerInfo__single-btn btn btn-danger' onClick={() => setModalDeleteAccountOpen(true)}>Usuń konto</button>
                        <button className='uerInfo__single-btn  btn-primary' onClick={() => setModalModifyUserDataOpen(true)}>Modyfikuj dane</button>
                        <button className='uerInfo__single-btn btn btn-success' onClick={(e) => handleGoChat(e)}>CZAT</button>

                    </div>
                </div >
            </div >
        )
    }

    const hadleEvents = () => {
        return (
            <div className={`userEvents ${changeStyle(cookies?.style, 'userEvents')}`}>
                <h1>Wydarzenia na które się zapisałeś</h1>
                <div>
                    <Table columns={columns} data={data} />
                </div>
            </div>
        )
    }

    const handleGetUserAndEvents = () => {
        const token = localStorage.getItem("token");
        // const url = "https://mocki.io/v1/da03b447-e010-4f66-9085-1627f672c1cc";
        const url = `${nameHost}/get-profile-data`;
        axios.get(url, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                if (res.data.message === "SUCCESS") {
                    if (res.data.events !== null) {
                        setEvents(res.data.events);
                    } else {
                        setEvents([]);
                    }
                    if (res.data.profile_data !== null) {
                        const user = {
                            name: res.data.profile_data[0].Name,
                            surname: res.data.profile_data[0].Surname
                        }
                        setDataUser(user);
                    } else {
                        const user = {
                            name: "",
                            surname: ""
                        }
                        setDataUser(user);
                    }
                }
            })
            .catch(err => {
                console.error('Error', err);
                setEvents([]);
                const user = {
                    name: "",
                    surname: ""
                }
                setDataUser(user);
            });
    }

    useEffect(() => {
        handleGetUserAndEvents();

        return () => { //cleanup function
            setEvents([]);
            setDataUser([]);
        };
    }, [refresh]);

    return (
        <div className='userPanel'>
            {handleDataUser()}
            {
                events.length ?
                    hadleEvents()
                    :
                    null
            }
            {
                modalAssignCancelEventOpen && <ApproveModal
                    isOpen={modalAssignCancelEventOpen}
                    closeModal={setModalAssignCancelEventOpen}
                    contentLabel="Cancel event"
                    approve={handleCancelEvent}
                    title='Czy chcesz zrezygnować z tego wydarzenia?'
                    nameBtn='Zrezygnuj z wydarzenia'
                />
            }
            {
                modalModifyUserDataOpen && <ModifyUserData
                    isOpen={modalModifyUserDataOpen}
                    closeModal={setModalModifyUserDataOpen}
                    contentLabel="Modify user data"
                    dataUser={dataUser}
                    changeDataUser={handleChangeDataUser}
                    approve={handleModifyUserData}
                    title='Zmodyfikuj swoje dane'
                    nameBtn='Akceptuj'
                />
            }
            {
                modalDeleteAccountOpen && <ApproveModal
                    isOpen={modalDeleteAccountOpen}
                    closeModal={setModalDeleteAccountOpen}
                    contentLabel="Delete account"
                    approve={handleDeleteAccount}
                    title='Czy chcesz usunąć konto?'
                    nameBtn='Usuń konto'
                />
            }
        </div>
    )
}

export default UserPanel;