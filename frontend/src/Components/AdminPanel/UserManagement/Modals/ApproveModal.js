import Modal from 'react-modal';

const ApproveModal = props => {

    const customStyles = {
        content: {
            minWidth: '450px',
            minHeight: '225px',
            top: '50%',
            left: '50%',
            right: '75%',
            height: '20%',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            backgroundColor: "rgb(48, 52, 92)",
            color: "white"
        },
    };

    Modal.setAppElement('#root');

    return (
        <>
            <Modal
                isOpen={props.isOpen}
                style={customStyles}
            >
                <div className='window'>
                    <h2 className='titleAssignRole'>{props.title}</h2>
                    <div>
                        <form onSubmit={e => props.approve(e)}>
                            <div className="buttonsRemoveEvent">
                                <button onClick={() => props.closeModal(false)} className="btn btn-secondary btn-block singleButton">Zamknij</button>
                                <button type="submit" className="btn btn-success btn-block singleButton">{props.nameBtn}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default ApproveModal;