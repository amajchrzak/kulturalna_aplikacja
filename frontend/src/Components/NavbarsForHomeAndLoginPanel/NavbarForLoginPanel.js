import logo from "../../Photos/logo.png";
import { animateScroll as scroll } from "react-scroll";
import { Link as LinkReactRouterDom } from "react-router-dom";
import handleIsToken from '../../AuxilliaryFunctions/handleIsToken.js';
import { useHistory } from "react-router-dom";
import { changeStyle } from "../../AuxilliaryFunctions/changeStyle";
import { isAdmin } from "../../AuxilliaryFunctions/isAdmin";
import { isAdminOrReceptionist } from "../../AuxilliaryFunctions/isAdminOrReceptionist";

const NavbarForLoginPanel = ({ setUserData, cookieStyle }) => {

    const history = useHistory();

    const scrollToTop = () => {

        scroll.scrollToTop();
    };

    const handleLogout = () => {
        const clearDataUser = {
            name: "",
        };

        setUserData(clearDataUser);
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        localStorage.removeItem("user_id");
        localStorage.removeItem("role_id");
        history.push("/login-panel");
    }

    const setStyle = () => {
        return `nav-item ${changeStyle(cookieStyle, 'linkOtherSection')}`;
    }

    return (
        <nav className={`nav ${changeStyle(cookieStyle, 'nav')}`} id="navbar">
            <div className="nav-content">
                <img
                    src={logo}
                    className="nav-logo"
                    alt="Logo"
                    onClick={scrollToTop}
                />
                <ul className="nav-items">
                    <li className={setStyle()} >
                        <LinkReactRouterDom to="/home" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                            Home
                        </LinkReactRouterDom>
                    </li>
                    {handleIsToken() && isAdmin() &&
                        <li className={setStyle()} >
                            <LinkReactRouterDom to="/admin-panel" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                                Panel admina
                            </LinkReactRouterDom>
                        </li>
                    }
                    {handleIsToken() && isAdminOrReceptionist() &&
                        <li className={setStyle()} >
                            <LinkReactRouterDom to="/emails" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                                Email
                            </LinkReactRouterDom>
                        </li>
                    }
                    {handleIsToken() &&
                        <li className={setStyle()} >
                            <LinkReactRouterDom to="/calendar" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                                Kalendarz wydarzeń
                            </LinkReactRouterDom>
                        </li>
                    }
                    {handleIsToken() ?
                        null :
                        <li className={setStyle()} >
                            <LinkReactRouterDom to="/login-panel" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                                Zaloguj się
                            </LinkReactRouterDom>
                        </li>
                    }
                    {!handleIsToken() &&
                        <li className={setStyle()} >
                            <LinkReactRouterDom to="/password-reset" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                                Resetuj hasło
                            </LinkReactRouterDom>
                        </li>
                    }
                    {handleIsToken() ?
                        null :
                        <li className={setStyle()} >
                            <LinkReactRouterDom to="/registration-panel" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                                Rejestracja
                            </LinkReactRouterDom>
                        </li>
                    }
                    {handleIsToken() &&
                        <li className={setStyle()} >
                            <LinkReactRouterDom to="/password-change" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                                Zmień hasło
                            </LinkReactRouterDom>
                        </li>
                    }
                    {handleIsToken() &&
                        <li className={setStyle()} >
                            <LinkReactRouterDom to="/login-panel" className={`linkOtherSection ${changeStyle(cookieStyle, 'linkOtherSection')}`} >
                                Panel użytkownika
                            </LinkReactRouterDom>
                        </li>
                    }
                    {handleIsToken() &&
                        <li className={setStyle()} onClick={() => handleLogout()}>
                            Wyloguj się
                        </li>
                    }
                </ul>
            </div>
        </nav >
    );
}

export default NavbarForLoginPanel;