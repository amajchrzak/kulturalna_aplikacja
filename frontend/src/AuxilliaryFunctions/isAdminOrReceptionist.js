export const isAdminOrReceptionist = () => {
    const role = localStorage.getItem('role_id');

    if (String(role) === '2' || String(role) === '3') {
        return true;
    } else return false;
}