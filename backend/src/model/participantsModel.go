package model

import (
	"net/http"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func Resign(participant interfaces.Participant) int {
	_, err := DB.Exec("DELETE FROM participants WHERE event_id = $1 AND user_id = $2", participant.EventID, participant.UserID)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func EventCanceled(event_id int) int {
	_, err := DB.Exec("DELETE FROM participants WHERE event_id = $1", event_id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetEventsID(user_id int) ([]int, int) {
	rows, err := DB.Query("SELECT event_id FROM participants WHERE user_id = $1", user_id)
	if err != nil {

		return nil, http.StatusBadRequest
	}
	defer rows.Close()

	var events_ids []int
	var event_id int

	for rows.Next() {
		if err := rows.Scan(&event_id); err != nil {

			return events_ids, http.StatusBadRequest
		}
		events_ids = append(events_ids, event_id)
	}
	if err = rows.Err(); err != nil {

		return events_ids, http.StatusBadRequest
	}

	return events_ids, http.StatusOK
}

func ParticipantDeleted(participant_id int) int {
	_, err := DB.Exec("DELETE FROM participants WHERE user_id = $1", participant_id)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}
