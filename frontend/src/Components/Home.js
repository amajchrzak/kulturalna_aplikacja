import { Slide } from 'react-slideshow-image';
import '../Styles/Home.css'

import IncomingEvents from './IncomingEvents';
import About from "./About";
import HomePosts from './HomePosts';
import Contact from "./Contact";

import img1 from '../Photos/1.jpg';
import img2 from '../Photos/2.jpg';
import img3 from '../Photos/3.jpg';

const slideImages = [
    img1,
    img2,
    img3
]

const Home = () => {

    const text = (
        <span>
            <h1>
                Witaj w <br />
                Kulturalnej Aplikacji
            </h1>
            <hr />
            <h2>
                Odkryj kulturę w swojej okolicy
            </h2>
            <hr />
            <h3>
                Ciekawe wydarzenia <br />
                Inspirujący ludzie <br />
            </h3>
        </span>
    )

    return (
        <>
            <div className='slides'>
                <div>
                    <Slide
                        easing="cubic-in"

                        duration={3000}
                        transitionDuration={1000}
                    >
                        <div className="each-slide">
                            <div style={{ 'backgroundImage': `url(${slideImages[0]})` }}>
                                {text}
                            </div>
                        </div>
                        <div className="each-slide">
                            <div style={{ 'backgroundImage': `url(${slideImages[1]})` }}>
                                {text}
                            </div>
                        </div>
                        <div className="each-slide">
                            <div style={{ 'backgroundImage': `url(${slideImages[2]})` }}>
                                {text}
                            </div>
                        </div>
                    </Slide>

                </div>

            </div >
            {/* <IncomingEvents  id="IncomingEvents"/> */}

            <HomePosts
                id="IncomingEvents"
            />

            <About
                id="About"
            />
            <Contact
                id="Contact"
            />

        </>
    );
}

export default Home;
