package services

import (
	"net/http"
	"strconv"
	"time"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func ValidateOpinion(event_id, title string) (string, int) {
	_, err := strconv.Atoi(event_id)
	if err != nil {

		return "ID wydarzenia musi być liczbą", http.StatusBadRequest
	}

	if len(title) == 0 {

		return "Tytuł jest wymagany", http.StatusBadRequest
	}

	return interfaces.MESSAGE, http.StatusOK
}

func PrepareOpinion(event_id, title, description string, user_id uint64) interfaces.Opinion {
	var opinion interfaces.Opinion
	opinion.EventID, _ = strconv.Atoi(event_id)
	opinion.UserId = int(user_id)
	opinion.Title = title
	opinion.Description = description
	opinion.CreatedAt = time.Now()

	return opinion
}
