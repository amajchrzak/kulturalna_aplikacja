
import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../../Notification/Notification';
import '../../../Styles/PanelPost.css';

import axios from 'axios';
import { nameHost } from '../../../GlobalVariables/GlobalVariables';

const EditPost = (props) => {    
    
    const [changes,setChanges]= useState(false);  
    
    const[values, setValues] = useState({
        id: props.location.state.id,
        title: props.location.state.title,
        description: props.location.state.description,
    });
    
    const ClearPanelAddPost = () => {
        setValues({
            title: "",
            description: ""
        })       
    }



    const handleFormSubmit = (event) => {
            event.preventDefault();
            const token = localStorage.getItem("token");    
            console.log(values , props.location )
            if(changes){                
      
            //const url = "https://mocki.io/v1/8ce1b654-c93e-4f1a-9e9e-5147435391aa"; //Success
            const url = nameHost + "/update-post"
            axios.patch(`${url}/${values.id}`, {
                title: values.title,
                description: values.description
            },
            {
                headers: 
                {
                    Authorization: 'Bearer ' + token
                }
            }).then(res => {
                console.log(values)
                console.info("###Response from editpost endpoint:", res);
                if (res.status === 200) {
                    
                    console.info(res.data.message)
                    if (res.data.message === "SUCCESS") {
                        console.info("###Edit post ")
                        

                        setChanges(false);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'success',
                    title: 'Wpis został zapisany',
                    message: ' ',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })
                
                             

            }}}).catch(err => {
                console.error("Error from register endpoint", err);

                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Problem z zapisaniem wpisu',
                    message: 'Wpis nie został zapisany',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })});}
            else {

                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Wpis nie został zapisany',
                    message: 'Brak zmian w wpisie',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })
            }
    }


    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value,

        })

        setChanges(true);        
    }

    let history = useHistory();
    const PanelPost = () => {
        if(changes){
        store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Zmiany nie zostały zapisane',
            message: ' ',
            dismiss: {
                duration: 8000,
                onScreen: true
            }
        })}
            history.push({ 
                pathname: '/panel_post'
                
               })

        }
    
    return (
        <div className="window panelpost">          
        <div >
                    <h2 className="title titlepost">Formularz edycji wpisu</h2>
                    <div>
                        <form >
                            <div className="">
                                <div className="titleaddpost">
                                    <label htmlFor="title" className="h3 mb-3">Tytuł</label>
                                    <br/>
                                    <input name="title" type="text" 
                                    value={values.title}
                                    onChange={handleChange} 
                                     className="form-control" placeholder="Title" required
                                     
                                    />
                                </div>
                                <div className="descriptionaddpost">
                                    <label htmlFor="description" className="h3 mb-3">Opis</label>
                                    <textarea name ="description"  
                                    value={values.description}
                                    onChange={handleChange} 
                                     type="text-area" className="form-control descriptionPostInput" placeholder="Description" required
                                                                        
                                    />
                                </div>
                            </div>
                            <br />
       

                                <div>
                                
                                <div className="clearButton">
                                <div className="buttons">
                                    <button className="btn btn-dark btn-block singleButton" onClick={ PanelPost}>Powrót</button>  
                                </div>
                                    <button type="submit" className="btn btn-success btn-block singleButton " onClick={ClearPanelAddPost}>Wyczyść formularz</button>
                                    </div>
                                             
                                
                                <div className="buttons">
                                    <button type="submit" className="btn btn-success btn-block singleButton " onClick={handleFormSubmit}>Zapisz wpis</button>
                                </div>          

                                </div>                     
                            
                        </form>
                    </div>
                </div>
        </div>
    
    )
}

export default EditPost
