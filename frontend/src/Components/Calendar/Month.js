import { changeStyle } from "../../AuxilliaryFunctions/changeStyle";

const Month = ({ headerDays, handleDisplayDays, cookieStyle }) => {
    return (
        <table className={`calendarMonth ${changeStyle(cookieStyle, 'calendarMonth')}`}>
            <thead>
                <tr>
                    {headerDays.map(day => {
                        return (

                            <th key={day} className={`calendarNamesDays ${changeStyle(cookieStyle, 'calendarNamesDays')}`}>
                                {day}
                            </th>
                        )
                    })}
                </tr>
            </thead>
            <tbody>{handleDisplayDays()}</tbody>
        </table>
    )
}

export default Month;