import '../../../Styles/Table.css';
import React, { useState, useMemo, useEffect } from 'react';
import axios from 'axios';
import { useTable } from 'react-table';
import Swal from 'sweetalert2';
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../../Notification/Notification';
import { Link as LinkReactRouterDom } from "react-router-dom";
import '../../../Styles/EditEvent.css';
import { useHistory } from "react-router-dom";
import { nameHost } from '../../../GlobalVariables/GlobalVariables';
import Pagination from './Pagination';


const columnsMock = [
  {
    header: " ID",
    accessor: "id", // accessor is the "key" in the data
  },
  {
    header: "Tytuł",
    accessor: "title",
  },
  {
    header: "Data rozpoczęcia",
    accessor: "start_of_event",
  },
  {
    header: "Data zakończenia",
    accessor: "end_of_event",
  },
  {
    header: "Opis",
    accessor: "description",
  },
  {
    header: "Edit",
    accessor: "edit",
  },
  {
    header: "Delete",
    accessor: "delete",
  },
];

const ManagementPanel = (props) => {

  let history = useHistory();



  //const UrlShowEvents ="https://mocki.io/v1/2a22938b-246a-46fe-90ac-14bfb5442174";
  //  const UrlShowEvents ="https://mocki.io/v1/c5f2f1f1-9112-4321-ba0b-b3abd5cd6615";
  //  const UrlShowEvents =" https://mocki.io/v1/874cace2-d7b8-40a3-adc7-13e1f8f54425";

  const [notes, setNotes] = useState([]);
  const [listEvents, setListEvents] = useState([]);
  const [listInstructors, setListInstructors] = useState([]);
  const [listPlaces, setListPlaces] = useState([]);
  const [limitParticipants, setLimitParticipants] = useState("");
  const [maxParticipants, setMaxParticipants] = useState("");
  const [refresh, setRefresh] = useState(true);


  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(4);


  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = listEvents.slice(indexOfFirstPost, indexOfLastPost);
  const paginate = pageNumber => setCurrentPage(pageNumber);

  const handleButtonReturn = () => {
    history.push({
      pathname: '/admin-panel'

    })
  }

  const handleGetPlaces = () => {
    console.log("###GET PLACES");

    const token = localStorage.getItem("token");

    //  const url = "https://mocki.io/v1/42714a61-d68a-4f1f-8965-b604b191dd5c";
    const url = `${nameHost}/get-places`;
    axios.get(url, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => {
        console.log("PLACES:", res);
        setListPlaces(res.data.places);
        console.log(res.data.places);
      })
      .catch(err => {
        console.error("Error:", err);
      })
  }

  const handleLimitParticipants = (value, maxValue) => {
    if (maxValue) {
      if (value > maxValue) {
        setLimitParticipants(maxValue);
      } else if (value < 0) {
        setLimitParticipants(0);
      } else {
        setLimitParticipants(value);
      }
    } else {
      if (value < 0) {
        setLimitParticipants(0);
      } else {
        setLimitParticipants(value);
      }
    }
  }


  useEffect(() => {
    //  fetchTableData(UrlShowEvents);
    handleEvents();
    handleGetPlaces();
    getInstructors();

  }, [refresh]);

  const handleEvents = () => {
    const token = localStorage.getItem("token");
    axios.get(`${nameHost}/get-events`, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    }).then(res => {
      console.log("Response data about events", res)

      if (Array.isArray(res.data.events) && res.data.events.length) {
        setListEvents(res.data.events);
      } else {
        setListEvents([]);
      }
      // setListEvents(res.data);

    })
      .catch(err => {
        console.error("Error", err);
      })
  }


  /*  const fetchTableData = async (URL) => {
      const response = await fetch(URL);
      const data = await response.json(); 
      console.log("data")
      console.log(data)
      setNotes(data);
    };*/
  const UrlSuccessDeleteEvents = "https://mocki.io/v1/7e68317e-f881-4701-986d-e49c38080c0b";

  const deleteEvents = (id) => {


    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Na pewno usunąć to wydarzenie?',
      text: "Cofnięcie tej akcji będzie niemożliwe",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Tak, usuń wydarzenie',
      cancelButtonText: 'Anuluj',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {

        const token = localStorage.getItem("token");

        axios.delete(`${nameHost}/remove-event/${id}`, {
          headers: {
            Authorization: 'Bearer ' + token
          }
        }).then(res => {

          console.info("###Response from ShowEventsDelete endpoint:", res);

          if (res.status === 200) {

            if (res.data.message === "sucess") {



              store.addNotification({
                ...notificationTemplate,
                type: 'success',
                title: 'Sukces!',
                message: 'Pomyślnie usunięto wydarzenie',
                dismiss: {
                  duration: 8000,
                  onScreen: true
                }
              })
            }
          }


        }).catch(err => {
          console.error("Error from ShowEvent endpoint", err);

          store.addNotification({
            ...notificationTemplate,
            type: 'danger',
            title: 'Error',
            message: 'Błąd z usunięciem wydarzenia',
            dismiss: {
              duration: 8000,
              onScreen: true
            }
          })
        });


        swalWithBootstrapButtons.fire(
          'Sukces!',
          'Wydarzenie zostało usunięte',
          'success'
        )
      } else if (

        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Anulowano',
          'Wydarzenie nie zostało usunięte',
          'error'
        )
      }
    })

  }

  const getInstructors = () => {
    const token = localStorage.getItem("token");

    const url = `${nameHost}/get-instructors`
    axios.get(url, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(res => {
        setListInstructors(res.data.instructors);

      })
      .catch(err => {
        console.error("Error", err);
      })
  }




  const editEvents = (id) => {

    const foundEvent = listEvents.find(singleEvent => singleEvent.ID === id);
    const startEvent = foundEvent.StartOfEvent;
    const startDateEvent = startEvent.substring(0, 10);
    const startHourEvent = startEvent.substring(11, 16);

    const endEvent = foundEvent.EndOfEvent;
    const endDateEvent = endEvent.substring(0, 10);
    const endHourEvent = endEvent.substring(11, 16);

    history.push({
      pathname: '/edit_events',
      state: {
        id: id,
        title: foundEvent.Title,
        description: foundEvent.Description,
        start_of_event: startDateEvent,
        end_of_event: endDateEvent,
        price: foundEvent.Price,
        instructor: foundEvent.Instructor,
        instructorID: foundEvent.InstructorID,
        limit_of_participants: foundEvent.AmountOfParticipants,
        place: foundEvent.PlaceName,
        place_id: foundEvent.PlaceID,
        place_limit_of_participants: foundEvent.PlaceLimitOfParticipants,
        startHourEvent: startHourEvent,
        endHourEvent: endHourEvent,
        listInstructorsAll: listInstructors,
        listPlace: listPlaces
      }
    })
  }

  return (
    <div className='panel_post'>
      <div className='titleevents'>
        Panel Wydarzeń
      </div>
      <div className='tablepost'>
        <table>

          <thead>
            <tr>
              {columnsMock.map(({ header, accessor }) => (
                <th key={accessor}>{header}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {listEvents.map(
              ({ ID, Title, StartOfEvent, EndOfEvent, Description }) => (
                <tr key={ID}>
                  <td>{ID}</td>
                  <td className="titlepostpanel">{Title}</td>
                  <td>{StartOfEvent.substring(0, 10)}</td>
                  <td>{EndOfEvent.substring(0, 10)}</td>
                  <td className="descriptionPostInput">{Description.substring(0, 700)}</td>
                  <td><button onClick={() => editEvents(ID)} >Edytuj</button></td>
                  <td><button onClick={() => deleteEvents(ID)}>Usuń</button></td>

                </tr>
              )
            )}

          </tbody>
        </table>

      </div>
      {/* <div id="paginactionposts"><Pagination
        postsPerPage={postsPerPage}
        totalPosts={listEvents.length}
        paginate={paginate}
      />Strona: {currentPage}</div> */}
      <button className="btn btn-success btn-block singleButton buttonreturn" onClick={handleButtonReturn}>Powrót</button>
    </div>
  );
};

export default ManagementPanel;
