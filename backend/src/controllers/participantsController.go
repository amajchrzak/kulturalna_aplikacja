package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/helpers"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
)

func ResignFromParticipation(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		_, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		requestedID := mux.Vars(r)
		eventID, err := strconv.Atoi(requestedID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		userID := helpers.GetLoggedUserID(r)

		participant := interfaces.Participant{EventID: eventID, UserID: int(userID)}

		status := model.Resign(participant)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status = model.DecreaseAmountOfParticipants(participant.EventID)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": interfaces.MESSAGE,
		})
	}
}
