package model

import (
	"database/sql"
)

var DB *sql.DB

func ConnectDB() {
	var err error
	DB, err = sql.Open("postgres", "postgres://postgres:mysecretpassword1.@localhost/ka?sslmode=disable")
	if err != nil {
		panic(err)
	}

	err = DB.Ping()
	if err != nil {
		panic(err)
	}
}

func CloseDB() {
	defer func() {
		if err := DB.Close(); err != nil {
			panic(err)
		}
	}()
}
