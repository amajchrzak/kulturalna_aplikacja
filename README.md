# Kulturalna_Aplikacja

Project implemented on OA 2021 

# Instalation

git clone https://gitlab.com/amajchrzak/kulturalna_aplikacja.git

## Backend

install golang version ^1.17
install postgres version ^12.7
install pgAdmin4

backup db:
- go into pgAdmin4
- file <dbschema.sql> includes db schema and few records
- create db, name of db is not crucial, restore dbschema.sql

cd backend
go mod tidy
go run main.go

server starts on localhost - port :8080

---

## Frontend

install Node.js version ^12.18.2

cd frontend
npm install
npm start

server starts on localhost - port :3000
