import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import '../Styles/HomePostStyle.css';
import '../App.css';
import '../Styles/IncomingEvents.css';
import DisplayPost from "./AdminPanel/AddPostPanel/DisplayPost";
import { nameHost } from '../GlobalVariables/GlobalVariables';


const HomePosts = ({ id }) => {

  const [listPosts, setListPosts] = useState([]);
  const [boolDisplaypost, setboolDisplayPost] = useState(false);
  const [readMorePost, setreadMorePost] = useState({
    IDPost: "",
    TitlePost: "",
    DescriptionPost: ""
  });


  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(4);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = listPosts.slice(indexOfFirstPost, indexOfLastPost);

  useEffect(() => {
    handlePosts();
  }, []);



  const token = localStorage.getItem("token");
  const ShowPostsHome = nameHost + "/get-posts";
  //const ShowPostsHome ="https://mocki.io/v1/d01f2e5a-496a-4c79-bff6-d0623ea0166f";
  async function asyncFunc() {
    // fetch data from a url endpoint
    const response = await axios.get(ShowPostsHome);
    const data = await response.data.posts;

    setListPosts(data);

  }

  const handlePosts = () => {
    asyncFunc();

  }
  const Test = (boolpost) => {

    setboolDisplayPost(
      boolpost
    )

  }

  const handleClick = () => {
    Test(false);
    console.log(boolDisplaypost)
  }

  const handleClickPost = (id, Title, Description) => {
    Test(true);

    setreadMorePost({
      IDPost: id,
      TitlePost: Title,
      DescriptionPost: Description
    })

  }

  return (
    <div className="incomingEvent" id={id}>

      {boolDisplaypost ?
        <div className="divpost">
          <h3 className="containerposts titlereadmore">{readMorePost.TitlePost}</h3>
          <h5 className="containerposts desreadmore">{readMorePost.DescriptionPost}</h5>
          <button className="buttonreadmore btn btn-lg btn-info" onClick={() => handleClick()}>Wpisy</button>
        </div> :
        <div>
          <h2 className="tiitelhomeposts "> Ostatnio dodane wpisy </h2>
          <div className=" col_grid">
            {currentPosts.map(
              ({ ID, Title, Description }) => (
                // <div className="marg">
                // <div className="containerposts"> 
                //   <h3 className="containerposts">{Title}</h3>
                //   <h5 className="containerposts">{Description.substring(0, 700)}...</h5>              
                //   <button className="buttonreadmore btn btn-lg btn-info" onClick={() => handleClickPost(ID, Title, Description)}>Czytaj dalej</button>
                //   </div>
                <div class="card w-75 marg">
                  <div class="card-body card_">
                    <h3 class="card-title containerposts">{Title}</h3>
                    <h5 class="card-text containerposts">{Description.substring(0, 200)}...</h5>
                    <button className="buttonreadmore btn btn-lg btn-info" onClick={() => handleClickPost(ID, Title, Description)}>Czytaj dalej</button>
                  </div>
                </div>

                //</div>



              )
            )}
            <div className="space"></div>



          </div> </div>}

    </div>

  );
}

export default HomePosts;
