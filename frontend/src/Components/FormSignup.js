import React, { useState, useEffect, setState } from 'react'
import validation from './validation';
import { Link as LinkReactRouterDom } from "react-router-dom";
import { store } from 'react-notifications-component';
import { notificationTemplate } from './Notification/Notification';
import logo from "../Photos/logo.png";
import axios from 'axios';
import '../Styles/Signupstyle.css';
import { useHistory } from "react-router-dom";
import { nameHost } from '../GlobalVariables/GlobalVariables';

const FormSignup = ({ }) => {

    const history = useHistory();

    const [values, setValues] = useState({
        username: "",
        email: "",
        passwordRegister: "",
        passwordRegisterConfirm: ""
    });

    const [errors, setErrors] = useState({});

    const [dataIsCorrect, setDataIsCorrect] = useState(false);

    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value,

        })
    }


    const handleFormSubmit = (event) => {
        event.preventDefault();
        setErrors(validation(values));
        setDataIsCorrect(true);



        if (Object.keys(errors).length === 0) {
            // const addressUrlRegister = "https://mocki.io/v1/8ce1b654-c93e-4f1a-9e9e-5147435391aa" // Success
            // const addressUrlRegister =  "https://mocki.io/v1/bc5c257d-00a7-4048-8584-80e1e0ba37e7" // bad request
            //   const addressUrlRegister = "https://mocki.io/v1/5d3f0ded-db50-4073-a371-dd3ae2b6da4d" // Login jest już zajęty
            //   const addressUrlRegister = "https://mocki.io/v1/f35f03ca-2bbd-4fa1-a9ca-333c6386fa66" // Email jest już zajęty
            const url = nameHost + "/register"
            axios.post(url, {
                username: values.username,
                email: values.email,
                password: values.passwordRegister,
                retypedPassword: values.passwordRegisterConfirm
            }).then(res => {
                console.info("###Response from register endpoint:", res);
                if (res.status === 200 /* && dataIsCorrect*/) {
                    console.info("jakis log")
                    console.info(res.data.message)
                    if (res.data.message === "SUCCESS") {
                        console.info("###Create an account ")
                        //submitForm(true);

                        store.addNotification({
                            ...notificationTemplate,
                            type: 'success',
                            title: 'Konto zostało stworzone',
                            message: 'Możesz teraz zalogować się',
                            dismiss: {
                                duration: 8000,
                                onScreen: true
                            }
                        })

                        history.push("/login-panel");

                    } else if (res.data.message === "Email jest już zajęty") {
                        // console.info("###Create an account ")
                        store.addNotification({
                            ...notificationTemplate,
                            type: 'danger',
                            title: 'Konto nie zostało stworzone',
                            message: 'Email jest już zajęty',
                            dismiss: {
                                duration: 8000,
                                onScreen: true
                            }
                        })
                    }

                    else if (res.data.message === "Login jest już zajęty") {
                        store.addNotification({
                            ...notificationTemplate,
                            type: 'danger',
                            title: 'Konto nie zostało stworzone',
                            message: 'Login jest już zajęty',
                            dismiss: {
                                duration: 8000,
                                onScreen: true
                            }
                        })
                    }
                    else {
                        store.addNotification({
                            ...notificationTemplate,
                            type: 'danger',
                            title: 'Konto nie zostało stworzone',
                            message: ' Jest stworzone konto już z tym loginem i emailem',
                            dismiss: {
                                duration: 8000,
                                onScreen: true
                            }
                        })



                    }
                }



            }).catch(err => {
                console.error("Error from register endpoint", err);

                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Problem z rejestracja',
                    message: 'Nie udało się zarejestrować.',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })


            });
        };

        handleClearFormSignUp();

    }


    //useEffect(()=> {})

    const handleClearFormSignUp = (event) => {
        console.info("###CLEAR FORM SignUp PROCESS");
        setValues({
            username: "",
            email: "",
            passwordRegister: "",
            passwordRegisterConfirm: ""

        })
    }

    return (
        <div className="LoginPanelForm">
            <form className="form" onSubmit={handleClearFormSignUp} >
                <h1>Rejestracja</h1>
                <img className="mb-4" src={logo} alt="" width="72" height="72" />
                <h1 className="h3 mb-3 font-weight-normal">Podaj dane</h1>
                <div className="form-inputs clear">
                    <div id="register_username">
                        <label htmlFor="username" className="h3 mb-2">Login
                        </label><input id="username" type="text" name="username" className=" mb-3 form-control" placeholder="Login" required autoFocus
                            value={values.username}
                            onChange={handleChange} /> </div>
                    {errors.username && <h5 className="errors_show">{errors.username}</h5>}
                </div>

                <div className="form-inputs clear">
                    <div id="register_email">
                        <label htmlFor="email" className="h3 mb-2">Email
                        </label>  <input id="email" type="email" name="email" className=" mb-3 form-control" placeholder="Email" required
                            value={values.email}
                            onChange={handleChange} /></div>
                    {errors.email && <h5 className="errors_show">{errors.email}</h5>}
                </div>

                <div className="form-inputs clear">
                    <div id="register_passwordRegister">
                        <label htmlFor="passwordRegister" className="h3 mb-2">Hasło
                        </label>   <input id="passwordRegister" type="password" name="passwordRegister" className=" mb-3 form-control" placeholder="Hasło" required
                            value={values.passwordRegister}
                            onChange={handleChange} /> </div>
                    {errors.passwordRegister && <h5 className="errors_show">{errors.passwordRegister}</h5>}
                </div>

                <div className="form-inputs clear">
                    <div id="register_passwordRegisterConfirm">
                        <label htmlFor="passwordRegisterConfirm" className="h3 mb-2">Powtórz hasło
                        </label>  <input type="password" name="passwordRegisterConfirm" className="form-control mb-3" placeholder="Powtórz Hasło" required
                            value={values.passwordRegisterConfirm}
                            onChange={handleChange} /></div>
                    {errors.passwordRegisterConfirm && <h5 className="errors_show" >{errors.passwordRegisterConfirm}</h5>}
                </div>
                <div id="button_register">
                    <button className="buttonRegister btn btn-lg btn-primary btn-block" onClick={handleFormSubmit} type="sumbit">Zarejestruj się</button>

                    <div>
                        Masz już konto? Zaloguj się <LinkReactRouterDom to="/login-panel" className="linkOtherSection" >
                            tutaj
                        </LinkReactRouterDom>.
                    </div>
                    <p className="mt-5 mb-3 text-muted">&copy; 2021</p>
                </div>

            </form>

        </div>

    )
}

export default FormSignup;
