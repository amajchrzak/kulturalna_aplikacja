import { isAdminOrReceptionist } from "../../AuxilliaryFunctions/isAdminOrReceptionist";
import { changeStyle } from "../../AuxilliaryFunctions/changeStyle";

export const ListEvents = ({ listEvents, event, setEvent, handleRemoveEvent, setRemoveEventFromList, cookieStyle }) => {
    const displayListEvents = [];

    listEvents.forEach((singleEvent, index) => {
        if (singleEvent.ID !== event.ID) {
            displayListEvents.push(
                <p key={index} className={`eventOnList eventsList ${changeStyle(cookieStyle, 'eventOnList')}`}>
                    <span className='h2Event'>{singleEvent.Title}</span>
                    <span className="buttonOnEventList">
                        <button className="btn btn-primary" onClick={() => setEvent(singleEvent)}>Wyświetl</button>
                    </span>
                    {isAdminOrReceptionist() &&
                        <span className="buttonOnEventList">
                            <button className="btn btn-danger"
                                onClick={
                                    () => {
                                        handleRemoveEvent();
                                        setRemoveEventFromList(singleEvent.ID);
                                    }}>Usuń wydarzenie</button>
                        </span>
                    }

                </p>
            )
        }
    });

    return displayListEvents;
}