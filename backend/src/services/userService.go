package services

import (
	"net/http"
	"net/mail"
	"regexp"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/model"
	"golang.org/x/crypto/bcrypt"
)

func PrepareRegisterOperation(email, username, password string) interfaces.User {
	hash, _ := bcrypt.GenerateFromPassword([]byte(password), 13)
	user := interfaces.User{Email: email, Username: username, Password: hash}

	return user
}

func ValidCorrectnessOfRequest(formattedBody map[string]string) (int, string) {
	_, err := mail.ParseAddress(formattedBody["email"])
	if err != nil {
		interfaces.MESSAGE = "Email address is invalid!"

		return http.StatusBadRequest, interfaces.MESSAGE
	}

	status := ValidPassword(formattedBody["password"])
	if status != http.StatusOK {

		return http.StatusBadRequest, interfaces.MESSAGE
	}

	return http.StatusOK, interfaces.MESSAGE
}

func ValidPassword(password string) int {
	match, _ := regexp.MatchString("[a-zA-Z]+", password)
	if !match {
		interfaces.MESSAGE = "The password must contain at least one letter!"

		return http.StatusBadRequest
	}

	match, _ = regexp.MatchString("[0-9]+", password)
	if !match {
		interfaces.MESSAGE = "The password must contain at least one number!"

		return http.StatusBadRequest
	}

	match, _ = regexp.MatchString("[!@#$%^&*_+=/?<>:;]+", password)
	if !match {
		interfaces.MESSAGE = "The password must contain at least one special character!"

		return http.StatusBadRequest
	}

	status := checkPasswordLength(password)
	if status != http.StatusOK {
		interfaces.MESSAGE = "The password must contain more than 5 characters!"

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func checkPasswordLength(password string) int {
	if len(password) >= 5 && len(password) <= 30 {

		return http.StatusOK
	} else {

		return http.StatusBadRequest
	}

}

func CompareHashAndPassword(hashedPassword string, password string) int {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))

	if err == bcrypt.ErrMismatchedHashAndPassword && err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func ComparePasswords(password string, retypedPassword string) int {
	if password != retypedPassword {
		interfaces.MESSAGE = "Podane hasła się różnią."

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func HashNewPassword(password string) (string, int) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 13)
	if err != nil {

		return password, http.StatusBadRequest
	}

	return string(hash), http.StatusOK
}

func ResetPasswordForUserWhoLostPassword(resetPassword interfaces.ResetPasword) int {
	user, status := model.FindOneByUUID(resetPassword.SentToken)
	if status != http.StatusOK {

		return http.StatusBadRequest
	}

	status = ValidPassword(resetPassword.NewPassword)
	if status != http.StatusOK {

		return http.StatusBadRequest
	}

	hash, status := HashNewPassword(resetPassword.NewPassword)
	if status != http.StatusOK {

		return http.StatusBadRequest
	}

	status = model.ResetPasswordAndToken(hash, int(user.ID))
	if status != http.StatusOK {

		return http.StatusBadRequest
	}

	return http.StatusOK
}
