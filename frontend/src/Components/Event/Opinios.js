export const Opinions = ({ opinions, handleRemoveOpinion }) => {
    const listOpions = [];
    if (opinions.length) {
        opinions.forEach((opinion, index) => {
            listOpions.push(
                <div className='opinion' key={index}>
                    <h1>{opinion.Title}</h1>
                    <h2 className='opinion_h2'>{handleDisplayAuthor(opinion.OpinionAuthor)}</h2>
                    <span>
                        {opinion.Description}
                    </span>
                    {localStorage.getItem('user_id') === String(opinion.UserId) &&
                        <div className='buttons'>
                            <button className='btn btn-danger buttonOpinion'
                                onClick={(e) => handleRemoveOpinion(e, opinion.EventID, opinion.ID)}
                            >
                                Usuń
                            </button>
                            {/* TODO */}
                            {/* <button
                        disabled={!(localStorage.getItem('user_id') === String(opinion.UserId))}
                        className='btn btn-primary buttonOpinion'
                    >
                        Edytuj
                    </button> */}
                        </div>
                    }
                </div>
            )
        })
    }
    return listOpions;
}

const handleDisplayAuthor = author => {
    if (author) {
        return author;
    } else {
        return 'Autor anonimowy';
    }
}