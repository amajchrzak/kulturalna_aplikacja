import axios from 'axios';
import { useCallback, useEffect, useState } from 'react';
import moment from 'moment'
import 'moment/locale/pl';
import Day from './Day';
import PanelInfoEvent from './PanelInfoEvents';
import Month from './Month';
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../Notification/Notification';
import { nameHost } from '../../GlobalVariables/GlobalVariables';
import AddEventModal from './AddEventModal';
import RemoveEventModal from './RemoveEventModal';
import { changeStyle } from '../../AuxilliaryFunctions/changeStyle';

//helpers
import preapreObjEvent from '../../AuxilliaryFunctions/prepareObjEvent';
import handleHowManyDays from '../../AuxilliaryFunctions/handleHowManyDays';
import { isAdminOrReceptionist } from '../../AuxilliaryFunctions/isAdminOrReceptionist';
import AddPlaceModal from './AddPlaceModal';

const Calendar = ({ cookieStyle }) => {
    moment.locale('pl'); //set pl names
    const headerDays = ["PN", "WT", "ŚR", "CZW", "PT", "SOB", "NIEDZ"];

    const daysInMonthOnCalendar = 35;
    const [selectedDay, setSelectedDay] = useState(1);
    const [eventsMap, setEventsMap] = useState(new Map());
    const [date, setDate] = useState(moment());
    const [daysInMonth, setDaysInMonth] = useState(moment().daysInMonth());

    const [dataForm, setDataForm] = useState({
        title: "",
        description: "",
        price: "",
        startDate: "",
        inputStartTime: "",
        endDate: "",
        inputEndTime: ""
    });

    const [dataPlaceForm, setDataPlaceForm] = useState({
        name: "",
        limit: ""
    });

    const [limitParticipants, setLimitParticipants] = useState("");
    const [instructor, setInstructor] = useState("");
    const [place, setPlace] = useState("")

    const [listEvents, setListEvents] = useState([]);
    const [listInstructors, setListInstructors] = useState([]);
    const [listPlaces, setListPlaces] = useState([]);

    const [refresh, setRefresh] = useState(false); //to rerender after adding/removing an event

    const listEventsAfterPreparation = []

    const [idEventToRemove, setIdEventToRemove] = useState();

    ////////////////MODAL VIEW///////////////////////////
    const [modalIsOpen, setIsOpen] = useState(false);
    const [modalAddPlaceIsOpen, setAddPlaceIsOpen] = useState(false);
    const [modalRemoveIsOpen, setModalRemoveIsOpen] = useState(false);

    const openModal = () => {
        setIsOpen(true);
    }

    const closeModal = () => {
        setIsOpen(false);
    }

    const openModalAddPlace = () => {
        setAddPlaceIsOpen(true);
    }

    const closeModalAddPlace = () => {
        setAddPlaceIsOpen(false);
    }

    const openModalRemoveEvent = () => {
        setModalRemoveIsOpen(true);
    }

    const closeModalRemoveEvent = () => {
        setModalRemoveIsOpen(false);
    }
    /////////////////////////////////////////////////////
    //EVENY
    const handleInputData = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        setDataForm(prevState => {
            return {
                ...prevState,
                [name]: value
            }

        })
    }

    //PLACE
    const handleInputDataPlace = (event) => {
        const value = event.target.value;
        const name = event.target.name;

        setDataPlaceForm(prevState => {
            return {
                ...prevState,
                [name]: value
            }

        })
    }

    const handleLimitParticipants = (value, maxValue) => {
        if (maxValue) {
            if (value > maxValue) {
                setLimitParticipants(maxValue);
            } else if (value < 0) {
                setLimitParticipants(0);
            } else {
                setLimitParticipants(value);
            }
        } else {
            if (value < 0) {
                setLimitParticipants(0);
            } else {
                setLimitParticipants(value);
            }
        }
    }

    const handleSetPrice = value => {
        if (value < 0) {
            setDataForm(prevState => {
                return {
                    ...prevState,
                    price: 0,
                }
            })
        } else {
            setDataForm(prevState => {
                return {
                    ...prevState,
                    price: value,
                }
            });
        }
    }

    const clearForm = () => {
        for (const data in dataForm) {
            setDataForm(prevState => {
                return {
                    ...prevState,
                    [data]: ""
                }
            })
        };
        setLimitParticipants("");
        setInstructor("");
        setPlace("");
    }

    const clearPlaceForm = () => {
        for (const data in dataPlaceForm) {
            setDataPlaceForm(prevState => {
                return {
                    ...prevState,
                    [data]: ""
                }
            })
        };
    }

    const handleAddBlanksDay = (oneWeek, handleIndex, weeks) => {
        const events = [];
        while (oneWeek.length !== headerDays.length) {
            oneWeek.push(
                <td key={handleIndex} className={`calendarMonth ${changeStyle(cookieStyle, 'calendarMonth')}`}>
                    <Day key={handleIndex} number={""} setSelectedDay={setSelectedDay} events={events} cookieStyle={cookieStyle} blankDay={true} />
                </td>
            )
            handleIndex++;
        }
        weeks.push(
            <tr key={handleIndex}>
                {oneWeek}
            </tr>
        )
    }

    const handlePrepareListEvents = (element) => {
        const currentlyDisplayedMonth = date.month();
        const currentlyDisplayedYear = date.year();

        const dateStartEvent = moment(element.StartOfEvent.substring(0, 10));
        const dateEndEvent = moment(element.EndOfEvent.substring(0, 10));

        if (dateStartEvent.month() === currentlyDisplayedMonth && dateStartEvent.year() === currentlyDisplayedYear) {
            const prepareObjEvent = preapreObjEvent(element, dateStartEvent.date(), dateStartEvent.month(), dateStartEvent.year());
            listEventsAfterPreparation.push(prepareObjEvent); //add next event
            //if event lasts for several days
            let endDay;
            if (dateEndEvent.month() > dateStartEvent.month()) {
                endDay = date.daysInMonth(); //when the end day is next month
            } else {
                endDay = dateEndEvent.date();
            }
            if (dateStartEvent.date() !== dateEndEvent.date()) {
                for (let startDay = dateStartEvent.date() + 1; startDay <= endDay; startDay++) {
                    const prepareObjEvent = preapreObjEvent(element, startDay, dateStartEvent.month(), dateStartEvent.year());
                    listEventsAfterPreparation.push(prepareObjEvent); //add next event
                }
            }
            if (dateEndEvent.year() > currentlyDisplayedYear) { //if the end day event is in the next year
                for (let startDay = dateStartEvent.date() + 1; startDay <= date.daysInMonth(); startDay++) {
                    const prepareObjEvent = preapreObjEvent(element, startDay, dateStartEvent.month(), dateStartEvent.year());
                    listEventsAfterPreparation.push(prepareObjEvent); //add next event
                }
            }
        } else if (dateStartEvent.month() !== currentlyDisplayedMonth && dateStartEvent.year() === currentlyDisplayedYear) {//if event was started previous month
            if (dateStartEvent.month() < date.month() && dateEndEvent.month() === date.month()) {
                for (let startDay = 1; startDay <= dateEndEvent.date(); startDay++) {
                    const prepareObjEvent = preapreObjEvent(element, startDay, dateEndEvent.month(), dateEndEvent.year());
                    listEventsAfterPreparation.push(prepareObjEvent); //add next event
                }
            }
        } else if (dateStartEvent.year() < currentlyDisplayedYear && dateEndEvent.year() === currentlyDisplayedYear && dateEndEvent.month() === currentlyDisplayedMonth) { //if event was started previous year
            for (let startDay = 1; startDay <= dateEndEvent.date(); startDay++) {
                const prepareObjEvent = preapreObjEvent(element, startDay, dateEndEvent.month(), dateEndEvent.year());
                listEventsAfterPreparation.push(prepareObjEvent); //add next event
            }
        }
    }

    const handleDisplayDays = () => {
        const days = [];
        const firstDay = date.startOf("month").format("d");
        for (let i = 1; i < firstDay; i++) {
            days.push("");
        };
        const daysMonth = handleHowManyDays(daysInMonth);
        days.push(...daysMonth);

        while (days.length < daysInMonthOnCalendar) {
            days.push("");
        }

        let oneWeek = [];
        const weeks = [];

        let handleIndex = 1;
        // eslint-disable-next-line array-callback-return
        days.map((day, index) => {
            let events = eventsMap.get(day);
            if (typeof events === "undefined") {
                events = [];
            }
            oneWeek.push(
                <td key={index + 1} className={`calendarMonth ${changeStyle(cookieStyle, 'calendarMonth')}`}>
                    <Day key={index} number={day} setSelectedDay={setSelectedDay} events={events} cookieStyle={cookieStyle} />
                </td>
            )
            if ((index + 1) % headerDays.length === 0) {
                weeks.push(
                    <tr key={index}>
                        {oneWeek}
                    </tr>
                )
                oneWeek = [];
            }
            if ((index + 1) > daysInMonthOnCalendar) {
                handleIndex = index;
                oneWeek = [];
                oneWeek.push(
                    <td key={index + 1} className={`calendarMonth ${changeStyle(cookieStyle, 'calendarMonth')}`}>
                        <Day key={index + 1} number={day} setSelectedDay={setSelectedDay} events={events} cookieStyle={cookieStyle} />
                    </td>
                )
            }
        })
        handleIndex = +1;
        if (oneWeek.length) { //when required display more then 35 days
            handleAddBlanksDay(oneWeek, handleIndex, weeks);
        }
        return weeks;
    }

    const handeNameMonth = () => {
        return date.format("MMMM").toUpperCase();
    }

    const handeNameYear = () => {
        return date.format("YYYY").toUpperCase();
    }

    const handleGetEvents = () => {
        const token = localStorage.getItem("token");

        // const url = "https://mocki.io/v1/ce1e58c3-8a09-474b-b913-1e220714aa8a";
        // const urlNoEvents = "https://mocki.io/v1/b289fd14-cf4e-4271-a260-7cc5aa7a50ad";
        // const url = "https://mocki.io/v1/aad77880-1e63-4f88-9884-84015859b363";
        console.log("###RESPONSE EVENTS")
        axios.get(`${nameHost}/get-events`, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                console.log("Response data about events", res)
                if (Array.isArray(res.data.events) && res.data.events.length) {
                    setListEvents(res.data.events);
                } else {
                    setListEvents([]);
                }
            })
            .catch(err => {
                console.error("Error", err);
            })
    }

    const handleGetInstructors = () => {
        console.log("###GET INSTRUCTORS");
        const token = localStorage.getItem("token");

        //const url = "https://mocki.io/v1/c1407904-8253-46f2-bd70-221581fba866";
        const url = `${nameHost}/get-instructors`
        axios.get(url, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                console.log("INSTRUCTORS:", res);
                setListInstructors(res.data.instructors);
            })
            .catch(err => {
                console.error("Error:", err);
            })
    }

    const handleGetPlaces = () => {
        console.log("###GET PLACES");
        const token = localStorage.getItem("token");

        // const url = "https://mocki.io/v1/42714a61-d68a-4f1f-8965-b604b191dd5c";
        const url = `${nameHost}/get-places`;
        axios.get(url, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                console.log("PLACES:", res);
                setListPlaces(res.data.places);
            })
            .catch(err => {
                console.error("Error:", err);
            })
    }

    const handlePrevNextMonth = (nextOrPrev) => { //next/prev
        let currentNrMonth;
        if (nextOrPrev === "next") {
            currentNrMonth = date.month() + 1;
        } else if (nextOrPrev === "prev") {
            currentNrMonth = date.month() - 1;

        } else {
            currentNrMonth = date.month();
        }

        let prepareDateObj = Object.assign({}, date);
        prepareDateObj = moment(date).set("month", currentNrMonth);
        setDate(prepareDateObj);
        setDaysInMonth(prepareDateObj.daysInMonth())
    }

    const handleRemoveEventAfterAprove = e => {
        e.preventDefault();
        const token = localStorage.getItem("token");

        axios.delete(`${nameHost}/remove-event/${idEventToRemove}`, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                console.info("Response after remove event", res)
                setRefresh(!refresh) //calling rerender
                store.addNotification({
                    ...notificationTemplate,
                    type: 'success',
                    title: 'Wydarzenie zostało usunięte',
                    message: 'Wydarzenie zostało poprawnie usunięte z kalendarza.'
                });
                setIdEventToRemove("");
                closeModalRemoveEvent();
            })
            .catch(err => {
                console.error("Error", err);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Wydarzenie nie zostało usunięte',
                    message: 'Wystąpił problem podczas usuwania wydarzenia z kalendarza.'
                });
                setIdEventToRemove("");
                closeModalRemoveEvent();
            });
    }

    const handleRemoveEvent = (eventId) => {
        console.info("###REMOVE EVENT PROCESS", eventId)
        setIdEventToRemove(eventId);
        openModalRemoveEvent();
    }

    const handleEventsMap = useCallback(() => {
        listEventsAfterPreparation.length = 0;
        const eventsMap = new Map();

        listEvents.forEach(handlePrepareListEvents) //for the currently displayed month

        //set map -> key - nr day, value - events
        for (let day = 1; day <= date.daysInMonth(); day++) {
            const listEvents = [];
            for (let i in listEventsAfterPreparation) {
                if (listEventsAfterPreparation[i].nrDay === day) {
                    listEvents.push(listEventsAfterPreparation[i]);
                }
            }
            eventsMap.set(day, listEvents);

        }
        setEventsMap(eventsMap);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [date, listEvents]);

    const handleAddEvent = e => {
        e.preventDefault();
        console.log("###ADD EVENT")
        if (dataForm.startDate > dataForm.endDate) {
            store.addNotification({
                ...notificationTemplate,
                type: 'warning',
                title: 'Błędne dane',
                message: 'Data rozpoczecia nie może być późniejsza od daty zakończenia wydarzenia.'
            });
        } else if (dataForm.startDate === dataForm.endDate && dataForm.inputStartTime >= dataForm.inputEndTime) {
            store.addNotification({
                ...notificationTemplate,
                type: 'warning',
                title: 'Błędne dane',
                message: 'Godzina rozpoczecia nie może być później niż godzina zakończenia ani taka sama jeśli wydarzenie jest jednodniowe.'
            });
        } else {
            const startDateWithTime = new Date(Date.UTC(dataForm.startDate.substring(0, 4), dataForm.startDate.substring(5, 7) - 1, dataForm.startDate.substring(8, 10), dataForm.inputStartTime.substring(0, 2), dataForm.inputStartTime.substring(3, 5)));
            const endDateWithTime = new Date(Date.UTC(dataForm.endDate.substring(0, 4), dataForm.endDate.substring(5, 7) - 1, dataForm.endDate.substring(8, 10), dataForm.inputEndTime.substring(0, 2), dataForm.inputEndTime.substring(3, 5)));
            const token = localStorage.getItem("token");
            const config = {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
            axios.post(`${nameHost}/add-event`, {
                title: dataForm.title,
                description: dataForm.description,
                price: String(dataForm.price),
                limit_of_participants: String(limitParticipants),
                start_of_event: startDateWithTime.toISOString(),
                end_of_event: endDateWithTime.toISOString(),
                author_id: localStorage.getItem("user_id"),
                instructor_id: instructor,
                place_id: place
            },
                config
            ).then(res => {
                console.log("Response after add event", res)
                store.addNotification({
                    ...notificationTemplate,
                    type: 'success',
                    title: 'Dodano wydarzenie',
                    message: 'Wydarzenie zostało poprawnie dodane do kalendarza.'
                });
                clearForm();
                closeModal();
                setRefresh(!refresh) //calling rerender
            }).catch(err => {
                console.error("Error", err);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Nie dodano wydarzenia',
                    message: 'Wydarzenie nie zostało dodane do kalendarza.'
                });
            })
        }
    }

    const handleAddPlace = e => {
        e.preventDefault();
        console.log("###ADD Place")

        const token = localStorage.getItem("token");
        const config = {
            headers: {
                Authorization: 'Bearer ' + token
            }
        }
        axios.post(`${nameHost}/add-place`, {
            name_of_place: dataPlaceForm.name,
            limit_of_participants: dataPlaceForm.limit
        },
            config
        ).then(res => {
            store.addNotification({
                ...notificationTemplate,
                type: 'success',
                title: 'Dodano miejsce',
                message: 'Miejsce zostało poprawnie dodane.'
            });
            clearPlaceForm();
            closeModalAddPlace();
            setRefresh(!refresh) //calling rerender
        }).catch(err => {
            console.error("Error", err);
            store.addNotification({
                ...notificationTemplate,
                type: 'danger',
                title: 'Nie dodano miejsca',
                message: 'Miejsce nie zostało dodane.'
            });
        })
    }

    useEffect(() => {
        handleGetEvents();
        handleGetInstructors();
        handleGetPlaces();

        return () => { //cleanup function
            setListEvents([]);
            setListInstructors([]);
            setListPlaces([]);
        };
    }, [refresh])

    useEffect(() => {
        handleEventsMap();
    }, [date, handleEventsMap, listEvents, refresh])

    return (
        <div className="month">
            <PanelInfoEvent
                eventsMap={eventsMap}
                selectedDay={selectedDay}
                handleRemoveEvent={handleRemoveEvent}
                cookieStyle={cookieStyle}
            />
            <div className="days">
                <div className={`nameYear ${changeStyle(cookieStyle, 'nameYear')}`}>
                    {handeNameYear()}
                </div>
                <div className={`nameMonth ${changeStyle(cookieStyle, 'nameMonth')}`}>
                    {handeNameMonth()}
                </div>
                <Month
                    headerDays={headerDays}
                    handleDisplayDays={handleDisplayDays}
                    cookieStyle={cookieStyle}
                />
            </div>
            <div>
                <button onClick={() => handlePrevNextMonth("prev")} className="buttonPrevNext btn btn-secondary">PREV</button>
                <button onClick={() => handlePrevNextMonth("next")} className="buttonPrevNext btn btn-primary">NEXT</button>
            </div>
            <div>
                {isAdminOrReceptionist() &&
                    <div>
                        <button className="buttonsAddEventAndPlace btn btn-success" onClick={openModal}>Dodaj wydarzenie</button>
                        <button className="buttonsAddEventAndPlace btn btn-success" onClick={openModalAddPlace}>Dodaj miejsce</button>
                    </div>
                }
                <AddEventModal
                    isOpen={modalIsOpen}
                    closeModal={closeModal}
                    contentLabel="Add event"
                    handleAddEvent={handleAddEvent}
                    limitParticipants={limitParticipants}
                    setLimitParticipants={setLimitParticipants}
                    instructor={instructor}
                    setInstructor={setInstructor}
                    clearForm={clearForm}
                    listInstructors={listInstructors}
                    listPlaces={listPlaces}
                    place={place}
                    setPlace={setPlace}
                    dataForm={dataForm}
                    setDataForm={setDataForm}
                    handleInputData={handleInputData}
                    handleLimitParticipants={handleLimitParticipants}
                    handleSetPrice={handleSetPrice}
                />
                <AddPlaceModal
                    isOpen={modalAddPlaceIsOpen}
                    closeModal={closeModalAddPlace}
                    contentLabel="Add place"
                    handleInputDataPlace={handleInputDataPlace}
                    dataPlaceForm={dataPlaceForm}
                    clearForm={clearPlaceForm}
                    handleAddPlace={handleAddPlace}
                />
            </div>
            <div>
                <RemoveEventModal
                    isOpen={modalRemoveIsOpen}
                    closeModal={closeModalRemoveEvent}
                    contentLabel="Remove event"
                    handleRemoveEvent={handleRemoveEventAfterAprove}
                />
            </div>
        </div>
    )
}

export default Calendar;