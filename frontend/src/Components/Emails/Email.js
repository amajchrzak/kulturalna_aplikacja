import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { nameHost } from '../../GlobalVariables/GlobalVariables';
import Multiselect from 'multiselect-react-dropdown';
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../Notification/Notification';
import '../../Styles/Email.css';
import { changeStyle } from '../../AuxilliaryFunctions/changeStyle';

const Email = ({ cookieStyle }) => {
    const [listOfEmails, setListOfEmails] = useState([]);
    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');
    let [emails] = useState([]);
    const [refresh, setRefresh] = useState(false);

    const getUsers = async () => {
        const token = localStorage.getItem("token");

        try {
            await axios.get(`${nameHost}/get-users`, {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            })
                .then(res => {
                    if (res.data.message === "SUCCESS") {
                        let tmp = []
                        for (let i = 0; i < res.data.users.length; i++) {
                            tmp.push(res.data.users[i].Email);
                        }
                        setListOfEmails(tmp);
                    } else {
                        setListOfEmails([])
                    }
                })
                .catch(err => {
                    console.error('Error', err);
                    setListOfEmails([])
                })

        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        getUsers();
    }, [refresh]);

    const submit = async (e) => {
        e.preventDefault()
        try {
            axios.post('http://localhost:8080/send-group-email', {
                title: title,
                message: message,
                emails: emails,
            }).then((res) => {
                if (res.data.message === "SUCCESS") {
                    setRefresh(prevState => !prevState);
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'success',
                        title: 'Email został wysłany',
                        message: 'Wysyłanie maila zakończyło się powodzeniem'
                    });
                } else {
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'danger',
                        title: 'Email nie został wysłany',
                        message: 'Wysyłanie maila zakończyło się niepowodzeniem'
                    });
                }
            })
                .catch((e) => {
                    console.log(e);
                    store.addNotification({
                        ...notificationTemplate,
                        type: 'danger',
                        title: 'Email nie został wysłany',
                        message: 'Wysyłanie maila zakończyło się niepowodzeniem'
                    });
                });
        } catch (e) {
            console.log(e);
        }
    }

    const onSelect = (data) => {
        emails.push(data[data.length - 1])
    }

    const onRemove = (data) => {
        while (emails.length > 0) {
            emails.pop();
        }

        if (data.length !== 0) {
            data.forEach((email) => {
                emails.push(email)
            })
        }

        console.log(emails)
    }

    return (
        <div className={`email ${changeStyle(cookieStyle, 'email')}`}>
            <form onSubmit={submit}>
                <Multiselect
                    isObject={false}
                    onRemove={onRemove}
                    onSelect={onSelect}
                    options={listOfEmails}
                    displayValue="emails"
                />
                <div className="form-group">
                    <input type="text" name="title" className="form-control" placeholder="Title" required
                        onChange={e => setTitle(e.target.value)} />
                </div>
                <div className="form-group">
                    <textarea name="message" id="message" className="form-control" rows={4} placeholder="Enter your message" required
                        onChange={e => setMessage(e.target.value)}></textarea>
                </div>
                <div className="form-group">
                    <button className="w-100 btn btn-lg btn-primary" type="submit">Send Mail</button>
                </div>
            </form>
        </div>
    );
};

export default Email;