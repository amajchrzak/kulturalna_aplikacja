
import React, {useState, useMemo, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { store } from 'react-notifications-component';
import { notificationTemplate } from '../../Notification/Notification';
import '../../../Styles/PanelPost.css';

import axios from 'axios';
import { nameHost } from '../../../GlobalVariables/GlobalVariables';

const AddPost = () => {    
    
    const [changes,setChanges]= useState(false);  
    
    const[values, setValues] = useState({
        title: "",
        description: ""
    });
    
    const ClearPanelAddPost = () => {
        setValues({
            title: "",
            description: ""
        })       
    }
 
    const token = localStorage.getItem("token");

    const handleFormSubmit = (event) => {
        console.log(values.title.length)
            event.preventDefault();
            if(values.title == "" && values.description == ""){

                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Wpis nie został dodany',
                    message: 'Formularz jest pusty',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })

            }
            else if(values.title == "" || values.description == ""){

                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Pole formularza jest puste',
                    message: 'Aby dodać wpis, wszytskie pola muszą być wypełnione',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })

            }
            else  if(values.title.length >= 250)
            {
                     store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Tytuł jest zbyt długi',
                    message: 'Tytuł powinien mieć mniej niż 250 znaków',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })
            }
    
            else if(changes){

            
            //const url = "https://mocki.io/v1/8ce1b654-c93e-4f1a-9e9e-5147435391aa"; //Succes
            const url = nameHost + "/add-post";
            
            axios.post(url, {
                title: values.title,
                description: values.description,
            },
            {
                headers: 
                {
                    Authorization: 'Bearer ' + token
                }
            }).then(res => {
                console.log(values)
                console.info("###Response from addpost endpoint:", res);
                if (res.status === 200) {
                    console.info("jakis log")
                    console.info(res.data.message)
                    if (res.data.message === "SUCCESS") {
                        console.info("###Add post ")
                        

                        setChanges(false);
                store.addNotification({
                    ...notificationTemplate,
                    type: 'success',
                    title: 'Wpis został dodany',
                    message: ' ',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })
                
                ClearPanelAddPost();
               

            }}}).catch(err => {
                console.error("Error from register endpoint", err);

                store.addNotification({
                    ...notificationTemplate,
                    type: 'danger',
                    title: 'Problem z dodaniem wpisu',
                    message: 'Wpis nie został dodany',
                    dismiss: {
                        duration: 8000,
                        onScreen: true
                    }
                })});}
           
    }


    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value,

        })

        setChanges(true);        
    }

    let history = useHistory();
    const PanelPost = () => {
        if(!changes){
        history.push({ 
            pathname: '/panel_post'
            
           })}
           else {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success',
                  cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
              })
              
              swalWithBootstrapButtons.fire({
                title: 'Chcesz na pewno wyjść bez dodania wydarzenia?',
                text: "Cofnięcie akcji będzie niemożliwe, wszystkie dane zostaną usunięte.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Tak, chce wrócić',
                cancelButtonText: 'Anuluj',
                reverseButtons: true
              }).then((result) => {
                if (result.isConfirmed) {
                    history.push({ 
                        pathname: '/Panel_post'
                        
                       })
                } 
              })
           }

    
           
    }
    return (
        <div className="window panelpost">          
        
                    <h2 className="title titlepost">Formularz dodania wpisu</h2>
                    <div>
                        <form >
                            <div className="">
                                <div className="titleaddpost">
                                    <label htmlFor="title" className="h3 mb-3">Tytuł</label>
                                    <br/>
                                    <input name="title" type="text" 
                                    value={values.title}
                                    onChange={handleChange} 
                                     className="form-control" placeholder="Title" required
                                     
                                    />
                                </div>
                                <div className="descriptionaddpost">
                                    <label htmlFor="description" className="h3 mb-3 ">Opis</label>
                                    <textarea name ="description"  
                                    value={values.description}
                                    onChange={handleChange} 
                                     type="text-area" className="form-control descriptionPostInput" placeholder="Description" required
                                                                        
                                    />
                                </div>
                            </div>
                            <br />
       

                                <div>
                                
                                <div className="clearButton">
                                <div className="buttons">
                                    <button className="btn btn-dark btn-block singleButton" onClick={() => PanelPost()}>Powrót</button>  
                                </div>
                                    <button type="submit" className="btn btn-success btn-block singleButton " onClick={ClearPanelAddPost} >Wyczyść formularz</button>
                                    </div>
                                             
                                
                                <div className="buttons">
                                    <button type="submit" className="btn btn-success btn-block singleButton " onClick={handleFormSubmit}>Dodaj nowy wpis</button>
                                </div>          

                                </div>                     
                            
                        </form>
                    </div>
                </div>
     
    
    )
}

export default AddPost
