import Modal from 'react-modal';
import '../../Styles/ModalViewAddEvent.css';
import AddPlaceForm from '../Forms/AddPlaceForm';

const AddPlaceModal = props => {

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: '50%',
            height: '40%',
            marginRight: '-30%',
            transform: 'translate(-50%, -50%)',
            backgroundColor: "rgb(48, 52, 92)",
            color: "white"
        },
    };

    Modal.setAppElement('#root');

    return (
        <>
            <Modal
                isOpen={props.isOpen}
                onRequestClose={props.onRequestClose}
                style={customStyles}
                contentLabel={props.contentLabel}
            >
                <AddPlaceForm
                    data={props}
                />
            </Modal>
        </>
    )
}

export default AddPlaceModal;