const AddOpinionForm = props => {
    return (
        <div className="window">
            <h2 className="title">Dodaj swoją opinię</h2>
            <div>
                <form onSubmit={e => props.handleAddOpinion(e)}>
                    <label htmlFor="inputTitle" className="h3 mb-3">Tytuł (5-30 znaków)</label>
                    <input type="text" id="inputTitle" className="form-control" placeholder="Title" required
                        name="inputTitle"
                        onChange={e => { props.handleSetInputTitle(e.target.value) }}
                        value={props.inputTitle}
                    />
                    <br />
                    <label htmlFor="inputDescription" className="h3 mb-3">Treść (5-300 znaków)</label>
                    <textarea type="text-area" id="inputDescription" className="form-control" placeholder="Description" required
                        name="inputDescription"
                        onChange={e => { props.handleSetInputDescription(e.target.value) }}
                        value={props.inputDescription}
                        rows="5"
                    />
                    <br />
                    <div className="buttons">
                        <button onClick={props.clearForm} className="btn btn-secondary btn-block singleButton">Wyczyść formularz</button>
                        <button onClick={props.closeModal} className="btn btn-secondary btn-block singleButton">Zamknij</button>
                        <button type="submit" className="btn btn-success btn-block singleButton">Dodaj opinię</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default AddOpinionForm;