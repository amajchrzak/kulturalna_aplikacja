const AddPlaceForm = props => {
    return (
        <div className="window">
            <h2 className="title">Formularz dodawania nowego miejsca</h2>
            <div>
                <form onSubmit={e => props.data.handleAddPlace(e)}>
                    <label htmlFor="inputName" className="h3 mb-3">Nazwa</label>
                    <input type="text" id="inputName" className="form-control" placeholder="Name" required
                        name="name"
                        value={props.data.dataPlaceForm.name}
                        onChange={e => { props.data.handleInputDataPlace(e) }}
                    />
                    <label htmlFor="inputLimit" className="h3 mb-3">Ilość miejsc</label>
                    <input type="number" id="inputLimit" className="form-control" placeholder="Limit" required
                        name="limit"
                        min={0}
                        value={props.data.dataPlaceForm.limit}
                        onChange={e => { props.data.handleInputDataPlace(e) }}
                    />
                    <br />
                    <div>
                        <div className="clearButton">
                            <button onClick={props.data.clearForm} className="btn btn-dark btn-block singleButton">Wyczyść formularz</button>
                        </div>
                        <div className="buttons">
                            <button onClick={props.data.closeModal} className="btn btn-secondary btn-block singleButton">Zamknij</button>
                            <button type="submit" className="btn btn-success btn-block singleButton">Dodaj miejsce</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default AddPlaceForm;