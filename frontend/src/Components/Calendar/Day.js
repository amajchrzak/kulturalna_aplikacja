import { changeStyle } from "../../AuxilliaryFunctions/changeStyle";

const Day = ({ number, setSelectedDay, events, cookieStyle, blankDay = false }) => {

    const handleDisplayEventTitle = () => {
        const listEvents = [];
        events.forEach((event, index) => {
            listEvents.push(
                <p key={index} className="titleEvent">
                    {event.title}
                </p>
            )
        });
        return listEvents;
    }

    return (

        <div className={`${(blankDay || number === "") ? "blankDay" : "day"}`} onClick={number ? () => setSelectedDay(number) : undefined}>
            <div>
                <div className={`numberDay ${changeStyle(cookieStyle, 'numberDay')}`}>
                    {number}
                </div>
                {events.length ?
                    <div>
                        {handleDisplayEventTitle()}
                    </div>
                    :
                    null
                }
            </div>
        </div>
    )
}

export default Day;