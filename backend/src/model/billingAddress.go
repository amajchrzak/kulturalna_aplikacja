package model

import (
	"database/sql"
	"net/http"

	"gitlab.com/amajchrzak/kulturalna_aplikacja/src/interfaces"
)

func GetUserBillingAddress(userID int) (interfaces.NewBillingData, int) {
	query := `SELECT name, surname FROM billing_address WHERE user_id = $1`

	rows := DB.QueryRow(query, userID)

	billingAddress := interfaces.NewBillingData{}

	err := rows.Scan(&billingAddress.Name, &billingAddress.Surname)
	switch {
	case err == sql.ErrNoRows:
		billingAddress.Name = ""
		billingAddress.Surname = ""

		return billingAddress, http.StatusOK
	case err != nil:

		return billingAddress, http.StatusBadRequest
	}

	return billingAddress, http.StatusOK
}
