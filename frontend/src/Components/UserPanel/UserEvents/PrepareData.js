import ShowEvent from "./ShowEvent";
import CancelEvent from "./CancelEvent";

const range = len => {
    const arr = [];
    for (let i = 0; i < len; i++) {
        arr.push(i);
    }
    return arr;
}

const nextEvent = (event, goEvent, cancelEvent) => {
    return {
        name: event.Title,
        startDate: event.StartOfEvent.substring(0, 10),
        endDate: event.EndOfEvent.substring(0, 10),
        goToEvent: ShowEvent(goEvent, event.StartOfEvent, event.ID),
        cancelEvent: CancelEvent(cancelEvent, event.ID),
    }
}

const PrepareData = (events, goEvent, cancelEvent, ...lens) => {
    const makeDataLevel = (depth = 0) => {
        const len = lens[depth];
        return range(len).map(numberEvent => {
            return {
                ...nextEvent(events[numberEvent], goEvent, cancelEvent),
                subRows: lens[depth + 1] ? makeDataLevel(depth + 1) : undefined
            }
        })
    }
    return makeDataLevel();
}

export default PrepareData;